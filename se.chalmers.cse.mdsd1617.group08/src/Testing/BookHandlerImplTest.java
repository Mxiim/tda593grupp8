package Testing;

import static org.junit.Assert.*;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ClassElements.Admin.impl.AdminFactoryImpl;
import ClassElements.Admin.impl.AdminImpl;
import ClassElements.Booking.IBookHandler;
import ClassElements.Booking.IBooking;
import ClassElements.Booking.ICountRoomType;
import ClassElements.Booking.impl.BookHandlerImpl;
import ClassElements.Booking.impl.BookingFactoryImpl;
import ClassElements.Booking.impl.BookingImpl;
import ClassElements.HotelCustomer.FreeRoomTypesDTO;
import ClassElements.Room.IRoom;
import ClassElements.Room.IRoomType;
import ClassElements.Room.impl.RoomFactoryImpl;
import ClassElements.Room.impl.RoomManagerImpl;
import sun.util.calendar.LocalGregorianCalendar.Date;


public class BookHandlerImplTest {
    /**
     * Sets up the test fixture. 
     * (Called before every test case method.)
     */
	BookHandlerImpl bookHandler;
	AdminImpl admin;
	RoomFactoryImpl room;
	
    @Before
    public void setUp() {
    	admin = (AdminImpl) AdminFactoryImpl.init().createAdmin();
    	bookHandler = (BookHandlerImpl) BookingFactoryImpl.init().createBookHandler();
    	
    	admin.startup(10);
    	admin.addRoomType("triple", 80.0, 3, "Bar");
    	IRoomType triple = null;
    	
    	for(IRoomType irt : admin.getIroomtypemanager().getRoomTypes()) {
    		if(irt.getName().equals("triple")) {
    			triple = irt;
    		}
    	}
    	
    	admin.addRoom("11", triple);
    	admin.addRoom("12", triple);
    	admin.addRoom("13", triple);
    }

    /**
     * Tears down the test fixture. 
     * (Called after every test case method.)
     */
    @After
    public void tearDown() {
    	bookHandler.clearData();
    	admin.getIroomtypemanager().getRoomTypes().clear();
    	((RoomManagerImpl) admin.getIroommanager()).getRooms().clear();
    }
    

	@Test
	public void testCheckInRoom() {
		String fname = "Ahraz";
		String lname = "Asif";
		
		String sDate = "22161223";
		String eDate = "22161224";
		int b1 = bookHandler.initiateBooking(fname, lname, sDate, eDate);
		boolean added = bookHandler.addRoomToBooking("double", b1);
		boolean added1 = bookHandler.addRoomToBooking("double", b1);
		boolean added2 = bookHandler.addRoomToBooking("double", b1);
		
		bookHandler.confirmBooking(b1);
		
		int roomNum = bookHandler.checkInRoom("double", b1);
		int roomNum1 = bookHandler.checkInRoom("double", b1);
		int roomNum2 = bookHandler.checkInRoom("double", b1);
		int roomNum3 = bookHandler.checkInRoom(null, null);
		
		boolean roomInList = false;
		boolean roomInList1 = false;
		boolean roomInList2 = false;
		
		RoomManagerImpl roomManager = (RoomManagerImpl) admin.getIroommanager();
		EList<IRoom> roomList = roomManager.getRooms();
		
		for(IRoom room : roomList) {
			int roomListNum = Integer.parseInt(room.getRoomNumber());
			if(roomListNum == roomNum) roomInList = true;
			if(roomListNum == roomNum1) roomInList1 = true;
			if(roomListNum == roomNum2) roomInList2 = true;
		}
		
		assertNotEquals(roomNum, roomNum1);
		assertNotEquals(roomNum1, roomNum2);
		assertNotEquals(roomNum2, roomNum);
		
		assertEquals(true, roomInList);
		assertEquals(true, roomInList1);
		assertEquals(true, roomInList2);
		assertTrue(roomNum3 == -1); //test that null input is handled correctly
		
	}

	@Test
	public void testAddExtraCost() {
		String fname = "Ahraz";
		String lname = "Asif";
		String sDate = "22161223";
		String eDate = "22161224";
		
		int b1 = bookHandler.initiateBooking(fname, lname, sDate, eDate);
		
		boolean added = bookHandler.addRoomToBooking("double", b1);
		boolean added1 = bookHandler.addRoomToBooking("double", b1);
		boolean added2 = bookHandler.addRoomToBooking("double", b1);
		boolean added3 = bookHandler.addRoomToBooking("double", b1);
		
		bookHandler.confirmBooking(b1);
		
		int roomNum = bookHandler.checkInRoom("double", b1);
		int roomNum1 = bookHandler.checkInRoom("double", b1);
		int roomNum2 = bookHandler.checkInRoom("double", b1);
		int roomNum3 = bookHandler.checkInRoom("double", b1);
		
		bookHandler.addExtraCost(b1, Integer.toString(roomNum), "drinks", 100.0);
		bookHandler.addExtraCost(b1, Integer.toString(roomNum1), "food", 20.0);
		bookHandler.addExtraCost(b1, Integer.toString(roomNum2), "entertainment", 4.20);
		boolean costWasAdded = bookHandler.addExtraCost(b1, Integer.toString(roomNum3), null, null);
		
		double p1 = bookHandler.initiateRoomCheckout(b1, roomNum);
		double p2 = bookHandler.initiateRoomCheckout(b1, roomNum1);
		double p3 = bookHandler.initiateRoomCheckout(b1, roomNum2);
		double p4 = bookHandler.initiateRoomCheckout(b1, roomNum3);
		
		assertEquals(140, p1, 0.001);
		assertEquals(60, p2, 0.001);
		assertEquals(44.20, p3, 0.001);
		//tests that null input is handled correctly
		assertEquals(40, p4, 0.001);
		assertTrue(false == costWasAdded);
		
	}

	@Test
	public void testUpdateBooking() {
		String fname = "Ahraz";
		String lname = "Asif";
		
		String sDate = "22161223";
		String eDate = "22161224";
		
		int b1 = bookHandler.initiateBooking(fname, lname, sDate, eDate);
		
		boolean added = bookHandler.addRoomToBooking("double", b1);
		boolean added1 = bookHandler.addRoomToBooking("double", b1);
		boolean added2 = bookHandler.addRoomToBooking("double", b1);
		
		bookHandler.updateBooking(b1, sDate, "22161225");
		
		BookingImpl booking = (BookingImpl) bookHandler.getBooking(b1);
		String newEndDate = booking.getEndDate();
		
		assertEquals("22161225", newEndDate);
		//tests that null input is handled correctly
		bookHandler.updateBooking(b1, null, null);
		assertEquals(sDate, booking.getStartDate());
		assertEquals(newEndDate, booking.getEndDate());
		
	}

	@Test
	public void testUpdateRooms() {
		String fname = "Ahraz";
		String lname = "Asif";
		String sDate = "22161223";
		String eDate = "22161224";
		
		int b1 = bookHandler.initiateBooking(fname, lname, sDate, eDate);
		
		boolean added = bookHandler.addRoomToBooking("double", b1);
		boolean added1 = bookHandler.addRoomToBooking("double", b1);
		boolean added2 = bookHandler.addRoomToBooking("double", b1);
		
		IRoomType roomType = null;
		
		for(IRoomType irt : admin.getIroomtypemanager().getRoomTypes()) {
			if(irt.getName() == "double") roomType = irt;
		}
		
		int numCount = 0;
		
		bookHandler.updateRooms(roomType, 9, Integer.toString(b1));
		EList<ICountRoomType> countRoomList = bookHandler.getBooking(b1).getCountRoomTypes();
		for(ICountRoomType crt : countRoomList) {
			if(crt.getRoomType().getName() == "double") numCount = crt.getCount();
		}
		
		assertEquals(9, numCount);
		
		//tests that you can remove rooms from a booking
		bookHandler.updateRooms(roomType, 6, Integer.toString(b1));
		for(ICountRoomType crt : countRoomList) {
			if(crt.getRoomType().getName() == "double") numCount = crt.getCount();
		}
		
		assertEquals(6, numCount);
		
		//tests that null input is handled correctly
		bookHandler.updateRooms(null, 1, null);
		for(ICountRoomType crt : countRoomList) {
			if(crt.getRoomType().getName() == "double") numCount = crt.getCount();
		}
		assertEquals(6, numCount);
		
		bookHandler.updateRooms(null, 1, Integer.toString(b1));
		for(ICountRoomType crt : countRoomList) {
			if(crt.getRoomType().getName() == "double") numCount = crt.getCount();
		}
		assertEquals(6, numCount);
	}

	@Test
	public void testGetConfirmedBookings() {
		String fname = "Ahraz";
		String lname = "Asif";
		String sDate = "22161223";
		String eDate = "22161224";
		
		int b1 = bookHandler.initiateBooking(fname, lname, sDate, eDate);
		
		boolean added = bookHandler.addRoomToBooking("double", b1);
		boolean added1 = bookHandler.addRoomToBooking("double", b1);
		boolean added2 = bookHandler.addRoomToBooking("double", b1);
		
		bookHandler.confirmBooking(b1);
		
		int confirmedSize = bookHandler.getConfirmedBookings().size();
		
		assertEquals(1, confirmedSize);
		
		BookingImpl booking = (BookingImpl) bookHandler.getConfirmedBookings().get(0);
		int bookingId = Integer.parseInt(booking.getBookID());
		
		assertEquals(b1, bookingId);
	}

	@Test
	public void testGetBooking() {
		String fname = "Ahraz";
		String lname = "Asif";
		String sDate = "22161223";
		String eDate = "22161224";
		
		int b1 = bookHandler.initiateBooking(fname, lname, sDate, eDate);
		
		BookingImpl booking = (BookingImpl) bookHandler.getBooking(b1);
		assertEquals(b1, Integer.parseInt(booking.getBookID()));
		
	}

	@Test
	public void testGetOccupiedRooms() {
		String fname = "Ahraz";
		String lname = "Asif";
		String sDate = "22161223";
		String eDate = "22161224";
		
		int b1 = bookHandler.initiateBooking(fname, lname, sDate, eDate);
		
		boolean added = bookHandler.addRoomToBooking("double", b1);
		boolean added1 = bookHandler.addRoomToBooking("double", b1);
		boolean added2 = bookHandler.addRoomToBooking("double", b1);
		
		bookHandler.confirmBooking(b1);
		
		int roomNum = bookHandler.checkInRoom("double", b1);
		int count = 0;
		
		EMap<IRoom, Integer> occupiedRooms = bookHandler.getOccupiedRooms("20161222");
		if(occupiedRooms != null){
			for(IRoom room : occupiedRooms.keySet()) {
				count += occupiedRooms.get(room);
			}
			assertEquals(3, count);
		}
		//tests that null input is handled correctly
		EMap<IRoom, Integer> occupiedRooms2 = bookHandler.getOccupiedRooms(null);
		if(occupiedRooms2 != null){
			for(IRoom room : occupiedRooms2.keySet()) {
				count += occupiedRooms2.get(room);
			}
			assertEquals(0, count);
		}
	}

	@Test
	public void testCancelBooking() {
		String fname = "Ahraz";
		String lname = "Asif";
		String sDate = "22161223";
		String eDate = "22161224";
		int b1 = bookHandler.initiateBooking(fname, lname, sDate, eDate);
		boolean added = bookHandler.addRoomToBooking("double", b1);
		boolean added1 = bookHandler.addRoomToBooking("double", b1);
		boolean added2 = bookHandler.addRoomToBooking("double", b1);
		
		bookHandler.confirmBooking(b1);
		
		int roomNum = bookHandler.checkInRoom("double", b1);
		int roomNum1 = bookHandler.checkInRoom("double", b1);
		int roomNum2 = bookHandler.checkInRoom("double", b1);
		
		bookHandler.cancelBooking(b1);
		BookingImpl booking = (BookingImpl) bookHandler.getBooking(b1);
		assertEquals(null, booking);
	}

	@Test
	public void testCheckInBooking() {
		String fname = "Ahraz";
		String lname = "Asif";
		String sDate = "22161223";
		String eDate = "22161224";
		int b1 = bookHandler.initiateBooking(fname, lname, sDate, eDate);
		boolean added = bookHandler.addRoomToBooking("double", b1);
		boolean added1 = bookHandler.addRoomToBooking("double", b1);
		boolean added2 = bookHandler.addRoomToBooking("double", b1);
		
		bookHandler.confirmBooking(b1);
		bookHandler.checkInBooking(b1);
		
		EList<Integer> checkins = bookHandler.getCheckIns(sDate);
		assertEquals(3, checkins.size());
	}

	@Test
	public void testInitiateBooking() {
		String fname = "Ahraz";
		String lname = "Asif";
		String sDate = "22161223";
		String eDate = "22161224";
		int b1 = bookHandler.initiateBooking(fname, lname, sDate, eDate);
		int b2 = bookHandler.initiateBooking(null, null, null, null);
		
		BookingImpl realBooking = (BookingImpl) bookHandler.getBooking(b1);
		assertNotEquals(null, realBooking);
		//tests that null input is handled correctly
		BookingImpl realBooking2 = (BookingImpl) bookHandler.getBooking(b2);
		assertEquals(null, realBooking2);
		assertEquals(-1, b2);
	}

	@Test
	public void testGetFreeRooms() {
		String sDate = "22161223";
		String eDate = "22161224";
		int count = 0;
		int count2 = 0;
		EList<FreeRoomTypesDTO> freeRooms = bookHandler.getFreeRooms(2, sDate, eDate);
		for(FreeRoomTypesDTO frt : freeRooms) {
			count += frt.getNumFreeRooms();
		}
		assertEquals(13, count);
		
		EList<FreeRoomTypesDTO> freeRooms2 = bookHandler.getFreeRooms(2, null, null);
		for(FreeRoomTypesDTO frt : freeRooms2) {
			count2 += frt.getNumFreeRooms();
		}
		assertEquals(0, count2);
	}
	

	@Test
	public void testConfirmBooking() {
		String fname = "Ahraz";
		String lname = "Asif";
		String sDate = "22161223";
		String eDate = "22161224";
		
		int b1 = bookHandler.initiateBooking(fname, lname, sDate, eDate);
		
		boolean added = bookHandler.addRoomToBooking("double", b1);
		boolean added1 = bookHandler.addRoomToBooking("double", b1);
		boolean added2 = bookHandler.addRoomToBooking("double", b1);
		
		bookHandler.confirmBooking(b1);
		int confirmedSize = bookHandler.getConfirmedBookings().size();
		
		assertEquals(1, confirmedSize);
		
		BookingImpl booking = (BookingImpl) bookHandler.getConfirmedBookings().get(0);
		int bookingId = Integer.parseInt(booking.getBookID());
		
		assertEquals(b1, bookingId);
		
		//tests that wrong input is handled correctly
		int b2 = bookHandler.initiateBooking(fname, lname, sDate, eDate);
		boolean added3 = bookHandler.addRoomToBooking("double", b1);
		boolean bookingWasConfirmed = bookHandler.confirmBooking(Integer.MIN_VALUE);
		int confirmedSize2 = bookHandler.getConfirmedBookings().size();
		assertEquals(1, confirmedSize2);
		assertEquals(false, bookingWasConfirmed);
		
	}

	@Test
	public void testAddRoomToBooking() {
		String fname = "Ahraz";
		String lname = "Asif";
		String sDate = "22161223";
		String eDate = "22161224";
		int b1 = bookHandler.initiateBooking(fname, lname, sDate, eDate);
		boolean added = bookHandler.addRoomToBooking("double", b1);
		boolean added1 = bookHandler.addRoomToBooking("double", b1);
		boolean added2 = bookHandler.addRoomToBooking("double", b1);
		
		bookHandler.confirmBooking(b1);
		
		int roomNum = bookHandler.checkInRoom("double", b1);
		assertNotEquals(-1, roomNum);
		
		
		bookHandler.checkInRoom("double", b1);
		bookHandler.checkInRoom("double", b1);
		//tests that null input is handled correctly
		bookHandler.addRoomToBooking(null, b1);
		int roomNum2 = bookHandler.checkInRoom(null, b1);
		assertEquals(-1, roomNum2);
		
	}

	@Test
	public void testInitiateCheckout() {
		String fname = "Ahraz";
		String lname = "Asif";
		String sDate = "22161223";
		String eDate = "22161224";
		
		int b1 = bookHandler.initiateBooking(fname, lname, sDate, eDate);
		
		boolean added = bookHandler.addRoomToBooking("double", b1);
		boolean added1 = bookHandler.addRoomToBooking("double", b1);
		boolean added2 = bookHandler.addRoomToBooking("double", b1);
		
		bookHandler.confirmBooking(b1);
		
		bookHandler.checkInBooking(b1);
		double price = bookHandler.initiateCheckout(b1);
		assertNotEquals(0.0, price, 0.001);
	}

	@Test
	public void testInitiateRoomCheckout() {
		String fname = "Ahraz";
		String lname = "Asif";
		String sDate = "22161223";
		String eDate = "22161224";
		
		int b1 = bookHandler.initiateBooking(fname, lname, sDate, eDate);
		
		boolean added = bookHandler.addRoomToBooking("double", b1);
		boolean added1 = bookHandler.addRoomToBooking("double", b1);
		boolean added2 = bookHandler.addRoomToBooking("double", b1);
		
		bookHandler.confirmBooking(b1);
		
		int roomNum = bookHandler.checkInRoom("double", b1);
		int roomNum1 = bookHandler.checkInRoom("double", b1);
		int roomNum2 = bookHandler.checkInRoom("double", b1);
		
		double price = bookHandler.initiateRoomCheckout(b1, roomNum);
		assertEquals(40.0, price, 0.001);
	}
	
	// The following tests will be tests of the use cases.
	
	
	// Use case 2.1.1
	@Test
	public void testCaseMakeABooking() {
		String fname = "Ahraz";
		String lname = "Asif";
		String sDate = "22161223";
		String eDate = "22161224";
		int b1 = bookHandler.initiateBooking(fname, lname, sDate, eDate);
		
		bookHandler.addRoomToBooking("double", b1);
		bookHandler.addRoomToBooking("triple", b1);
		
		bookHandler.confirmBooking(b1);
	}
	
	// Use case 2.1.2
	@Test
	public void testCaseSearchFreeRooms() {
		String fname = "Ahraz";
		String lname = "Asif";
		String sDate = "22161223";
		String eDate = "22161224";
		int b1 = bookHandler.initiateBooking(fname, lname, sDate, eDate);
		
		bookHandler.addRoomToBooking("double", b1);
		bookHandler.addRoomToBooking("triple", b1);
		
		bookHandler.confirmBooking(b1);
		
		EList<FreeRoomTypesDTO> freeRooms = bookHandler.getFreeRooms(2, sDate, eDate);
		int doubleFree = 0;
		int tripleFree = 0;
		
		for(FreeRoomTypesDTO frt : freeRooms) {
			if(frt.getRoomTypeDescription() == "triple") tripleFree = frt.getNumFreeRooms();
			if(frt.getRoomTypeDescription() == "double") doubleFree = frt.getNumFreeRooms();
 		}
		
		assertEquals(doubleFree, 9);
		assertEquals(tripleFree, 2);
	}
	
	// Use case 2.1.3
	@Test
	public void testCaseCheckInBooking() {
		String fname = "Ahraz";
		String lname = "Asif";
		String sDate = "22161223";
		String eDate = "22161224";
		int b1 = bookHandler.initiateBooking(fname, lname, sDate, eDate);
		boolean added = bookHandler.addRoomToBooking("double", b1);
		boolean added1 = bookHandler.addRoomToBooking("double", b1);
		boolean added2 = bookHandler.addRoomToBooking("double", b1);
		
		bookHandler.confirmBooking(b1);
		
		bookHandler.checkInBooking(b1);
		
		EList<Integer> checkins = bookHandler.getCheckIns(sDate);
		assertEquals(3, checkins.size());
	}
	
	// Use case 2.1.4
	@Test
	public void testCaseCheckOutBooking() {
		String fname = "Ahraz";
		String lname = "Asif";
		String sDate = "22161223";
		String eDate = "22161224";
		
		int b1 = bookHandler.initiateBooking(fname, lname, sDate, eDate);
		
		boolean added = bookHandler.addRoomToBooking("double", b1);
		boolean added1 = bookHandler.addRoomToBooking("double", b1);
		boolean added2 = bookHandler.addRoomToBooking("double", b1);
		bookHandler.confirmBooking(b1);
		
		bookHandler.checkInBooking(b1);
		double price = bookHandler.initiateCheckout(b1);
		assertEquals(120.0, price, 0.001);
	}
	
	// Use case 2.1.5 is covered by the earlier tests testUpdateBooking() & testUpdateRoom()
	
	// Use case 2.1.6 is covered by the earlier test testCancelBooking();
	
	// Use case 2.1.7 is covered by the earlier test testGetConfirmedBookings();
	
	// Use case 2.1.8 is covered by the earlier test testGetOccupiedRooms();
	
	// Use case 2.1.9 is covered by the earlier test testCheckInBooking();
	
	// Use case 2.1.10
	
	// Use case 2.1.11 is covered by the earlier test testCheckInRoom();
	
	// Use case 2.1.12
	@Test
	public void testCaseCheckOutAndPayRoom() {
		String fname = "Ahraz";
		String lname = "Asif";
		String sDate = "22161223";
		String eDate = "22161224";
		
		int b1 = bookHandler.initiateBooking(fname, lname, sDate, eDate);
		
		boolean added = bookHandler.addRoomToBooking("double", b1);
		boolean added1 = bookHandler.addRoomToBooking("double", b1);
		boolean added2 = bookHandler.addRoomToBooking("double", b1);
		
		bookHandler.confirmBooking(b1);
		
		int roomNum = bookHandler.checkInRoom("double", b1);
		int roomNum1 = bookHandler.checkInRoom("double", b1);
		int roomNum2 = bookHandler.checkInRoom("double", b1);
		
		double price = bookHandler.initiateRoomCheckout(b1, roomNum);
		boolean paid = bookHandler.payRoomDuringCheckout(roomNum, "0812345678911234", "123", 5, 16, fname, lname);
		assertEquals(true, paid);
	}
	
	// Use case 2.1.13 is covered by the previous test testAddExtraCost();
}
