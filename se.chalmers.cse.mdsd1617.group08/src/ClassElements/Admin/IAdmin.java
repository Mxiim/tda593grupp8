/**
 */
package ClassElements.Admin;

import ClassElements.Room.IRoomType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IAdmin</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ClassElements.Admin.AdminPackage#getIAdmin()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IAdmin extends IHotelStartupProvides {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomNumberRequired="true" roomNumberOrdered="false"
	 * @generated
	 */
	void blockRoom(String roomNumber);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomNumberRequired="true" roomNumberOrdered="false"
	 * @generated
	 */
	void unblockRoom(String roomNumber);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomNumberRequired="true" roomNumberOrdered="false" roomTypeRequired="true" roomTypeOrdered="false"
	 * @generated
	 */
	void addRoom(String roomNumber, IRoomType roomType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNumberRequired="true" roomNumberOrdered="false" roomTypeNameRequired="true" roomTypeNameOrdered="false"
	 * @generated
	 */
	Boolean changeRoomType(String roomNumber, String roomTypeName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" nameRequired="true" nameOrdered="false" priceRequired="true" priceOrdered="false" numOfBedsRequired="true" numOfBedsOrdered="false" featuresRequired="true" featuresOrdered="false"
	 * @generated
	 */
	Boolean addRoomType(String name, Double price, int numOfBeds, String features);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" oldNameRequired="true" oldNameOrdered="false" newNameRequired="true" newNameOrdered="false" newPriceRequired="true" newPriceOrdered="false" newNumOfBedsRequired="true" newNumOfBedsOrdered="false" newFeaturesRequired="true" newFeaturesOrdered="false"
	 * @generated
	 */
	Boolean updateRoomType(String oldName, String newName, Double newPrice, int newNumOfBeds, String newFeatures);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" nameRequired="true" nameOrdered="false"
	 * @generated
	 */
	Boolean removeRoomType(String name);
} // IAdmin
