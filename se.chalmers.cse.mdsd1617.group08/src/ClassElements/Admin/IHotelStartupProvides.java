/**
 */
package ClassElements.Admin;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IHotel Startup Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ClassElements.Admin.AdminPackage#getIHotelStartupProvides()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IHotelStartupProvides extends EObject {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model numRoomsRequired="true" numRoomsOrdered="false"
	 * @generated
	 */
	void startup(int numRooms);
} // IHotelStartupProvides
