/**
 */
package ClassElements.Admin;

import ClassElements.Room.IRoomManager;
import ClassElements.Room.IRoomTypeManager;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Admin</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.Admin.Admin#getIroomtypemanager <em>Iroomtypemanager</em>}</li>
 *   <li>{@link ClassElements.Admin.Admin#getIroommanager <em>Iroommanager</em>}</li>
 * </ul>
 *
 * @see ClassElements.Admin.AdminPackage#getAdmin()
 * @model
 * @generated
 */
public interface Admin extends IAdmin {
	/**
	 * Returns the value of the '<em><b>Iroommanager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iroommanager</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iroommanager</em>' reference.
	 * @see #setIroommanager(IRoomManager)
	 * @see ClassElements.Admin.AdminPackage#getAdmin_Iroommanager()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomManager getIroommanager();

	/**
	 * Sets the value of the '{@link ClassElements.Admin.Admin#getIroommanager <em>Iroommanager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iroommanager</em>' reference.
	 * @see #getIroommanager()
	 * @generated
	 */
	void setIroommanager(IRoomManager value);

	/**
	 * Returns the value of the '<em><b>Iroomtypemanager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iroomtypemanager</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iroomtypemanager</em>' reference.
	 * @see #setIroomtypemanager(IRoomTypeManager)
	 * @see ClassElements.Admin.AdminPackage#getAdmin_Iroomtypemanager()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomTypeManager getIroomtypemanager();

	/**
	 * Sets the value of the '{@link ClassElements.Admin.Admin#getIroomtypemanager <em>Iroomtypemanager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iroomtypemanager</em>' reference.
	 * @see #getIroomtypemanager()
	 * @generated
	 */
	void setIroomtypemanager(IRoomTypeManager value);

} // Admin
