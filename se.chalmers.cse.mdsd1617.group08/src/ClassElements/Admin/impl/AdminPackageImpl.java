/**
 */
package ClassElements.Admin.impl;

import ClassElements.Admin.Admin;
import ClassElements.Admin.AdminFactory;
import ClassElements.Admin.AdminPackage;
import ClassElements.Admin.IAdmin;
import ClassElements.Admin.IHotelStartupProvides;

import ClassElements.Booking.BookingPackage;

import ClassElements.Booking.impl.BookingPackageImpl;

import ClassElements.HotelCustomer.ExternalSupply.ExternalSupplyPackage;

import ClassElements.HotelCustomer.ExternalSupply.impl.ExternalSupplyPackageImpl;

import ClassElements.HotelCustomer.HotelCustomerPackage;

import ClassElements.HotelCustomer.Receptionist.ReceptionistPackage;

import ClassElements.HotelCustomer.Receptionist.impl.ReceptionistPackageImpl;

import ClassElements.HotelCustomer.impl.HotelCustomerPackageImpl;

import ClassElements.Room.RoomPackage;

import ClassElements.Room.impl.RoomPackageImpl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AdminPackageImpl extends EPackageImpl implements AdminPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iAdminEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iHotelStartupProvidesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass adminEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ClassElements.Admin.AdminPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private AdminPackageImpl() {
		super(eNS_URI, AdminFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link AdminPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static AdminPackage init() {
		if (isInited) return (AdminPackage)EPackage.Registry.INSTANCE.getEPackage(AdminPackage.eNS_URI);

		// Obtain or create and register package
		AdminPackageImpl theAdminPackage = (AdminPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof AdminPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new AdminPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		HotelCustomerPackageImpl theHotelCustomerPackage = (HotelCustomerPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(HotelCustomerPackage.eNS_URI) instanceof HotelCustomerPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(HotelCustomerPackage.eNS_URI) : HotelCustomerPackage.eINSTANCE);
		ReceptionistPackageImpl theReceptionistPackage = (ReceptionistPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ReceptionistPackage.eNS_URI) instanceof ReceptionistPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ReceptionistPackage.eNS_URI) : ReceptionistPackage.eINSTANCE);
		ExternalSupplyPackageImpl theExternalSupplyPackage = (ExternalSupplyPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExternalSupplyPackage.eNS_URI) instanceof ExternalSupplyPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExternalSupplyPackage.eNS_URI) : ExternalSupplyPackage.eINSTANCE);
		BookingPackageImpl theBookingPackage = (BookingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) instanceof BookingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) : BookingPackage.eINSTANCE);
		RoomPackageImpl theRoomPackage = (RoomPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) instanceof RoomPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) : RoomPackage.eINSTANCE);

		// Create package meta-data objects
		theAdminPackage.createPackageContents();
		theHotelCustomerPackage.createPackageContents();
		theReceptionistPackage.createPackageContents();
		theExternalSupplyPackage.createPackageContents();
		theBookingPackage.createPackageContents();
		theRoomPackage.createPackageContents();

		// Initialize created meta-data
		theAdminPackage.initializePackageContents();
		theHotelCustomerPackage.initializePackageContents();
		theReceptionistPackage.initializePackageContents();
		theExternalSupplyPackage.initializePackageContents();
		theBookingPackage.initializePackageContents();
		theRoomPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theAdminPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(AdminPackage.eNS_URI, theAdminPackage);
		return theAdminPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIAdmin() {
		return iAdminEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIAdmin__BlockRoom__String() {
		return iAdminEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIAdmin__UnblockRoom__String() {
		return iAdminEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIAdmin__AddRoom__String_IRoomType() {
		return iAdminEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIAdmin__ChangeRoomType__String_String() {
		return iAdminEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIAdmin__AddRoomType__String_Double_int_String() {
		return iAdminEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIAdmin__UpdateRoomType__String_String_Double_int_String() {
		return iAdminEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIAdmin__RemoveRoomType__String() {
		return iAdminEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIHotelStartupProvides() {
		return iHotelStartupProvidesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIHotelStartupProvides__Startup__int() {
		return iHotelStartupProvidesEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAdmin() {
		return adminEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAdmin_Iroommanager() {
		return (EReference)adminEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAdmin_Iroomtypemanager() {
		return (EReference)adminEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdminFactory getAdminFactory() {
		return (AdminFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		iAdminEClass = createEClass(IADMIN);
		createEOperation(iAdminEClass, IADMIN___BLOCK_ROOM__STRING);
		createEOperation(iAdminEClass, IADMIN___UNBLOCK_ROOM__STRING);
		createEOperation(iAdminEClass, IADMIN___ADD_ROOM__STRING_IROOMTYPE);
		createEOperation(iAdminEClass, IADMIN___CHANGE_ROOM_TYPE__STRING_STRING);
		createEOperation(iAdminEClass, IADMIN___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING);
		createEOperation(iAdminEClass, IADMIN___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_STRING);
		createEOperation(iAdminEClass, IADMIN___REMOVE_ROOM_TYPE__STRING);

		iHotelStartupProvidesEClass = createEClass(IHOTEL_STARTUP_PROVIDES);
		createEOperation(iHotelStartupProvidesEClass, IHOTEL_STARTUP_PROVIDES___STARTUP__INT);

		adminEClass = createEClass(ADMIN);
		createEReference(adminEClass, ADMIN__IROOMTYPEMANAGER);
		createEReference(adminEClass, ADMIN__IROOMMANAGER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		RoomPackage theRoomPackage = (RoomPackage)EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		iAdminEClass.getESuperTypes().add(this.getIHotelStartupProvides());
		adminEClass.getESuperTypes().add(this.getIAdmin());

		// Initialize classes, features, and operations; add parameters
		initEClass(iAdminEClass, IAdmin.class, "IAdmin", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getIAdmin__BlockRoom__String(), null, "blockRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIAdmin__UnblockRoom__String(), null, "unblockRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIAdmin__AddRoom__String_IRoomType(), null, "addRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theRoomPackage.getIRoomType(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIAdmin__ChangeRoomType__String_String(), ecorePackage.getEBooleanObject(), "changeRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeName", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIAdmin__AddRoomType__String_Double_int_String(), ecorePackage.getEBooleanObject(), "addRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDoubleObject(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "numOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "features", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIAdmin__UpdateRoomType__String_String_Double_int_String(), ecorePackage.getEBooleanObject(), "updateRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "oldName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "newName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDoubleObject(), "newPrice", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "newNumOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "newFeatures", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIAdmin__RemoveRoomType__String(), ecorePackage.getEBooleanObject(), "removeRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(iHotelStartupProvidesEClass, IHotelStartupProvides.class, "IHotelStartupProvides", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getIHotelStartupProvides__Startup__int(), null, "startup", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "numRooms", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(adminEClass, Admin.class, "Admin", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAdmin_Iroomtypemanager(), theRoomPackage.getIRoomTypeManager(), null, "iroomtypemanager", null, 1, 1, Admin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAdmin_Iroommanager(), theRoomPackage.getIRoomManager(), null, "iroommanager", null, 1, 1, Admin.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //AdminPackageImpl
