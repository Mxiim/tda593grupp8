/**
 */
package ClassElements.Admin.impl;

import ClassElements.Admin.Admin;
import ClassElements.Admin.AdminPackage;
import ClassElements.Room.IRoomManager;
import ClassElements.Room.IRoomType;
import ClassElements.Room.IRoomTypeManager;
import ClassElements.Room.impl.RoomFactoryImpl;
import ClassElements.Booking.IBookHandler;
import ClassElements.Booking.impl.BookingFactoryImpl;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Admin</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.Admin.impl.AdminImpl#getIroomtypemanager <em>Iroomtypemanager</em>}</li>
 *   <li>{@link ClassElements.Admin.impl.AdminImpl#getIroommanager <em>Iroommanager</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AdminImpl extends MinimalEObjectImpl.Container implements Admin {
	/**
	 * The cached value of the '{@link #getIroomtypemanager() <em>Iroomtypemanager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIroomtypemanager()
	 * @generated NOT
	 * @ordered
	 */
	protected IRoomTypeManager iroomtypemanager = new RoomFactoryImpl().createRoomTypeManager();

	/**
	 * The cached value of the '{@link #getIroommanager() <em>Iroommanager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIroommanager()
	 * @generated NOT
	 * @ordered
	 */
	protected IRoomManager iroommanager = new RoomFactoryImpl().createRoomManager();
	protected IBookHandler ibookhandler = (IBookHandler) BookingFactoryImpl.init().createBookHandler();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AdminImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdminPackage.Literals.ADMIN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomManager getIroommanager() {
		if (iroommanager != null && iroommanager.eIsProxy()) {
			InternalEObject oldIroommanager = (InternalEObject)iroommanager;
			iroommanager = (IRoomManager)eResolveProxy(oldIroommanager);
			if (iroommanager != oldIroommanager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AdminPackage.ADMIN__IROOMMANAGER, oldIroommanager, iroommanager));
			}
		}
		return iroommanager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomManager basicGetIroommanager() {
		return iroommanager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIroommanager(IRoomManager newIroommanager) {
		IRoomManager oldIroommanager = iroommanager;
		iroommanager = newIroommanager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdminPackage.ADMIN__IROOMMANAGER, oldIroommanager, iroommanager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void startup(int numRooms) {
		iroommanager.clearRoomList();
		iroomtypemanager.getRoomTypes().clear();
		ibookhandler.clearData();
		
		iroomtypemanager.addRoomType("double", 40.0, 2, "gardiner");
        for (int i = 1; i<= numRooms; i++) {
            String roomNumber = ""+i;
            iroommanager.addRoom(roomNumber, iroomtypemanager.getRoomTypes().get(0));
        }
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void blockRoom(String roomNumber) {
        iroommanager.blockRoom(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void unblockRoom(String roomNumber) {
        iroommanager.unblockRoom(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addRoom(String roomNumber, IRoomType roomType) {
        iroommanager.addRoom(roomNumber, roomType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean changeRoomType(String roomNumber, String roomTypeName) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		return iroommanager.changeRoomType(roomNumber, roomTypeName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean addRoomType(String name, Double price, int numOfBeds, String features) {
        return iroomtypemanager.addRoomType(name, price, numOfBeds, features);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean updateRoomType(String oldName, String newName, Double newPrice, int newNumOfBeds, String newFeatures) {
        return iroomtypemanager.updateRoomType(oldName, newName, newPrice, newNumOfBeds, newFeatures);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean removeRoomType(String name) {
        return iroomtypemanager.removeRoomType(name);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomTypeManager getIroomtypemanager() {
		if (iroomtypemanager != null && iroomtypemanager.eIsProxy()) {
			InternalEObject oldIroomtypemanager = (InternalEObject)iroomtypemanager;
			iroomtypemanager = (IRoomTypeManager)eResolveProxy(oldIroomtypemanager);
			if (iroomtypemanager != oldIroomtypemanager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AdminPackage.ADMIN__IROOMTYPEMANAGER, oldIroomtypemanager, iroomtypemanager));
			}
		}
		return iroomtypemanager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomTypeManager basicGetIroomtypemanager() {
		return iroomtypemanager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIroomtypemanager(IRoomTypeManager newIroomtypemanager) {
		IRoomTypeManager oldIroomtypemanager = iroomtypemanager;
		iroomtypemanager = newIroomtypemanager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdminPackage.ADMIN__IROOMTYPEMANAGER, oldIroomtypemanager, iroomtypemanager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdminPackage.ADMIN__IROOMTYPEMANAGER:
				if (resolve) return getIroomtypemanager();
				return basicGetIroomtypemanager();
			case AdminPackage.ADMIN__IROOMMANAGER:
				if (resolve) return getIroommanager();
				return basicGetIroommanager();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdminPackage.ADMIN__IROOMTYPEMANAGER:
				setIroomtypemanager((IRoomTypeManager)newValue);
				return;
			case AdminPackage.ADMIN__IROOMMANAGER:
				setIroommanager((IRoomManager)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdminPackage.ADMIN__IROOMTYPEMANAGER:
				setIroomtypemanager((IRoomTypeManager)null);
				return;
			case AdminPackage.ADMIN__IROOMMANAGER:
				setIroommanager((IRoomManager)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdminPackage.ADMIN__IROOMTYPEMANAGER:
				return iroomtypemanager != null;
			case AdminPackage.ADMIN__IROOMMANAGER:
				return iroommanager != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case AdminPackage.ADMIN___STARTUP__INT:
				startup((Integer)arguments.get(0));
				return null;
			case AdminPackage.ADMIN___BLOCK_ROOM__STRING:
				blockRoom((String)arguments.get(0));
				return null;
			case AdminPackage.ADMIN___UNBLOCK_ROOM__STRING:
				unblockRoom((String)arguments.get(0));
				return null;
			case AdminPackage.ADMIN___ADD_ROOM__STRING_IROOMTYPE:
				addRoom((String)arguments.get(0), (IRoomType)arguments.get(1));
				return null;
			case AdminPackage.ADMIN___CHANGE_ROOM_TYPE__STRING_STRING:
				return changeRoomType((String)arguments.get(0), (String)arguments.get(1));
			case AdminPackage.ADMIN___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING:
				return addRoomType((String)arguments.get(0), (Double)arguments.get(1), (Integer)arguments.get(2), (String)arguments.get(3));
			case AdminPackage.ADMIN___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_STRING:
				return updateRoomType((String)arguments.get(0), (String)arguments.get(1), (Double)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4));
			case AdminPackage.ADMIN___REMOVE_ROOM_TYPE__STRING:
				return removeRoomType((String)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //AdminImpl
