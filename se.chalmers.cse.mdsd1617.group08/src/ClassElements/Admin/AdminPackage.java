/**
 */
package ClassElements.Admin;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ClassElements.Admin.AdminFactory
 * @model kind="package"
 * @generated
 */
public interface AdminPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Admin";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///ClassElements/Admin.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ClassElements.Admin";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AdminPackage eINSTANCE = ClassElements.Admin.impl.AdminPackageImpl.init();

	/**
	 * The meta object id for the '{@link ClassElements.Admin.IAdmin <em>IAdmin</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Admin.IAdmin
	 * @see ClassElements.Admin.impl.AdminPackageImpl#getIAdmin()
	 * @generated
	 */
	int IADMIN = 0;

	/**
	 * The meta object id for the '{@link ClassElements.Admin.IHotelStartupProvides <em>IHotel Startup Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Admin.IHotelStartupProvides
	 * @see ClassElements.Admin.impl.AdminPackageImpl#getIHotelStartupProvides()
	 * @generated
	 */
	int IHOTEL_STARTUP_PROVIDES = 1;

	/**
	 * The number of structural features of the '<em>IHotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_STARTUP_PROVIDES___STARTUP__INT = 0;

	/**
	 * The number of operations of the '<em>IHotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT = 1;

	/**
	 * The number of structural features of the '<em>IAdmin</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN_FEATURE_COUNT = IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN___STARTUP__INT = IHOTEL_STARTUP_PROVIDES___STARTUP__INT;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN___BLOCK_ROOM__STRING = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN___UNBLOCK_ROOM__STRING = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN___ADD_ROOM__STRING_IROOMTYPE = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Change Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN___CHANGE_ROOM_TYPE__STRING_STRING = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_STRING = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN___REMOVE_ROOM_TYPE__STRING = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 6;

	/**
	 * The number of operations of the '<em>IAdmin</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IADMIN_OPERATION_COUNT = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 7;

	/**
	 * The meta object id for the '{@link ClassElements.Admin.impl.AdminImpl <em>Admin</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Admin.impl.AdminImpl
	 * @see ClassElements.Admin.impl.AdminPackageImpl#getAdmin()
	 * @generated
	 */
	int ADMIN = 2;

	/**
	 * The feature id for the '<em><b>Iroomtypemanager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN__IROOMTYPEMANAGER = IADMIN_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Iroommanager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN__IROOMMANAGER = IADMIN_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Admin</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_FEATURE_COUNT = IADMIN_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___STARTUP__INT = IADMIN___STARTUP__INT;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___BLOCK_ROOM__STRING = IADMIN___BLOCK_ROOM__STRING;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___UNBLOCK_ROOM__STRING = IADMIN___UNBLOCK_ROOM__STRING;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___ADD_ROOM__STRING_IROOMTYPE = IADMIN___ADD_ROOM__STRING_IROOMTYPE;

	/**
	 * The operation id for the '<em>Change Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___CHANGE_ROOM_TYPE__STRING_STRING = IADMIN___CHANGE_ROOM_TYPE__STRING_STRING;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = IADMIN___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_STRING = IADMIN___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_STRING;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN___REMOVE_ROOM_TYPE__STRING = IADMIN___REMOVE_ROOM_TYPE__STRING;

	/**
	 * The number of operations of the '<em>Admin</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADMIN_OPERATION_COUNT = IADMIN_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link ClassElements.Admin.IAdmin <em>IAdmin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IAdmin</em>'.
	 * @see ClassElements.Admin.IAdmin
	 * @generated
	 */
	EClass getIAdmin();

	/**
	 * Returns the meta object for the '{@link ClassElements.Admin.IAdmin#blockRoom(java.lang.String) <em>Block Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Block Room</em>' operation.
	 * @see ClassElements.Admin.IAdmin#blockRoom(java.lang.String)
	 * @generated
	 */
	EOperation getIAdmin__BlockRoom__String();

	/**
	 * Returns the meta object for the '{@link ClassElements.Admin.IAdmin#unblockRoom(java.lang.String) <em>Unblock Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unblock Room</em>' operation.
	 * @see ClassElements.Admin.IAdmin#unblockRoom(java.lang.String)
	 * @generated
	 */
	EOperation getIAdmin__UnblockRoom__String();

	/**
	 * Returns the meta object for the '{@link ClassElements.Admin.IAdmin#addRoom(java.lang.String, ClassElements.Room.IRoomType) <em>Add Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room</em>' operation.
	 * @see ClassElements.Admin.IAdmin#addRoom(java.lang.String, ClassElements.Room.IRoomType)
	 * @generated
	 */
	EOperation getIAdmin__AddRoom__String_IRoomType();

	/**
	 * Returns the meta object for the '{@link ClassElements.Admin.IAdmin#changeRoomType(java.lang.String, java.lang.String) <em>Change Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Room Type</em>' operation.
	 * @see ClassElements.Admin.IAdmin#changeRoomType(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIAdmin__ChangeRoomType__String_String();

	/**
	 * Returns the meta object for the '{@link ClassElements.Admin.IAdmin#addRoomType(java.lang.String, java.lang.Double, int, java.lang.String) <em>Add Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Type</em>' operation.
	 * @see ClassElements.Admin.IAdmin#addRoomType(java.lang.String, java.lang.Double, int, java.lang.String)
	 * @generated
	 */
	EOperation getIAdmin__AddRoomType__String_Double_int_String();

	/**
	 * Returns the meta object for the '{@link ClassElements.Admin.IAdmin#updateRoomType(java.lang.String, java.lang.String, java.lang.Double, int, java.lang.String) <em>Update Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Room Type</em>' operation.
	 * @see ClassElements.Admin.IAdmin#updateRoomType(java.lang.String, java.lang.String, java.lang.Double, int, java.lang.String)
	 * @generated
	 */
	EOperation getIAdmin__UpdateRoomType__String_String_Double_int_String();

	/**
	 * Returns the meta object for the '{@link ClassElements.Admin.IAdmin#removeRoomType(java.lang.String) <em>Remove Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room Type</em>' operation.
	 * @see ClassElements.Admin.IAdmin#removeRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getIAdmin__RemoveRoomType__String();

	/**
	 * Returns the meta object for class '{@link ClassElements.Admin.IHotelStartupProvides <em>IHotel Startup Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IHotel Startup Provides</em>'.
	 * @see ClassElements.Admin.IHotelStartupProvides
	 * @generated
	 */
	EClass getIHotelStartupProvides();

	/**
	 * Returns the meta object for the '{@link ClassElements.Admin.IHotelStartupProvides#startup(int) <em>Startup</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Startup</em>' operation.
	 * @see ClassElements.Admin.IHotelStartupProvides#startup(int)
	 * @generated
	 */
	EOperation getIHotelStartupProvides__Startup__int();

	/**
	 * Returns the meta object for class '{@link ClassElements.Admin.Admin <em>Admin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Admin</em>'.
	 * @see ClassElements.Admin.Admin
	 * @generated
	 */
	EClass getAdmin();

	/**
	 * Returns the meta object for the reference '{@link ClassElements.Admin.Admin#getIroommanager <em>Iroommanager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Iroommanager</em>'.
	 * @see ClassElements.Admin.Admin#getIroommanager()
	 * @see #getAdmin()
	 * @generated
	 */
	EReference getAdmin_Iroommanager();

	/**
	 * Returns the meta object for the reference '{@link ClassElements.Admin.Admin#getIroomtypemanager <em>Iroomtypemanager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Iroomtypemanager</em>'.
	 * @see ClassElements.Admin.Admin#getIroomtypemanager()
	 * @see #getAdmin()
	 * @generated
	 */
	EReference getAdmin_Iroomtypemanager();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AdminFactory getAdminFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ClassElements.Admin.IAdmin <em>IAdmin</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Admin.IAdmin
		 * @see ClassElements.Admin.impl.AdminPackageImpl#getIAdmin()
		 * @generated
		 */
		EClass IADMIN = eINSTANCE.getIAdmin();

		/**
		 * The meta object literal for the '<em><b>Block Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMIN___BLOCK_ROOM__STRING = eINSTANCE.getIAdmin__BlockRoom__String();

		/**
		 * The meta object literal for the '<em><b>Unblock Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMIN___UNBLOCK_ROOM__STRING = eINSTANCE.getIAdmin__UnblockRoom__String();

		/**
		 * The meta object literal for the '<em><b>Add Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMIN___ADD_ROOM__STRING_IROOMTYPE = eINSTANCE.getIAdmin__AddRoom__String_IRoomType();

		/**
		 * The meta object literal for the '<em><b>Change Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMIN___CHANGE_ROOM_TYPE__STRING_STRING = eINSTANCE.getIAdmin__ChangeRoomType__String_String();

		/**
		 * The meta object literal for the '<em><b>Add Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMIN___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = eINSTANCE.getIAdmin__AddRoomType__String_Double_int_String();

		/**
		 * The meta object literal for the '<em><b>Update Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMIN___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_STRING = eINSTANCE.getIAdmin__UpdateRoomType__String_String_Double_int_String();

		/**
		 * The meta object literal for the '<em><b>Remove Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IADMIN___REMOVE_ROOM_TYPE__STRING = eINSTANCE.getIAdmin__RemoveRoomType__String();

		/**
		 * The meta object literal for the '{@link ClassElements.Admin.IHotelStartupProvides <em>IHotel Startup Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Admin.IHotelStartupProvides
		 * @see ClassElements.Admin.impl.AdminPackageImpl#getIHotelStartupProvides()
		 * @generated
		 */
		EClass IHOTEL_STARTUP_PROVIDES = eINSTANCE.getIHotelStartupProvides();

		/**
		 * The meta object literal for the '<em><b>Startup</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_STARTUP_PROVIDES___STARTUP__INT = eINSTANCE.getIHotelStartupProvides__Startup__int();

		/**
		 * The meta object literal for the '{@link ClassElements.Admin.impl.AdminImpl <em>Admin</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Admin.impl.AdminImpl
		 * @see ClassElements.Admin.impl.AdminPackageImpl#getAdmin()
		 * @generated
		 */
		EClass ADMIN = eINSTANCE.getAdmin();

		/**
		 * The meta object literal for the '<em><b>Iroommanager</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ADMIN__IROOMMANAGER = eINSTANCE.getAdmin_Iroommanager();

		/**
		 * The meta object literal for the '<em><b>Iroomtypemanager</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ADMIN__IROOMTYPEMANAGER = eINSTANCE.getAdmin_Iroomtypemanager();

	}

} //AdminPackage
