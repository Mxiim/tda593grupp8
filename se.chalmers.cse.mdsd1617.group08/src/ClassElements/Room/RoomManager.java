/**
 */
package ClassElements.Room;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.Room.RoomManager#getRooms <em>Rooms</em>}</li>
 * </ul>
 *
 * @see ClassElements.Room.RoomPackage#getRoomManager()
 * @model
 * @generated
 */
public interface RoomManager extends IRoomManager {
	/**
	 * Returns the value of the '<em><b>Rooms</b></em>' reference list.
	 * The list contents are of type {@link ClassElements.Room.IRoom}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rooms</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rooms</em>' reference list.
	 * @see ClassElements.Room.RoomPackage#getRoomManager_Rooms()
	 * @model ordered="false"
	 * @generated
	 */
	EList<IRoom> getRooms();

} // RoomManager
