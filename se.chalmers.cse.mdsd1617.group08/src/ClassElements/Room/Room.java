/**
 */
package ClassElements.Room;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.Room.Room#getIroomtype <em>Iroomtype</em>}</li>
 *   <li>{@link ClassElements.Room.Room#getBlockedStatus <em>Blocked Status</em>}</li>
 *   <li>{@link ClassElements.Room.Room#getRoomNumber <em>Room Number</em>}</li>
 * </ul>
 *
 * @see ClassElements.Room.RoomPackage#getRoom()
 * @model
 * @generated
 */
public interface Room extends IRoom {
	/**
	 * Returns the value of the '<em><b>Iroomtype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iroomtype</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iroomtype</em>' reference.
	 * @see #setIroomtype(IRoomType)
	 * @see ClassElements.Room.RoomPackage#getRoom_Iroomtype()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomType getIroomtype();

	/**
	 * Sets the value of the '{@link ClassElements.Room.Room#getIroomtype <em>Iroomtype</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iroomtype</em>' reference.
	 * @see #getIroomtype()
	 * @generated
	 */
	void setIroomtype(IRoomType value);

	/**
	 * Returns the value of the '<em><b>Blocked Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Blocked Status</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Blocked Status</em>' attribute.
	 * @see #setBlockedStatus(Boolean)
	 * @see ClassElements.Room.RoomPackage#getRoom_BlockedStatus()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Boolean getBlockedStatus();

	/**
	 * Sets the value of the '{@link ClassElements.Room.Room#getBlockedStatus <em>Blocked Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Blocked Status</em>' attribute.
	 * @see #getBlockedStatus()
	 * @generated
	 */
	void setBlockedStatus(Boolean value);

	/**
	 * Returns the value of the '<em><b>Room Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Number</em>' attribute.
	 * @see #setRoomNumber(String)
	 * @see ClassElements.Room.RoomPackage#getRoom_RoomNumber()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getRoomNumber();

	/**
	 * Sets the value of the '{@link ClassElements.Room.Room#getRoomNumber <em>Room Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Number</em>' attribute.
	 * @see #getRoomNumber()
	 * @generated
	 */
	void setRoomNumber(String value);

} // Room
