/**
 */
package ClassElements.Room;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.Room.RoomTypeManager#getIroomtype <em>Iroomtype</em>}</li>
 * </ul>
 *
 * @see ClassElements.Room.RoomPackage#getRoomTypeManager()
 * @model
 * @generated
 */
public interface RoomTypeManager extends IRoomTypeManager {
	/**
	 * Returns the value of the '<em><b>Iroomtype</b></em>' reference list.
	 * The list contents are of type {@link ClassElements.Room.IRoomType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iroomtype</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iroomtype</em>' reference list.
	 * @see ClassElements.Room.RoomPackage#getRoomTypeManager_Iroomtype()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	EList<IRoomType> getIroomtype();

} // RoomTypeManager
