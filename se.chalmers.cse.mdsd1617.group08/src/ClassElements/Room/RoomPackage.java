/**
 */
package ClassElements.Room;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ClassElements.Room.RoomFactory
 * @model kind="package"
 * @generated
 */
public interface RoomPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Room";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///ClassElements/Room.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ClassElements.Room";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RoomPackage eINSTANCE = ClassElements.Room.impl.RoomPackageImpl.init();

	/**
	 * The meta object id for the '{@link ClassElements.Room.IRoomType <em>IRoom Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Room.IRoomType
	 * @see ClassElements.Room.impl.RoomPackageImpl#getIRoomType()
	 * @generated
	 */
	int IROOM_TYPE = 7;

	/**
	 * The meta object id for the '{@link ClassElements.Room.IRoomManager <em>IRoom Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Room.IRoomManager
	 * @see ClassElements.Room.impl.RoomPackageImpl#getIRoomManager()
	 * @generated
	 */
	int IROOM_MANAGER = 1;

	/**
	 * The meta object id for the '{@link ClassElements.Room.IRoomTypeManager <em>IRoom Type Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Room.IRoomTypeManager
	 * @see ClassElements.Room.impl.RoomPackageImpl#getIRoomTypeManager()
	 * @generated
	 */
	int IROOM_TYPE_MANAGER = 2;

	/**
	 * The meta object id for the '{@link ClassElements.Room.impl.RoomImpl <em>Room</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Room.impl.RoomImpl
	 * @see ClassElements.Room.impl.RoomPackageImpl#getRoom()
	 * @generated
	 */
	int ROOM = 3;

	/**
	 * The meta object id for the '{@link ClassElements.Room.impl.RoomTypeImpl <em>Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Room.impl.RoomTypeImpl
	 * @see ClassElements.Room.impl.RoomPackageImpl#getRoomType()
	 * @generated
	 */
	int ROOM_TYPE = 4;

	/**
	 * The meta object id for the '{@link ClassElements.Room.impl.RoomManagerImpl <em>Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Room.impl.RoomManagerImpl
	 * @see ClassElements.Room.impl.RoomPackageImpl#getRoomManager()
	 * @generated
	 */
	int ROOM_MANAGER = 5;

	/**
	 * The meta object id for the '{@link ClassElements.Room.impl.RoomTypeManagerImpl <em>Type Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Room.impl.RoomTypeManagerImpl
	 * @see ClassElements.Room.impl.RoomPackageImpl#getRoomTypeManager()
	 * @generated
	 */
	int ROOM_TYPE_MANAGER = 6;

	/**
	 * The meta object id for the '{@link ClassElements.Room.IRoom <em>IRoom</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Room.IRoom
	 * @see ClassElements.Room.impl.RoomPackageImpl#getIRoom()
	 * @generated
	 */
	int IROOM = 0;

	/**
	 * The number of structural features of the '<em>IRoom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Blocked Status</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM___GET_BLOCKED_STATUS = 0;

	/**
	 * The operation id for the '<em>Get Room Number</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM___GET_ROOM_NUMBER = 1;

	/**
	 * The operation id for the '<em>Set Room Number</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM___SET_ROOM_NUMBER__STRING = 2;

	/**
	 * The number of operations of the '<em>IRoom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_OPERATION_COUNT = 3;

	/**
	 * The number of structural features of the '<em>IRoom Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER___ADD_ROOM__STRING_IROOMTYPE = 0;

	/**
	 * The operation id for the '<em>Change Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER___CHANGE_ROOM_TYPE__STRING_STRING = 1;

	/**
	 * The operation id for the '<em>Set Blocked Room Status</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER___SET_BLOCKED_ROOM_STATUS__STRING_BOOLEAN = 2;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER___BLOCK_ROOM__STRING = 3;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER___UNBLOCK_ROOM__STRING = 4;

	/**
	 * The operation id for the '<em>Clear Room List</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER___CLEAR_ROOM_LIST = 5;

	/**
	 * The number of operations of the '<em>IRoom Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER_OPERATION_COUNT = 6;

	/**
	 * The number of structural features of the '<em>IRoom Type Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_MANAGER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_MANAGER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = 0;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_MANAGER___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_STRING = 1;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_MANAGER___REMOVE_ROOM_TYPE__STRING = 2;

	/**
	 * The operation id for the '<em>Get Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_MANAGER___GET_ROOM_TYPES = 3;

	/**
	 * The number of operations of the '<em>IRoom Type Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_MANAGER_OPERATION_COUNT = 4;

	/**
	 * The feature id for the '<em><b>Iroomtype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__IROOMTYPE = IROOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Blocked Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__BLOCKED_STATUS = IROOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Room Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__ROOM_NUMBER = IROOM_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_FEATURE_COUNT = IROOM_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Blocked Status</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___GET_BLOCKED_STATUS = IROOM___GET_BLOCKED_STATUS;

	/**
	 * The operation id for the '<em>Get Room Number</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___GET_ROOM_NUMBER = IROOM___GET_ROOM_NUMBER;

	/**
	 * The operation id for the '<em>Set Room Number</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___SET_ROOM_NUMBER__STRING = IROOM___SET_ROOM_NUMBER__STRING;

	/**
	 * The number of operations of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_OPERATION_COUNT = IROOM_OPERATION_COUNT + 0;

	/**
	 * The number of structural features of the '<em>IRoom Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE___UPDATE__STRING_DOUBLE_INT_STRING = 0;

	/**
	 * The operation id for the '<em>Get Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE___GET_NAME = 1;

	/**
	 * The operation id for the '<em>Get Price</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE___GET_PRICE = 2;

	/**
	 * The operation id for the '<em>Get Num Of Beds</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE___GET_NUM_OF_BEDS = 3;

	/**
	 * The operation id for the '<em>Get Features</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE___GET_FEATURES = 4;

	/**
	 * The number of operations of the '<em>IRoom Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_OPERATION_COUNT = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__NAME = IROOM_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__PRICE = IROOM_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Num Of Beds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__NUM_OF_BEDS = IROOM_TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Features</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__FEATURES = IROOM_TYPE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_FEATURE_COUNT = IROOM_TYPE_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___UPDATE__STRING_DOUBLE_INT_STRING = IROOM_TYPE___UPDATE__STRING_DOUBLE_INT_STRING;

	/**
	 * The operation id for the '<em>Get Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___GET_NAME = IROOM_TYPE___GET_NAME;

	/**
	 * The operation id for the '<em>Get Price</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___GET_PRICE = IROOM_TYPE___GET_PRICE;

	/**
	 * The operation id for the '<em>Get Num Of Beds</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___GET_NUM_OF_BEDS = IROOM_TYPE___GET_NUM_OF_BEDS;

	/**
	 * The operation id for the '<em>Get Features</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___GET_FEATURES = IROOM_TYPE___GET_FEATURES;

	/**
	 * The number of operations of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_OPERATION_COUNT = IROOM_TYPE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Rooms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER__ROOMS = IROOM_MANAGER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER_FEATURE_COUNT = IROOM_MANAGER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___ADD_ROOM__STRING_IROOMTYPE = IROOM_MANAGER___ADD_ROOM__STRING_IROOMTYPE;

	/**
	 * The operation id for the '<em>Change Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___CHANGE_ROOM_TYPE__STRING_STRING = IROOM_MANAGER___CHANGE_ROOM_TYPE__STRING_STRING;

	/**
	 * The operation id for the '<em>Set Blocked Room Status</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___SET_BLOCKED_ROOM_STATUS__STRING_BOOLEAN = IROOM_MANAGER___SET_BLOCKED_ROOM_STATUS__STRING_BOOLEAN;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___BLOCK_ROOM__STRING = IROOM_MANAGER___BLOCK_ROOM__STRING;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___UNBLOCK_ROOM__STRING = IROOM_MANAGER___UNBLOCK_ROOM__STRING;

	/**
	 * The operation id for the '<em>Clear Room List</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___CLEAR_ROOM_LIST = IROOM_MANAGER___CLEAR_ROOM_LIST;

	/**
	 * The number of operations of the '<em>Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER_OPERATION_COUNT = IROOM_MANAGER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Iroomtype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_MANAGER__IROOMTYPE = IROOM_TYPE_MANAGER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Type Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_MANAGER_FEATURE_COUNT = IROOM_TYPE_MANAGER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_MANAGER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = IROOM_TYPE_MANAGER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_MANAGER___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_STRING = IROOM_TYPE_MANAGER___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_STRING;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_MANAGER___REMOVE_ROOM_TYPE__STRING = IROOM_TYPE_MANAGER___REMOVE_ROOM_TYPE__STRING;

	/**
	 * The operation id for the '<em>Get Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_MANAGER___GET_ROOM_TYPES = IROOM_TYPE_MANAGER___GET_ROOM_TYPES;

	/**
	 * The number of operations of the '<em>Type Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_MANAGER_OPERATION_COUNT = IROOM_TYPE_MANAGER_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link ClassElements.Room.IRoomType <em>IRoom Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Type</em>'.
	 * @see ClassElements.Room.IRoomType
	 * @generated
	 */
	EClass getIRoomType();

	/**
	 * Returns the meta object for the '{@link ClassElements.Room.IRoomType#update(java.lang.String, java.lang.Double, int, java.lang.String) <em>Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update</em>' operation.
	 * @see ClassElements.Room.IRoomType#update(java.lang.String, java.lang.Double, int, java.lang.String)
	 * @generated
	 */
	EOperation getIRoomType__Update__String_Double_int_String();

	/**
	 * Returns the meta object for the '{@link ClassElements.Room.IRoomType#getName() <em>Get Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Name</em>' operation.
	 * @see ClassElements.Room.IRoomType#getName()
	 * @generated
	 */
	EOperation getIRoomType__GetName();

	/**
	 * Returns the meta object for the '{@link ClassElements.Room.IRoomType#getPrice() <em>Get Price</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Price</em>' operation.
	 * @see ClassElements.Room.IRoomType#getPrice()
	 * @generated
	 */
	EOperation getIRoomType__GetPrice();

	/**
	 * Returns the meta object for the '{@link ClassElements.Room.IRoomType#getNumOfBeds() <em>Get Num Of Beds</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Num Of Beds</em>' operation.
	 * @see ClassElements.Room.IRoomType#getNumOfBeds()
	 * @generated
	 */
	EOperation getIRoomType__GetNumOfBeds();

	/**
	 * Returns the meta object for the '{@link ClassElements.Room.IRoomType#getFeatures() <em>Get Features</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Features</em>' operation.
	 * @see ClassElements.Room.IRoomType#getFeatures()
	 * @generated
	 */
	EOperation getIRoomType__GetFeatures();

	/**
	 * Returns the meta object for class '{@link ClassElements.Room.IRoomManager <em>IRoom Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Manager</em>'.
	 * @see ClassElements.Room.IRoomManager
	 * @generated
	 */
	EClass getIRoomManager();

	/**
	 * Returns the meta object for the '{@link ClassElements.Room.IRoomManager#addRoom(java.lang.String, ClassElements.Room.IRoomType) <em>Add Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room</em>' operation.
	 * @see ClassElements.Room.IRoomManager#addRoom(java.lang.String, ClassElements.Room.IRoomType)
	 * @generated
	 */
	EOperation getIRoomManager__AddRoom__String_IRoomType();

	/**
	 * Returns the meta object for the '{@link ClassElements.Room.IRoomManager#changeRoomType(java.lang.String, java.lang.String) <em>Change Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Room Type</em>' operation.
	 * @see ClassElements.Room.IRoomManager#changeRoomType(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIRoomManager__ChangeRoomType__String_String();

	/**
	 * Returns the meta object for the '{@link ClassElements.Room.IRoomManager#setBlockedRoomStatus(java.lang.String, java.lang.Boolean) <em>Set Blocked Room Status</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Blocked Room Status</em>' operation.
	 * @see ClassElements.Room.IRoomManager#setBlockedRoomStatus(java.lang.String, java.lang.Boolean)
	 * @generated
	 */
	EOperation getIRoomManager__SetBlockedRoomStatus__String_Boolean();

	/**
	 * Returns the meta object for the '{@link ClassElements.Room.IRoomManager#blockRoom(java.lang.String) <em>Block Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Block Room</em>' operation.
	 * @see ClassElements.Room.IRoomManager#blockRoom(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomManager__BlockRoom__String();

	/**
	 * Returns the meta object for the '{@link ClassElements.Room.IRoomManager#unblockRoom(java.lang.String) <em>Unblock Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unblock Room</em>' operation.
	 * @see ClassElements.Room.IRoomManager#unblockRoom(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomManager__UnblockRoom__String();

	/**
	 * Returns the meta object for the '{@link ClassElements.Room.IRoomManager#clearRoomList() <em>Clear Room List</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clear Room List</em>' operation.
	 * @see ClassElements.Room.IRoomManager#clearRoomList()
	 * @generated
	 */
	EOperation getIRoomManager__ClearRoomList();

	/**
	 * Returns the meta object for class '{@link ClassElements.Room.IRoomTypeManager <em>IRoom Type Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Type Manager</em>'.
	 * @see ClassElements.Room.IRoomTypeManager
	 * @generated
	 */
	EClass getIRoomTypeManager();

	/**
	 * Returns the meta object for the '{@link ClassElements.Room.IRoomTypeManager#addRoomType(java.lang.String, java.lang.Double, int, java.lang.String) <em>Add Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Type</em>' operation.
	 * @see ClassElements.Room.IRoomTypeManager#addRoomType(java.lang.String, java.lang.Double, int, java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeManager__AddRoomType__String_Double_int_String();

	/**
	 * Returns the meta object for the '{@link ClassElements.Room.IRoomTypeManager#updateRoomType(java.lang.String, java.lang.String, java.lang.Double, int, java.lang.String) <em>Update Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Room Type</em>' operation.
	 * @see ClassElements.Room.IRoomTypeManager#updateRoomType(java.lang.String, java.lang.String, java.lang.Double, int, java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeManager__UpdateRoomType__String_String_Double_int_String();

	/**
	 * Returns the meta object for the '{@link ClassElements.Room.IRoomTypeManager#removeRoomType(java.lang.String) <em>Remove Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room Type</em>' operation.
	 * @see ClassElements.Room.IRoomTypeManager#removeRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeManager__RemoveRoomType__String();

	/**
	 * Returns the meta object for the '{@link ClassElements.Room.IRoomTypeManager#getRoomTypes() <em>Get Room Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room Types</em>' operation.
	 * @see ClassElements.Room.IRoomTypeManager#getRoomTypes()
	 * @generated
	 */
	EOperation getIRoomTypeManager__GetRoomTypes();

	/**
	 * Returns the meta object for class '{@link ClassElements.Room.Room <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room</em>'.
	 * @see ClassElements.Room.Room
	 * @generated
	 */
	EClass getRoom();

	/**
	 * Returns the meta object for the reference '{@link ClassElements.Room.Room#getIroomtype <em>Iroomtype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Iroomtype</em>'.
	 * @see ClassElements.Room.Room#getIroomtype()
	 * @see #getRoom()
	 * @generated
	 */
	EReference getRoom_Iroomtype();

	/**
	 * Returns the meta object for the attribute '{@link ClassElements.Room.Room#getBlockedStatus <em>Blocked Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Blocked Status</em>'.
	 * @see ClassElements.Room.Room#getBlockedStatus()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_BlockedStatus();

	/**
	 * Returns the meta object for the attribute '{@link ClassElements.Room.Room#getRoomNumber <em>Room Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Number</em>'.
	 * @see ClassElements.Room.Room#getRoomNumber()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_RoomNumber();

	/**
	 * Returns the meta object for class '{@link ClassElements.Room.RoomType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type</em>'.
	 * @see ClassElements.Room.RoomType
	 * @generated
	 */
	EClass getRoomType();

	/**
	 * Returns the meta object for the attribute '{@link ClassElements.Room.RoomType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see ClassElements.Room.RoomType#getName()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_Name();

	/**
	 * Returns the meta object for the attribute '{@link ClassElements.Room.RoomType#getPrice <em>Price</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price</em>'.
	 * @see ClassElements.Room.RoomType#getPrice()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_Price();

	/**
	 * Returns the meta object for the attribute '{@link ClassElements.Room.RoomType#getNumOfBeds <em>Num Of Beds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Of Beds</em>'.
	 * @see ClassElements.Room.RoomType#getNumOfBeds()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_NumOfBeds();

	/**
	 * Returns the meta object for the attribute '{@link ClassElements.Room.RoomType#getFeatures <em>Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Features</em>'.
	 * @see ClassElements.Room.RoomType#getFeatures()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_Features();

	/**
	 * Returns the meta object for class '{@link ClassElements.Room.RoomManager <em>Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Manager</em>'.
	 * @see ClassElements.Room.RoomManager
	 * @generated
	 */
	EClass getRoomManager();

	/**
	 * Returns the meta object for the reference list '{@link ClassElements.Room.RoomManager#getRooms <em>Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Rooms</em>'.
	 * @see ClassElements.Room.RoomManager#getRooms()
	 * @see #getRoomManager()
	 * @generated
	 */
	EReference getRoomManager_Rooms();

	/**
	 * Returns the meta object for class '{@link ClassElements.Room.RoomTypeManager <em>Type Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Manager</em>'.
	 * @see ClassElements.Room.RoomTypeManager
	 * @generated
	 */
	EClass getRoomTypeManager();

	/**
	 * Returns the meta object for the reference list '{@link ClassElements.Room.RoomTypeManager#getIroomtype <em>Iroomtype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Iroomtype</em>'.
	 * @see ClassElements.Room.RoomTypeManager#getIroomtype()
	 * @see #getRoomTypeManager()
	 * @generated
	 */
	EReference getRoomTypeManager_Iroomtype();

	/**
	 * Returns the meta object for class '{@link ClassElements.Room.IRoom <em>IRoom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom</em>'.
	 * @see ClassElements.Room.IRoom
	 * @generated
	 */
	EClass getIRoom();

	/**
	 * Returns the meta object for the '{@link ClassElements.Room.IRoom#getBlockedStatus() <em>Get Blocked Status</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Blocked Status</em>' operation.
	 * @see ClassElements.Room.IRoom#getBlockedStatus()
	 * @generated
	 */
	EOperation getIRoom__GetBlockedStatus();

	/**
	 * Returns the meta object for the '{@link ClassElements.Room.IRoom#getRoomNumber() <em>Get Room Number</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room Number</em>' operation.
	 * @see ClassElements.Room.IRoom#getRoomNumber()
	 * @generated
	 */
	EOperation getIRoom__GetRoomNumber();

	/**
	 * Returns the meta object for the '{@link ClassElements.Room.IRoom#setRoomNumber(java.lang.String) <em>Set Room Number</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Room Number</em>' operation.
	 * @see ClassElements.Room.IRoom#setRoomNumber(java.lang.String)
	 * @generated
	 */
	EOperation getIRoom__SetRoomNumber__String();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RoomFactory getRoomFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ClassElements.Room.IRoomType <em>IRoom Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Room.IRoomType
		 * @see ClassElements.Room.impl.RoomPackageImpl#getIRoomType()
		 * @generated
		 */
		EClass IROOM_TYPE = eINSTANCE.getIRoomType();

		/**
		 * The meta object literal for the '<em><b>Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE___UPDATE__STRING_DOUBLE_INT_STRING = eINSTANCE.getIRoomType__Update__String_Double_int_String();

		/**
		 * The meta object literal for the '<em><b>Get Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE___GET_NAME = eINSTANCE.getIRoomType__GetName();

		/**
		 * The meta object literal for the '<em><b>Get Price</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE___GET_PRICE = eINSTANCE.getIRoomType__GetPrice();

		/**
		 * The meta object literal for the '<em><b>Get Num Of Beds</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE___GET_NUM_OF_BEDS = eINSTANCE.getIRoomType__GetNumOfBeds();

		/**
		 * The meta object literal for the '<em><b>Get Features</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE___GET_FEATURES = eINSTANCE.getIRoomType__GetFeatures();

		/**
		 * The meta object literal for the '{@link ClassElements.Room.IRoomManager <em>IRoom Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Room.IRoomManager
		 * @see ClassElements.Room.impl.RoomPackageImpl#getIRoomManager()
		 * @generated
		 */
		EClass IROOM_MANAGER = eINSTANCE.getIRoomManager();

		/**
		 * The meta object literal for the '<em><b>Add Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_MANAGER___ADD_ROOM__STRING_IROOMTYPE = eINSTANCE.getIRoomManager__AddRoom__String_IRoomType();

		/**
		 * The meta object literal for the '<em><b>Change Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_MANAGER___CHANGE_ROOM_TYPE__STRING_STRING = eINSTANCE.getIRoomManager__ChangeRoomType__String_String();

		/**
		 * The meta object literal for the '<em><b>Set Blocked Room Status</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_MANAGER___SET_BLOCKED_ROOM_STATUS__STRING_BOOLEAN = eINSTANCE.getIRoomManager__SetBlockedRoomStatus__String_Boolean();

		/**
		 * The meta object literal for the '<em><b>Block Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_MANAGER___BLOCK_ROOM__STRING = eINSTANCE.getIRoomManager__BlockRoom__String();

		/**
		 * The meta object literal for the '<em><b>Unblock Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_MANAGER___UNBLOCK_ROOM__STRING = eINSTANCE.getIRoomManager__UnblockRoom__String();

		/**
		 * The meta object literal for the '<em><b>Clear Room List</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_MANAGER___CLEAR_ROOM_LIST = eINSTANCE.getIRoomManager__ClearRoomList();

		/**
		 * The meta object literal for the '{@link ClassElements.Room.IRoomTypeManager <em>IRoom Type Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Room.IRoomTypeManager
		 * @see ClassElements.Room.impl.RoomPackageImpl#getIRoomTypeManager()
		 * @generated
		 */
		EClass IROOM_TYPE_MANAGER = eINSTANCE.getIRoomTypeManager();

		/**
		 * The meta object literal for the '<em><b>Add Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_MANAGER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = eINSTANCE.getIRoomTypeManager__AddRoomType__String_Double_int_String();

		/**
		 * The meta object literal for the '<em><b>Update Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_MANAGER___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_STRING = eINSTANCE.getIRoomTypeManager__UpdateRoomType__String_String_Double_int_String();

		/**
		 * The meta object literal for the '<em><b>Remove Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_MANAGER___REMOVE_ROOM_TYPE__STRING = eINSTANCE.getIRoomTypeManager__RemoveRoomType__String();

		/**
		 * The meta object literal for the '<em><b>Get Room Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_MANAGER___GET_ROOM_TYPES = eINSTANCE.getIRoomTypeManager__GetRoomTypes();

		/**
		 * The meta object literal for the '{@link ClassElements.Room.impl.RoomImpl <em>Room</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Room.impl.RoomImpl
		 * @see ClassElements.Room.impl.RoomPackageImpl#getRoom()
		 * @generated
		 */
		EClass ROOM = eINSTANCE.getRoom();

		/**
		 * The meta object literal for the '<em><b>Iroomtype</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM__IROOMTYPE = eINSTANCE.getRoom_Iroomtype();

		/**
		 * The meta object literal for the '<em><b>Blocked Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__BLOCKED_STATUS = eINSTANCE.getRoom_BlockedStatus();

		/**
		 * The meta object literal for the '<em><b>Room Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__ROOM_NUMBER = eINSTANCE.getRoom_RoomNumber();

		/**
		 * The meta object literal for the '{@link ClassElements.Room.impl.RoomTypeImpl <em>Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Room.impl.RoomTypeImpl
		 * @see ClassElements.Room.impl.RoomPackageImpl#getRoomType()
		 * @generated
		 */
		EClass ROOM_TYPE = eINSTANCE.getRoomType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__NAME = eINSTANCE.getRoomType_Name();

		/**
		 * The meta object literal for the '<em><b>Price</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__PRICE = eINSTANCE.getRoomType_Price();

		/**
		 * The meta object literal for the '<em><b>Num Of Beds</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__NUM_OF_BEDS = eINSTANCE.getRoomType_NumOfBeds();

		/**
		 * The meta object literal for the '<em><b>Features</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__FEATURES = eINSTANCE.getRoomType_Features();

		/**
		 * The meta object literal for the '{@link ClassElements.Room.impl.RoomManagerImpl <em>Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Room.impl.RoomManagerImpl
		 * @see ClassElements.Room.impl.RoomPackageImpl#getRoomManager()
		 * @generated
		 */
		EClass ROOM_MANAGER = eINSTANCE.getRoomManager();

		/**
		 * The meta object literal for the '<em><b>Rooms</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_MANAGER__ROOMS = eINSTANCE.getRoomManager_Rooms();

		/**
		 * The meta object literal for the '{@link ClassElements.Room.impl.RoomTypeManagerImpl <em>Type Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Room.impl.RoomTypeManagerImpl
		 * @see ClassElements.Room.impl.RoomPackageImpl#getRoomTypeManager()
		 * @generated
		 */
		EClass ROOM_TYPE_MANAGER = eINSTANCE.getRoomTypeManager();

		/**
		 * The meta object literal for the '<em><b>Iroomtype</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_TYPE_MANAGER__IROOMTYPE = eINSTANCE.getRoomTypeManager_Iroomtype();

		/**
		 * The meta object literal for the '{@link ClassElements.Room.IRoom <em>IRoom</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Room.IRoom
		 * @see ClassElements.Room.impl.RoomPackageImpl#getIRoom()
		 * @generated
		 */
		EClass IROOM = eINSTANCE.getIRoom();

		/**
		 * The meta object literal for the '<em><b>Get Blocked Status</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM___GET_BLOCKED_STATUS = eINSTANCE.getIRoom__GetBlockedStatus();

		/**
		 * The meta object literal for the '<em><b>Get Room Number</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM___GET_ROOM_NUMBER = eINSTANCE.getIRoom__GetRoomNumber();

		/**
		 * The meta object literal for the '<em><b>Set Room Number</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM___SET_ROOM_NUMBER__STRING = eINSTANCE.getIRoom__SetRoomNumber__String();

	}

} //RoomPackage
