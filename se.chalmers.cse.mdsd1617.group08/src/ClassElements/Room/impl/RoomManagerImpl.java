/**
 */
package ClassElements.Room.impl;

import ClassElements.Room.IRoom;
import ClassElements.Room.IRoomType;
import ClassElements.Room.Room;
import ClassElements.Room.RoomFactory;
import ClassElements.Room.RoomManager;
import ClassElements.Room.RoomPackage;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.Room.impl.RoomManagerImpl#getRooms <em>Rooms</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomManagerImpl extends MinimalEObjectImpl.Container implements RoomManager {
	/**
	 * The cached value of the '{@link #getRooms() <em>Rooms</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRooms()
	 * @generated NOT
	 * @ordered
	 */
	protected static EList<IRoom> rooms = new BasicEList<>();
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomManagerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomPackage.Literals.ROOM_MANAGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IRoom> getRooms() {
		if (rooms == null) {
			rooms = new EObjectResolvingEList<IRoom>(IRoom.class, this, RoomPackage.ROOM_MANAGER__ROOMS);
		}
		return rooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addRoom(String roomNumber, IRoomType roomType) {
		for(IRoom room : rooms){
			if(room.getRoomNumber().equals(roomNumber))//Room with the same number already exists
				return;
		}
		RoomFactory factory = new RoomFactoryImpl();
		Room room = factory.createRoom();
		room.setRoomNumber(roomNumber);
		room.setIroomtype(roomType);
		rooms.add(room);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean changeRoomType(String roomNumber, String roomTypeName) {
		//Find correct room
		IRoom theRoom = null;
		for(IRoom room : rooms){
			if(roomNumber.equals(room.getRoomNumber())){
				theRoom = room;
				break;
			}
		}
		if(theRoom == null)//Room number not found
			return false;
		
		//Find correct room type
		IRoomType theType = null;
		for(IRoomType type : new RoomTypeManagerImpl().getRoomTypes()){
			if(type.getName().equals(roomTypeName)){
				theType = type;
				break;
			}
		}
		if(theType==null)//Room type not found
			return false;
		
		Room room = (Room)theRoom;//Will always work, required due to generation artifacts
		
		if(room.getIroomtype().getName().equals(roomTypeName))
			return false;//Room type is already the same as the given parameter
		

		room.setIroomtype(theType);//Set Room type to proper value
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean setBlockedRoomStatus(String roomNumber, Boolean blockedStatus) {
		for(IRoom room : rooms){
			if(room.getRoomNumber().equals(roomNumber)){
				Room roomTwo = (Room)room;//Will always work, required due to generation artifacts
				roomTwo.setBlockedStatus(blockedStatus);
				return true;
			}
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void blockRoom(String roomNumber) {
		for(IRoom room : rooms){
			if(room.getRoomNumber().equals(roomNumber)){
				Room roomTwo = (Room)room;//Will always work, required due to generation artifacts
				roomTwo.setBlockedStatus(true);
				return;
			}
		}
		throw new IllegalArgumentException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void unblockRoom(String roomNumber) {
		for(IRoom room : rooms){
			if(room.getRoomNumber().equals(roomNumber)){
				Room roomTwo = (Room)room;//Will always work, required due to generation artifacts
				roomTwo.setBlockedStatus(false);
				return;
			}
		}
		throw new IllegalArgumentException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void clearRoomList() {
		rooms.clear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RoomPackage.ROOM_MANAGER__ROOMS:
				return getRooms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RoomPackage.ROOM_MANAGER__ROOMS:
				getRooms().clear();
				getRooms().addAll((Collection<? extends IRoom>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM_MANAGER__ROOMS:
				getRooms().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM_MANAGER__ROOMS:
				return rooms != null && !rooms.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomPackage.ROOM_MANAGER___ADD_ROOM__STRING_IROOMTYPE:
				addRoom((String)arguments.get(0), (IRoomType)arguments.get(1));
				return null;
			case RoomPackage.ROOM_MANAGER___CHANGE_ROOM_TYPE__STRING_STRING:
				return changeRoomType((String)arguments.get(0), (String)arguments.get(1));
			case RoomPackage.ROOM_MANAGER___SET_BLOCKED_ROOM_STATUS__STRING_BOOLEAN:
				return setBlockedRoomStatus((String)arguments.get(0), (Boolean)arguments.get(1));
			case RoomPackage.ROOM_MANAGER___BLOCK_ROOM__STRING:
				blockRoom((String)arguments.get(0));
				return null;
			case RoomPackage.ROOM_MANAGER___UNBLOCK_ROOM__STRING:
				unblockRoom((String)arguments.get(0));
				return null;
			case RoomPackage.ROOM_MANAGER___CLEAR_ROOM_LIST:
				clearRoomList();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //RoomManagerImpl
