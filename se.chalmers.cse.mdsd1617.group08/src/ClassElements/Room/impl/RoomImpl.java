/**
 */
package ClassElements.Room.impl;

import ClassElements.Room.IRoomType;
import ClassElements.Room.Room;
import ClassElements.Room.RoomPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Room</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.Room.impl.RoomImpl#getIroomtype <em>Iroomtype</em>}</li>
 *   <li>{@link ClassElements.Room.impl.RoomImpl#getBlockedStatus <em>Blocked Status</em>}</li>
 *   <li>{@link ClassElements.Room.impl.RoomImpl#getRoomNumber <em>Room Number</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomImpl extends MinimalEObjectImpl.Container implements Room {
	/**
	 * The cached value of the '{@link #getIroomtype() <em>Iroomtype</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIroomtype()
	 * @generated
	 * @ordered
	 */
	protected IRoomType iroomtype;

	/**
	 * The default value of the '{@link #getBlockedStatus() <em>Blocked Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlockedStatus()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean BLOCKED_STATUS_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getBlockedStatus() <em>Blocked Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlockedStatus()
	 * @generated
	 * @ordered
	 */
	protected Boolean blockedStatus = BLOCKED_STATUS_EDEFAULT;
	/**
	 * The default value of the '{@link #getRoomNumber() <em>Room Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomNumber()
	 * @generated
	 * @ordered
	 */
	protected static final String ROOM_NUMBER_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getRoomNumber() <em>Room Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomNumber()
	 * @generated
	 * @ordered
	 */
	protected String roomNumber = ROOM_NUMBER_EDEFAULT;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomPackage.Literals.ROOM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomType getIroomtype() {
		if (iroomtype != null && iroomtype.eIsProxy()) {
			InternalEObject oldIroomtype = (InternalEObject)iroomtype;
			iroomtype = (IRoomType)eResolveProxy(oldIroomtype);
			if (iroomtype != oldIroomtype) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RoomPackage.ROOM__IROOMTYPE, oldIroomtype, iroomtype));
			}
		}
		return iroomtype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomType basicGetIroomtype() {
		return iroomtype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIroomtype(IRoomType newIroomtype) {
		IRoomType oldIroomtype = iroomtype;
		iroomtype = newIroomtype;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM__IROOMTYPE, oldIroomtype, iroomtype));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getBlockedStatus() {
		return blockedStatus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBlockedStatus(Boolean newBlockedStatus) {
		Boolean oldBlockedStatus = blockedStatus;
		blockedStatus = newBlockedStatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM__BLOCKED_STATUS, oldBlockedStatus, blockedStatus));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRoomNumber() {
		return roomNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomNumber(String newRoomNumber) {
		String oldRoomNumber = roomNumber;
		roomNumber = newRoomNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM__ROOM_NUMBER, oldRoomNumber, roomNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RoomPackage.ROOM__IROOMTYPE:
				if (resolve) return getIroomtype();
				return basicGetIroomtype();
			case RoomPackage.ROOM__BLOCKED_STATUS:
				return getBlockedStatus();
			case RoomPackage.ROOM__ROOM_NUMBER:
				return getRoomNumber();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RoomPackage.ROOM__IROOMTYPE:
				setIroomtype((IRoomType)newValue);
				return;
			case RoomPackage.ROOM__BLOCKED_STATUS:
				setBlockedStatus((Boolean)newValue);
				return;
			case RoomPackage.ROOM__ROOM_NUMBER:
				setRoomNumber((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM__IROOMTYPE:
				setIroomtype((IRoomType)null);
				return;
			case RoomPackage.ROOM__BLOCKED_STATUS:
				setBlockedStatus(BLOCKED_STATUS_EDEFAULT);
				return;
			case RoomPackage.ROOM__ROOM_NUMBER:
				setRoomNumber(ROOM_NUMBER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM__IROOMTYPE:
				return iroomtype != null;
			case RoomPackage.ROOM__BLOCKED_STATUS:
				return BLOCKED_STATUS_EDEFAULT == null ? blockedStatus != null : !BLOCKED_STATUS_EDEFAULT.equals(blockedStatus);
			case RoomPackage.ROOM__ROOM_NUMBER:
				return ROOM_NUMBER_EDEFAULT == null ? roomNumber != null : !ROOM_NUMBER_EDEFAULT.equals(roomNumber);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (blockedStatus: ");
		result.append(blockedStatus);
		result.append(", roomNumber: ");
		result.append(roomNumber);
		result.append(')');
		return result.toString();
	}

} //RoomImpl
