/**
 */
package ClassElements.Room.impl;

import ClassElements.Room.IRoomType;
import ClassElements.Room.RoomFactory;
import ClassElements.Room.RoomPackage;
import ClassElements.Room.RoomType;
import ClassElements.Room.RoomTypeManager;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.Room.impl.RoomTypeManagerImpl#getIroomtype <em>Iroomtype</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomTypeManagerImpl extends MinimalEObjectImpl.Container implements RoomTypeManager {
	/**
	 * The cached value of the '{@link #getIroomtype() <em>Iroomtype</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIroomtype()
	 * @generated NOT
	 * @ordered
	 */
	protected static EList<IRoomType> iroomtype = new BasicEList<>();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomTypeManagerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomPackage.Literals.ROOM_TYPE_MANAGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IRoomType> getIroomtype() {
		if (iroomtype == null) {
			iroomtype = new EObjectResolvingEList<IRoomType>(IRoomType.class, this, RoomPackage.ROOM_TYPE_MANAGER__IROOMTYPE);
		}
		return iroomtype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<IRoomType> getRoomTypes() {
		return iroomtype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean addRoomType(String name, Double price, int numOfBeds, String features) {
		for(IRoomType roomType : iroomtype){//Check if room type with that name already exists
			if(roomType.getName().equals(name)){
				return false;//Room type already exists, use update method
			}
		}
		//Add room type
		RoomFactory factory = new RoomFactoryImpl();
		RoomType type = factory.createRoomType();
		type.setName(name);
		type.setPrice(price);
		type.setNumOfBeds(numOfBeds);
		type.setFeatures(features);
		iroomtype.add(type);
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean updateRoomType(String oldName, String newName, Double newPrice, int newNumOfBeds, String newFeatures) {
		for(IRoomType roomType : iroomtype){//Find room type
			if(roomType.getName().equals(oldName)){//Update room type
				RoomType type = (RoomType)roomType;//Will always work, required due to generation artifacts
				type.setName(newName);
				type.setPrice(newPrice);
				type.setNumOfBeds(newNumOfBeds);
				type.setFeatures(newFeatures);
				return true;
			}
		}
		return false;//No such room type
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean removeRoomType(String name) {
		for(int i = 0; i < iroomtype.size(); i++){//Find room type
			if(iroomtype.get(i).getName().equals(name)){//Remove room type
				iroomtype.remove(i);
				return true;
			}
		}
		return false;//No such room type
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RoomPackage.ROOM_TYPE_MANAGER__IROOMTYPE:
				return getIroomtype();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RoomPackage.ROOM_TYPE_MANAGER__IROOMTYPE:
				getIroomtype().clear();
				getIroomtype().addAll((Collection<? extends IRoomType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM_TYPE_MANAGER__IROOMTYPE:
				getIroomtype().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM_TYPE_MANAGER__IROOMTYPE:
				return iroomtype != null && !iroomtype.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomPackage.ROOM_TYPE_MANAGER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING:
				return addRoomType((String)arguments.get(0), (Double)arguments.get(1), (Integer)arguments.get(2), (String)arguments.get(3));
			case RoomPackage.ROOM_TYPE_MANAGER___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_STRING:
				return updateRoomType((String)arguments.get(0), (String)arguments.get(1), (Double)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4));
			case RoomPackage.ROOM_TYPE_MANAGER___REMOVE_ROOM_TYPE__STRING:
				return removeRoomType((String)arguments.get(0));
			case RoomPackage.ROOM_TYPE_MANAGER___GET_ROOM_TYPES:
				return getRoomTypes();
		}
		return super.eInvoke(operationID, arguments);
	}

} //RoomTypeManagerImpl
