/**
 */
package ClassElements.Room;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.Room.RoomType#getName <em>Name</em>}</li>
 *   <li>{@link ClassElements.Room.RoomType#getPrice <em>Price</em>}</li>
 *   <li>{@link ClassElements.Room.RoomType#getNumOfBeds <em>Num Of Beds</em>}</li>
 *   <li>{@link ClassElements.Room.RoomType#getFeatures <em>Features</em>}</li>
 * </ul>
 *
 * @see ClassElements.Room.RoomPackage#getRoomType()
 * @model
 * @generated
 */
public interface RoomType extends IRoomType {

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see ClassElements.Room.RoomPackage#getRoomType_Name()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link ClassElements.Room.RoomType#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Price</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Price</em>' attribute.
	 * @see #setPrice(Double)
	 * @see ClassElements.Room.RoomPackage#getRoomType_Price()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Double getPrice();

	/**
	 * Sets the value of the '{@link ClassElements.Room.RoomType#getPrice <em>Price</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Price</em>' attribute.
	 * @see #getPrice()
	 * @generated
	 */
	void setPrice(Double value);

	/**
	 * Returns the value of the '<em><b>Num Of Beds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Num Of Beds</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Num Of Beds</em>' attribute.
	 * @see #setNumOfBeds(int)
	 * @see ClassElements.Room.RoomPackage#getRoomType_NumOfBeds()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getNumOfBeds();

	/**
	 * Sets the value of the '{@link ClassElements.Room.RoomType#getNumOfBeds <em>Num Of Beds</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Num Of Beds</em>' attribute.
	 * @see #getNumOfBeds()
	 * @generated
	 */
	void setNumOfBeds(int value);

	/**
	 * Returns the value of the '<em><b>Features</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Features</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Features</em>' attribute.
	 * @see #setFeatures(String)
	 * @see ClassElements.Room.RoomPackage#getRoomType_Features()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getFeatures();

	/**
	 * Sets the value of the '{@link ClassElements.Room.RoomType#getFeatures <em>Features</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Features</em>' attribute.
	 * @see #getFeatures()
	 * @generated
	 */
	void setFeatures(String value);
} // RoomType
