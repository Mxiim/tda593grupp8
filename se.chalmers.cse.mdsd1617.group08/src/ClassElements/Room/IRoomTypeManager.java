/**
 */
package ClassElements.Room;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IRoom Type Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ClassElements.Room.RoomPackage#getIRoomTypeManager()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IRoomTypeManager extends EObject {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" nameRequired="true" nameOrdered="false" priceRequired="true" priceOrdered="false" numOfBedsRequired="true" numOfBedsOrdered="false" featuresRequired="true" featuresOrdered="false"
	 * @generated
	 */
	Boolean addRoomType(String name, Double price, int numOfBeds, String features);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" oldNameRequired="true" oldNameOrdered="false" newNameRequired="true" newNameOrdered="false" newPriceRequired="true" newPriceOrdered="false" newNumOfBedsRequired="true" newNumOfBedsOrdered="false" newFeaturesRequired="true" newFeaturesOrdered="false"
	 * @generated
	 */
	Boolean updateRoomType(String oldName, String newName, Double newPrice, int newNumOfBeds, String newFeatures);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" nameRequired="true" nameOrdered="false"
	 * @generated
	 */
	Boolean removeRoomType(String name);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" ordered="false"
	 * @generated
	 */
	EList<IRoomType> getRoomTypes();
} // IRoomTypeManager
