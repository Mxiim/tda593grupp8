/**
 */
package ClassElements.HotelCustomer.Receptionist;

import ClassElements.HotelCustomer.HotelCustomerPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ClassElements.HotelCustomer.Receptionist.ReceptionistFactory
 * @model kind="package"
 * @generated
 */
public interface ReceptionistPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Receptionist";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///ClassElements/HotelCustomer/Receptionist.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ClassElements.HotelCustomer.Receptionist";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ReceptionistPackage eINSTANCE = ClassElements.HotelCustomer.Receptionist.impl.ReceptionistPackageImpl.init();

	/**
	 * The meta object id for the '{@link ClassElements.HotelCustomer.Receptionist.impl.ReceptionistImpl <em>Receptionist</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.HotelCustomer.Receptionist.impl.ReceptionistImpl
	 * @see ClassElements.HotelCustomer.Receptionist.impl.ReceptionistPackageImpl#getReceptionist()
	 * @generated
	 */
	int RECEPTIONIST = 0;

	/**
	 * The meta object id for the '{@link ClassElements.HotelCustomer.Receptionist.IReceptionistHandler <em>IReceptionist Handler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.HotelCustomer.Receptionist.IReceptionistHandler
	 * @see ClassElements.HotelCustomer.Receptionist.impl.ReceptionistPackageImpl#getIReceptionistHandler()
	 * @generated
	 */
	int IRECEPTIONIST_HANDLER = 1;

	/**
	 * The number of structural features of the '<em>IReceptionist Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_HANDLER_FEATURE_COUNT = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_HANDLER___INITIATE_BOOKING__STRING_STRING_STRING_STRING = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_HANDLER___GET_FREE_ROOMS__INT_STRING_STRING = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_HANDLER___CONFIRM_BOOKING__INT = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_HANDLER___ADD_ROOM_TO_BOOKING__STRING_INT = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_HANDLER___INITIATE_CHECKOUT__INT = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_HANDLER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_HANDLER___CHECK_IN_ROOM__STRING_INTEGER = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INTEGER;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_HANDLER___INITIATE_ROOM_CHECKOUT__INTEGER_INTEGER = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INTEGER_INTEGER;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_HANDLER___PAY_ROOM_DURING_CHECKOUT__INTEGER_STRING_STRING_INTEGER_INTEGER_STRING_STRING = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INTEGER_STRING_STRING_INTEGER_INTEGER_STRING_STRING;

	/**
	 * The operation id for the '<em>Get Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_HANDLER___GET_OCCUPIED_ROOMS__STRING = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_HANDLER___GET_CHECK_INS__STRING = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_HANDLER___GET_CHECK_INS__STRING_STRING = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Checkouts</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_HANDLER___GET_CHECKOUTS__STRING = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Checkouts</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_HANDLER___GET_CHECKOUTS__STRING_STRING = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Check In Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_HANDLER___CHECK_IN_BOOKING__INT = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Update Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_HANDLER___UPDATE_BOOKING__INT_STRING_STRING = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Update Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_HANDLER___UPDATE_ROOMS__IROOMTYPE_INT_STRING = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_HANDLER___CANCEL_BOOKING__INT = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>Get Confirmed Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_HANDLER___GET_CONFIRMED_BOOKINGS = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 9;

	/**
	 * The operation id for the '<em>Add Extra Cost</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_HANDLER___ADD_EXTRA_COST__INT_STRING_STRING_DOUBLE = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 10;

	/**
	 * The number of operations of the '<em>IReceptionist Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRECEPTIONIST_HANDLER_OPERATION_COUNT = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Ibookhandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST__IBOOKHANDLER = IRECEPTIONIST_HANDLER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Receptionist</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_FEATURE_COUNT = IRECEPTIONIST_HANDLER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___INITIATE_BOOKING__STRING_STRING_STRING_STRING = IRECEPTIONIST_HANDLER___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___GET_FREE_ROOMS__INT_STRING_STRING = IRECEPTIONIST_HANDLER___GET_FREE_ROOMS__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___CONFIRM_BOOKING__INT = IRECEPTIONIST_HANDLER___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___ADD_ROOM_TO_BOOKING__STRING_INT = IRECEPTIONIST_HANDLER___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___INITIATE_CHECKOUT__INT = IRECEPTIONIST_HANDLER___INITIATE_CHECKOUT__INT;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = IRECEPTIONIST_HANDLER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___CHECK_IN_ROOM__STRING_INTEGER = IRECEPTIONIST_HANDLER___CHECK_IN_ROOM__STRING_INTEGER;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___INITIATE_ROOM_CHECKOUT__INTEGER_INTEGER = IRECEPTIONIST_HANDLER___INITIATE_ROOM_CHECKOUT__INTEGER_INTEGER;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___PAY_ROOM_DURING_CHECKOUT__INTEGER_STRING_STRING_INTEGER_INTEGER_STRING_STRING = IRECEPTIONIST_HANDLER___PAY_ROOM_DURING_CHECKOUT__INTEGER_STRING_STRING_INTEGER_INTEGER_STRING_STRING;

	/**
	 * The operation id for the '<em>Get Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___GET_OCCUPIED_ROOMS__STRING = IRECEPTIONIST_HANDLER___GET_OCCUPIED_ROOMS__STRING;

	/**
	 * The operation id for the '<em>Get Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___GET_CHECK_INS__STRING = IRECEPTIONIST_HANDLER___GET_CHECK_INS__STRING;

	/**
	 * The operation id for the '<em>Get Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___GET_CHECK_INS__STRING_STRING = IRECEPTIONIST_HANDLER___GET_CHECK_INS__STRING_STRING;

	/**
	 * The operation id for the '<em>Get Checkouts</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___GET_CHECKOUTS__STRING = IRECEPTIONIST_HANDLER___GET_CHECKOUTS__STRING;

	/**
	 * The operation id for the '<em>Get Checkouts</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___GET_CHECKOUTS__STRING_STRING = IRECEPTIONIST_HANDLER___GET_CHECKOUTS__STRING_STRING;

	/**
	 * The operation id for the '<em>Check In Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___CHECK_IN_BOOKING__INT = IRECEPTIONIST_HANDLER___CHECK_IN_BOOKING__INT;

	/**
	 * The operation id for the '<em>Update Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___UPDATE_BOOKING__INT_STRING_STRING = IRECEPTIONIST_HANDLER___UPDATE_BOOKING__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Update Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___UPDATE_ROOMS__IROOMTYPE_INT_STRING = IRECEPTIONIST_HANDLER___UPDATE_ROOMS__IROOMTYPE_INT_STRING;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___CANCEL_BOOKING__INT = IRECEPTIONIST_HANDLER___CANCEL_BOOKING__INT;

	/**
	 * The operation id for the '<em>Get Confirmed Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___GET_CONFIRMED_BOOKINGS = IRECEPTIONIST_HANDLER___GET_CONFIRMED_BOOKINGS;

	/**
	 * The operation id for the '<em>Add Extra Cost</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST___ADD_EXTRA_COST__INT_STRING_STRING_DOUBLE = IRECEPTIONIST_HANDLER___ADD_EXTRA_COST__INT_STRING_STRING_DOUBLE;

	/**
	 * The number of operations of the '<em>Receptionist</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_OPERATION_COUNT = IRECEPTIONIST_HANDLER_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link ClassElements.HotelCustomer.Receptionist.Receptionist <em>Receptionist</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Receptionist</em>'.
	 * @see ClassElements.HotelCustomer.Receptionist.Receptionist
	 * @generated
	 */
	EClass getReceptionist();

	/**
	 * Returns the meta object for the reference '{@link ClassElements.HotelCustomer.Receptionist.Receptionist#getIbookhandler <em>Ibookhandler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ibookhandler</em>'.
	 * @see ClassElements.HotelCustomer.Receptionist.Receptionist#getIbookhandler()
	 * @see #getReceptionist()
	 * @generated
	 */
	EReference getReceptionist_Ibookhandler();

	/**
	 * Returns the meta object for class '{@link ClassElements.HotelCustomer.Receptionist.IReceptionistHandler <em>IReceptionist Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IReceptionist Handler</em>'.
	 * @see ClassElements.HotelCustomer.Receptionist.IReceptionistHandler
	 * @generated
	 */
	EClass getIReceptionistHandler();

	/**
	 * Returns the meta object for the '{@link ClassElements.HotelCustomer.Receptionist.IReceptionistHandler#getOccupiedRooms(java.lang.String) <em>Get Occupied Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Occupied Rooms</em>' operation.
	 * @see ClassElements.HotelCustomer.Receptionist.IReceptionistHandler#getOccupiedRooms(java.lang.String)
	 * @generated
	 */
	EOperation getIReceptionistHandler__GetOccupiedRooms__String();

	/**
	 * Returns the meta object for the '{@link ClassElements.HotelCustomer.Receptionist.IReceptionistHandler#getCheckIns(java.lang.String) <em>Get Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Check Ins</em>' operation.
	 * @see ClassElements.HotelCustomer.Receptionist.IReceptionistHandler#getCheckIns(java.lang.String)
	 * @generated
	 */
	EOperation getIReceptionistHandler__GetCheckIns__String();

	/**
	 * Returns the meta object for the '{@link ClassElements.HotelCustomer.Receptionist.IReceptionistHandler#getCheckIns(java.lang.String, java.lang.String) <em>Get Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Check Ins</em>' operation.
	 * @see ClassElements.HotelCustomer.Receptionist.IReceptionistHandler#getCheckIns(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIReceptionistHandler__GetCheckIns__String_String();

	/**
	 * Returns the meta object for the '{@link ClassElements.HotelCustomer.Receptionist.IReceptionistHandler#getCheckouts(java.lang.String) <em>Get Checkouts</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Checkouts</em>' operation.
	 * @see ClassElements.HotelCustomer.Receptionist.IReceptionistHandler#getCheckouts(java.lang.String)
	 * @generated
	 */
	EOperation getIReceptionistHandler__GetCheckouts__String();

	/**
	 * Returns the meta object for the '{@link ClassElements.HotelCustomer.Receptionist.IReceptionistHandler#getCheckouts(java.lang.String, java.lang.String) <em>Get Checkouts</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Checkouts</em>' operation.
	 * @see ClassElements.HotelCustomer.Receptionist.IReceptionistHandler#getCheckouts(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIReceptionistHandler__GetCheckouts__String_String();

	/**
	 * Returns the meta object for the '{@link ClassElements.HotelCustomer.Receptionist.IReceptionistHandler#checkInBooking(int) <em>Check In Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Booking</em>' operation.
	 * @see ClassElements.HotelCustomer.Receptionist.IReceptionistHandler#checkInBooking(int)
	 * @generated
	 */
	EOperation getIReceptionistHandler__CheckInBooking__int();

	/**
	 * Returns the meta object for the '{@link ClassElements.HotelCustomer.Receptionist.IReceptionistHandler#updateBooking(int, java.lang.String, java.lang.String) <em>Update Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Booking</em>' operation.
	 * @see ClassElements.HotelCustomer.Receptionist.IReceptionistHandler#updateBooking(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIReceptionistHandler__UpdateBooking__int_String_String();

	/**
	 * Returns the meta object for the '{@link ClassElements.HotelCustomer.Receptionist.IReceptionistHandler#updateRooms(ClassElements.Room.IRoomType, int, java.lang.String) <em>Update Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Rooms</em>' operation.
	 * @see ClassElements.HotelCustomer.Receptionist.IReceptionistHandler#updateRooms(ClassElements.Room.IRoomType, int, java.lang.String)
	 * @generated
	 */
	EOperation getIReceptionistHandler__UpdateRooms__IRoomType_int_String();

	/**
	 * Returns the meta object for the '{@link ClassElements.HotelCustomer.Receptionist.IReceptionistHandler#cancelBooking(int) <em>Cancel Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Booking</em>' operation.
	 * @see ClassElements.HotelCustomer.Receptionist.IReceptionistHandler#cancelBooking(int)
	 * @generated
	 */
	EOperation getIReceptionistHandler__CancelBooking__int();

	/**
	 * Returns the meta object for the '{@link ClassElements.HotelCustomer.Receptionist.IReceptionistHandler#getConfirmedBookings() <em>Get Confirmed Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Confirmed Bookings</em>' operation.
	 * @see ClassElements.HotelCustomer.Receptionist.IReceptionistHandler#getConfirmedBookings()
	 * @generated
	 */
	EOperation getIReceptionistHandler__GetConfirmedBookings();

	/**
	 * Returns the meta object for the '{@link ClassElements.HotelCustomer.Receptionist.IReceptionistHandler#addExtraCost(int, java.lang.String, java.lang.String, java.lang.Double) <em>Add Extra Cost</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra Cost</em>' operation.
	 * @see ClassElements.HotelCustomer.Receptionist.IReceptionistHandler#addExtraCost(int, java.lang.String, java.lang.String, java.lang.Double)
	 * @generated
	 */
	EOperation getIReceptionistHandler__AddExtraCost__int_String_String_Double();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ReceptionistFactory getReceptionistFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ClassElements.HotelCustomer.Receptionist.impl.ReceptionistImpl <em>Receptionist</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.HotelCustomer.Receptionist.impl.ReceptionistImpl
		 * @see ClassElements.HotelCustomer.Receptionist.impl.ReceptionistPackageImpl#getReceptionist()
		 * @generated
		 */
		EClass RECEPTIONIST = eINSTANCE.getReceptionist();

		/**
		 * The meta object literal for the '<em><b>Ibookhandler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RECEPTIONIST__IBOOKHANDLER = eINSTANCE.getReceptionist_Ibookhandler();

		/**
		 * The meta object literal for the '{@link ClassElements.HotelCustomer.Receptionist.IReceptionistHandler <em>IReceptionist Handler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.HotelCustomer.Receptionist.IReceptionistHandler
		 * @see ClassElements.HotelCustomer.Receptionist.impl.ReceptionistPackageImpl#getIReceptionistHandler()
		 * @generated
		 */
		EClass IRECEPTIONIST_HANDLER = eINSTANCE.getIReceptionistHandler();

		/**
		 * The meta object literal for the '<em><b>Get Occupied Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST_HANDLER___GET_OCCUPIED_ROOMS__STRING = eINSTANCE.getIReceptionistHandler__GetOccupiedRooms__String();

		/**
		 * The meta object literal for the '<em><b>Get Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST_HANDLER___GET_CHECK_INS__STRING = eINSTANCE.getIReceptionistHandler__GetCheckIns__String();

		/**
		 * The meta object literal for the '<em><b>Get Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST_HANDLER___GET_CHECK_INS__STRING_STRING = eINSTANCE.getIReceptionistHandler__GetCheckIns__String_String();

		/**
		 * The meta object literal for the '<em><b>Get Checkouts</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST_HANDLER___GET_CHECKOUTS__STRING = eINSTANCE.getIReceptionistHandler__GetCheckouts__String();

		/**
		 * The meta object literal for the '<em><b>Get Checkouts</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST_HANDLER___GET_CHECKOUTS__STRING_STRING = eINSTANCE.getIReceptionistHandler__GetCheckouts__String_String();

		/**
		 * The meta object literal for the '<em><b>Check In Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST_HANDLER___CHECK_IN_BOOKING__INT = eINSTANCE.getIReceptionistHandler__CheckInBooking__int();

		/**
		 * The meta object literal for the '<em><b>Update Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST_HANDLER___UPDATE_BOOKING__INT_STRING_STRING = eINSTANCE.getIReceptionistHandler__UpdateBooking__int_String_String();

		/**
		 * The meta object literal for the '<em><b>Update Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST_HANDLER___UPDATE_ROOMS__IROOMTYPE_INT_STRING = eINSTANCE.getIReceptionistHandler__UpdateRooms__IRoomType_int_String();

		/**
		 * The meta object literal for the '<em><b>Cancel Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST_HANDLER___CANCEL_BOOKING__INT = eINSTANCE.getIReceptionistHandler__CancelBooking__int();

		/**
		 * The meta object literal for the '<em><b>Get Confirmed Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST_HANDLER___GET_CONFIRMED_BOOKINGS = eINSTANCE.getIReceptionistHandler__GetConfirmedBookings();

		/**
		 * The meta object literal for the '<em><b>Add Extra Cost</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRECEPTIONIST_HANDLER___ADD_EXTRA_COST__INT_STRING_STRING_DOUBLE = eINSTANCE.getIReceptionistHandler__AddExtraCost__int_String_String_Double();

	}

} //ReceptionistPackage
