/**
 */
package ClassElements.HotelCustomer.Receptionist;

import ClassElements.Booking.IBooking;
import ClassElements.HotelCustomer.IHotelCustomerProvides;
import ClassElements.Room.IRoom;
import ClassElements.Room.IRoomType;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IReceptionist Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ClassElements.HotelCustomer.Receptionist.ReceptionistPackage#getIReceptionistHandler()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IReceptionistHandler extends IHotelCustomerProvides {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model mapType="ClassElements.Booking.BookingAndRoomHashMap<ClassElements.Room.IRoom, org.eclipse.emf.ecore.EIntegerObject>" ordered="false" dateRequired="true" dateOrdered="false"
	 * @generated
	 */
	EMap<IRoom, Integer> getOccupiedRooms(String date);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" dateRequired="true" dateOrdered="false"
	 * @generated
	 */
	EList<Integer> getCheckIns(String date);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" startDateRequired="true" startDateOrdered="false" endDateRequired="true" endDateOrdered="false"
	 * @generated
	 */
	EList<Integer> getCheckIns(String startDate, String endDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" dateRequired="true" dateOrdered="false"
	 * @generated
	 */
	EList<Integer> getCheckouts(String date);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" startDateRequired="true" startDateOrdered="false" endDateRequired="true" endDateOrdered="false"
	 * @generated
	 */
	EList<Integer> getCheckouts(String startDate, String endDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	void checkInBooking(int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bookingIDRequired="true" bookingIDOrdered="false" startDateRequired="true" startDateOrdered="false" endDateRequired="true" endDateOrdered="false"
	 * @generated
	 */
	void updateBooking(int bookingID, String startDate, String endDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomTypeRequired="true" roomTypeOrdered="false" newCountRequired="true" newCountOrdered="false" bookIDRequired="true" bookIDOrdered="false"
	 * @generated
	 */
	void updateRooms(IRoomType roomType, int newCount, String bookID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	void cancelBooking(int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" ordered="false"
	 * @generated
	 */
	EList<IBooking> getConfirmedBookings();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookIDRequired="true" bookIDOrdered="false" roomNumberRequired="true" roomNumberOrdered="false" nameRequired="true" nameOrdered="false" priceRequired="true" priceOrdered="false"
	 * @generated
	 */
	Boolean addExtraCost(int bookID, String roomNumber, String name, Double price);

} // IReceptionistHandler
