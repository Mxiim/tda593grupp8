/**
 */
package ClassElements.HotelCustomer.Receptionist;

import ClassElements.Booking.IBookHandler;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Receptionist</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.HotelCustomer.Receptionist.Receptionist#getIbookhandler <em>Ibookhandler</em>}</li>
 * </ul>
 *
 * @see ClassElements.HotelCustomer.Receptionist.ReceptionistPackage#getReceptionist()
 * @model
 * @generated
 */
public interface Receptionist extends IReceptionistHandler {
	/**
	 * Returns the value of the '<em><b>Ibookhandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ibookhandler</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ibookhandler</em>' reference.
	 * @see #setIbookhandler(IBookHandler)
	 * @see ClassElements.HotelCustomer.Receptionist.ReceptionistPackage#getReceptionist_Ibookhandler()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IBookHandler getIbookhandler();

	/**
	 * Sets the value of the '{@link ClassElements.HotelCustomer.Receptionist.Receptionist#getIbookhandler <em>Ibookhandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ibookhandler</em>' reference.
	 * @see #getIbookhandler()
	 * @generated
	 */
	void setIbookhandler(IBookHandler value);

} // Receptionist
