/**
 */
package ClassElements.HotelCustomer.Receptionist.impl;

import ClassElements.Booking.IBookHandler;

import ClassElements.Booking.IBooking;
import ClassElements.Booking.impl.BookingFactoryImpl;
import ClassElements.HotelCustomer.FreeRoomTypesDTO;
import ClassElements.HotelCustomer.Receptionist.Receptionist;
import ClassElements.HotelCustomer.Receptionist.ReceptionistPackage;
import ClassElements.Room.IRoom;
import ClassElements.Room.IRoomType;
import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Receptionist</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.HotelCustomer.Receptionist.impl.ReceptionistImpl#getIbookhandler <em>Ibookhandler</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReceptionistImpl extends MinimalEObjectImpl.Container implements Receptionist {
	/**
	 * The cached value of the '{@link #getIbookhandler() <em>Ibookhandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIbookhandler()
	 * @generated NOT
	 * @ordered
	 */
	protected IBookHandler ibookhandler = (IBookHandler) BookingFactoryImpl.init().createBookHandler();
	double amount = 0;
	double amountRoom = 0;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReceptionistImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ReceptionistPackage.Literals.RECEPTIONIST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IBookHandler getIbookhandler() {
		if (ibookhandler != null && ibookhandler.eIsProxy()) {
			InternalEObject oldIbookhandler = (InternalEObject)ibookhandler;
			ibookhandler = (IBookHandler)eResolveProxy(oldIbookhandler);
			if (ibookhandler != oldIbookhandler) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ReceptionistPackage.RECEPTIONIST__IBOOKHANDLER, oldIbookhandler, ibookhandler));
			}
		}
		return ibookhandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IBookHandler basicGetIbookhandler() {
		return ibookhandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIbookhandler(IBookHandler newIbookhandler) {
		IBookHandler oldIbookhandler = ibookhandler;
		ibookhandler = newIbookhandler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ReceptionistPackage.RECEPTIONIST__IBOOKHANDLER, oldIbookhandler, ibookhandler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int initiateBooking(String firstName, String startDate, String endDate, String lastName) {
		return this.getIbookhandler().initiateBooking(firstName, lastName, startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<FreeRoomTypesDTO> getFreeRooms(int numBeds, String startDate, String endDate) {
		return this.getIbookhandler().getFreeRooms(numBeds, startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean confirmBooking(int bookingID) {
		return this.getIbookhandler().confirmBooking(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean addRoomToBooking(String roomTypeDesc, int bookID) {
		return this.getIbookhandler().addRoomToBooking(roomTypeDesc, bookID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Double initiateCheckout(int bookID) {
		this.amount = this.getIbookhandler().initiateCheckout(bookID);
		return amount;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean payDuringCheckout(String cc, String ccv, int expMonth, int expYear, String firstName, String lastName) {
		return this.getIbookhandler().payDuringCheckout(cc, ccv, expMonth, expYear, firstName, lastName, amount);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Integer checkInRoom(String roomTypeName, Integer bookID) {
		return this.getIbookhandler().checkInRoom(roomTypeName, bookID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateRoomCheckout(Integer bookID, Integer roomNumber) {
		this.amountRoom = this.getIbookhandler().initiateRoomCheckout(bookID, roomNumber);
		return this.amountRoom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payRoomDuringCheckout(Integer roomNumber, String cc, String ccv, Integer expMonth, Integer expYear, String firstName, String lastName) {
		return this.getIbookhandler().payRoomDuringCheckout(roomNumber, cc, ccv, expMonth, expYear, firstName, lastName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EMap<IRoom, Integer> getOccupiedRooms(String date) {
		return this.getIbookhandler().getOccupiedRooms(date);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> getCheckIns(String date) {
		return this.getIbookhandler().getCheckIns(date);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> getCheckIns(String startDate, String endDate) {
		return this.getIbookhandler().getCheckIns(startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> getCheckouts(String date) {
		return this.getIbookhandler().getCheckouts(date);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> getCheckouts(String startDate, String endDate) {
		return this.getIbookhandler().getCheckouts(startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void checkInBooking(int bookingID) {
		this.getIbookhandler().checkInBooking(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void updateBooking(int bookingID, String startDate, String endDate) {
		this.getIbookhandler().updateBooking(bookingID, startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void updateRooms(IRoomType roomType, int newCount, String bookID) {
		this.getIbookhandler().updateRooms(roomType, newCount, bookID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void cancelBooking(int bookingID) {
		this.getIbookhandler().cancelBooking(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<IBooking> getConfirmedBookings() {
		return this.getIbookhandler().getConfirmedBookings();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean addExtraCost(int bookID, String roomNumber, String name, Double price) {
		return this.getIbookhandler().addExtraCost(bookID, roomNumber, name, price);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ReceptionistPackage.RECEPTIONIST__IBOOKHANDLER:
				if (resolve) return getIbookhandler();
				return basicGetIbookhandler();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ReceptionistPackage.RECEPTIONIST__IBOOKHANDLER:
				setIbookhandler((IBookHandler)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ReceptionistPackage.RECEPTIONIST__IBOOKHANDLER:
				setIbookhandler((IBookHandler)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ReceptionistPackage.RECEPTIONIST__IBOOKHANDLER:
				return ibookhandler != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ReceptionistPackage.RECEPTIONIST___INITIATE_BOOKING__STRING_STRING_STRING_STRING:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case ReceptionistPackage.RECEPTIONIST___GET_FREE_ROOMS__INT_STRING_STRING:
				return getFreeRooms((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case ReceptionistPackage.RECEPTIONIST___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case ReceptionistPackage.RECEPTIONIST___ADD_ROOM_TO_BOOKING__STRING_INT:
				return addRoomToBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case ReceptionistPackage.RECEPTIONIST___INITIATE_CHECKOUT__INT:
				return initiateCheckout((Integer)arguments.get(0));
			case ReceptionistPackage.RECEPTIONIST___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING:
				return payDuringCheckout((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5));
			case ReceptionistPackage.RECEPTIONIST___CHECK_IN_ROOM__STRING_INTEGER:
				return checkInRoom((String)arguments.get(0), (Integer)arguments.get(1));
			case ReceptionistPackage.RECEPTIONIST___INITIATE_ROOM_CHECKOUT__INTEGER_INTEGER:
				return initiateRoomCheckout((Integer)arguments.get(0), (Integer)arguments.get(1));
			case ReceptionistPackage.RECEPTIONIST___PAY_ROOM_DURING_CHECKOUT__INTEGER_STRING_STRING_INTEGER_INTEGER_STRING_STRING:
				return payRoomDuringCheckout((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3), (Integer)arguments.get(4), (String)arguments.get(5), (String)arguments.get(6));
			case ReceptionistPackage.RECEPTIONIST___GET_OCCUPIED_ROOMS__STRING:
				return getOccupiedRooms((String)arguments.get(0));
			case ReceptionistPackage.RECEPTIONIST___GET_CHECK_INS__STRING:
				return getCheckIns((String)arguments.get(0));
			case ReceptionistPackage.RECEPTIONIST___GET_CHECK_INS__STRING_STRING:
				return getCheckIns((String)arguments.get(0), (String)arguments.get(1));
			case ReceptionistPackage.RECEPTIONIST___GET_CHECKOUTS__STRING:
				return getCheckouts((String)arguments.get(0));
			case ReceptionistPackage.RECEPTIONIST___GET_CHECKOUTS__STRING_STRING:
				return getCheckouts((String)arguments.get(0), (String)arguments.get(1));
			case ReceptionistPackage.RECEPTIONIST___CHECK_IN_BOOKING__INT:
				checkInBooking((Integer)arguments.get(0));
				return null;
			case ReceptionistPackage.RECEPTIONIST___UPDATE_BOOKING__INT_STRING_STRING:
				updateBooking((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
				return null;
			case ReceptionistPackage.RECEPTIONIST___UPDATE_ROOMS__IROOMTYPE_INT_STRING:
				updateRooms((IRoomType)arguments.get(0), (Integer)arguments.get(1), (String)arguments.get(2));
				return null;
			case ReceptionistPackage.RECEPTIONIST___CANCEL_BOOKING__INT:
				cancelBooking((Integer)arguments.get(0));
				return null;
			case ReceptionistPackage.RECEPTIONIST___GET_CONFIRMED_BOOKINGS:
				return getConfirmedBookings();
			case ReceptionistPackage.RECEPTIONIST___ADD_EXTRA_COST__INT_STRING_STRING_DOUBLE:
				return addExtraCost((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Double)arguments.get(3));
		}
		return super.eInvoke(operationID, arguments);
	}

} //ReceptionistImpl
