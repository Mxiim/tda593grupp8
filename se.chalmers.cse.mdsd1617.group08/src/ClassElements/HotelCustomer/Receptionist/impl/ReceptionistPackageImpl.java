/**
 */
package ClassElements.HotelCustomer.Receptionist.impl;

import ClassElements.Admin.AdminPackage;

import ClassElements.Admin.impl.AdminPackageImpl;

import ClassElements.Booking.BookingPackage;

import ClassElements.Booking.impl.BookingPackageImpl;

import ClassElements.HotelCustomer.ExternalSupply.ExternalSupplyPackage;

import ClassElements.HotelCustomer.ExternalSupply.impl.ExternalSupplyPackageImpl;

import ClassElements.HotelCustomer.HotelCustomerPackage;

import ClassElements.HotelCustomer.Receptionist.IReceptionistHandler;
import ClassElements.HotelCustomer.Receptionist.Receptionist;
import ClassElements.HotelCustomer.Receptionist.ReceptionistFactory;
import ClassElements.HotelCustomer.Receptionist.ReceptionistPackage;

import ClassElements.HotelCustomer.impl.HotelCustomerPackageImpl;

import ClassElements.Room.RoomPackage;

import ClassElements.Room.impl.RoomPackageImpl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ReceptionistPackageImpl extends EPackageImpl implements ReceptionistPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass receptionistEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iReceptionistHandlerEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ClassElements.HotelCustomer.Receptionist.ReceptionistPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ReceptionistPackageImpl() {
		super(eNS_URI, ReceptionistFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ReceptionistPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ReceptionistPackage init() {
		if (isInited) return (ReceptionistPackage)EPackage.Registry.INSTANCE.getEPackage(ReceptionistPackage.eNS_URI);

		// Obtain or create and register package
		ReceptionistPackageImpl theReceptionistPackage = (ReceptionistPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ReceptionistPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ReceptionistPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		HotelCustomerPackageImpl theHotelCustomerPackage = (HotelCustomerPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(HotelCustomerPackage.eNS_URI) instanceof HotelCustomerPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(HotelCustomerPackage.eNS_URI) : HotelCustomerPackage.eINSTANCE);
		ExternalSupplyPackageImpl theExternalSupplyPackage = (ExternalSupplyPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExternalSupplyPackage.eNS_URI) instanceof ExternalSupplyPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExternalSupplyPackage.eNS_URI) : ExternalSupplyPackage.eINSTANCE);
		BookingPackageImpl theBookingPackage = (BookingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) instanceof BookingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) : BookingPackage.eINSTANCE);
		RoomPackageImpl theRoomPackage = (RoomPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) instanceof RoomPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) : RoomPackage.eINSTANCE);
		AdminPackageImpl theAdminPackage = (AdminPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AdminPackage.eNS_URI) instanceof AdminPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AdminPackage.eNS_URI) : AdminPackage.eINSTANCE);

		// Create package meta-data objects
		theReceptionistPackage.createPackageContents();
		theHotelCustomerPackage.createPackageContents();
		theExternalSupplyPackage.createPackageContents();
		theBookingPackage.createPackageContents();
		theRoomPackage.createPackageContents();
		theAdminPackage.createPackageContents();

		// Initialize created meta-data
		theReceptionistPackage.initializePackageContents();
		theHotelCustomerPackage.initializePackageContents();
		theExternalSupplyPackage.initializePackageContents();
		theBookingPackage.initializePackageContents();
		theRoomPackage.initializePackageContents();
		theAdminPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theReceptionistPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ReceptionistPackage.eNS_URI, theReceptionistPackage);
		return theReceptionistPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReceptionist() {
		return receptionistEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReceptionist_Ibookhandler() {
		return (EReference)receptionistEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIReceptionistHandler() {
		return iReceptionistHandlerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIReceptionistHandler__GetOccupiedRooms__String() {
		return iReceptionistHandlerEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIReceptionistHandler__GetCheckIns__String() {
		return iReceptionistHandlerEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIReceptionistHandler__GetCheckIns__String_String() {
		return iReceptionistHandlerEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIReceptionistHandler__GetCheckouts__String() {
		return iReceptionistHandlerEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIReceptionistHandler__GetCheckouts__String_String() {
		return iReceptionistHandlerEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIReceptionistHandler__CheckInBooking__int() {
		return iReceptionistHandlerEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIReceptionistHandler__UpdateBooking__int_String_String() {
		return iReceptionistHandlerEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIReceptionistHandler__UpdateRooms__IRoomType_int_String() {
		return iReceptionistHandlerEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIReceptionistHandler__CancelBooking__int() {
		return iReceptionistHandlerEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIReceptionistHandler__GetConfirmedBookings() {
		return iReceptionistHandlerEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIReceptionistHandler__AddExtraCost__int_String_String_Double() {
		return iReceptionistHandlerEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReceptionistFactory getReceptionistFactory() {
		return (ReceptionistFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		receptionistEClass = createEClass(RECEPTIONIST);
		createEReference(receptionistEClass, RECEPTIONIST__IBOOKHANDLER);

		iReceptionistHandlerEClass = createEClass(IRECEPTIONIST_HANDLER);
		createEOperation(iReceptionistHandlerEClass, IRECEPTIONIST_HANDLER___GET_OCCUPIED_ROOMS__STRING);
		createEOperation(iReceptionistHandlerEClass, IRECEPTIONIST_HANDLER___GET_CHECK_INS__STRING);
		createEOperation(iReceptionistHandlerEClass, IRECEPTIONIST_HANDLER___GET_CHECK_INS__STRING_STRING);
		createEOperation(iReceptionistHandlerEClass, IRECEPTIONIST_HANDLER___GET_CHECKOUTS__STRING);
		createEOperation(iReceptionistHandlerEClass, IRECEPTIONIST_HANDLER___GET_CHECKOUTS__STRING_STRING);
		createEOperation(iReceptionistHandlerEClass, IRECEPTIONIST_HANDLER___CHECK_IN_BOOKING__INT);
		createEOperation(iReceptionistHandlerEClass, IRECEPTIONIST_HANDLER___UPDATE_BOOKING__INT_STRING_STRING);
		createEOperation(iReceptionistHandlerEClass, IRECEPTIONIST_HANDLER___UPDATE_ROOMS__IROOMTYPE_INT_STRING);
		createEOperation(iReceptionistHandlerEClass, IRECEPTIONIST_HANDLER___CANCEL_BOOKING__INT);
		createEOperation(iReceptionistHandlerEClass, IRECEPTIONIST_HANDLER___GET_CONFIRMED_BOOKINGS);
		createEOperation(iReceptionistHandlerEClass, IRECEPTIONIST_HANDLER___ADD_EXTRA_COST__INT_STRING_STRING_DOUBLE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		BookingPackage theBookingPackage = (BookingPackage)EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI);
		HotelCustomerPackage theHotelCustomerPackage = (HotelCustomerPackage)EPackage.Registry.INSTANCE.getEPackage(HotelCustomerPackage.eNS_URI);
		RoomPackage theRoomPackage = (RoomPackage)EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		receptionistEClass.getESuperTypes().add(this.getIReceptionistHandler());
		iReceptionistHandlerEClass.getESuperTypes().add(theHotelCustomerPackage.getIHotelCustomerProvides());

		// Initialize classes, features, and operations; add parameters
		initEClass(receptionistEClass, Receptionist.class, "Receptionist", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReceptionist_Ibookhandler(), theBookingPackage.getIBookHandler(), null, "ibookhandler", null, 1, 1, Receptionist.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iReceptionistHandlerEClass, IReceptionistHandler.class, "IReceptionistHandler", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getIReceptionistHandler__GetOccupiedRooms__String(), theBookingPackage.getBookingAndRoomHashMap(), "getOccupiedRooms", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "date", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIReceptionistHandler__GetCheckIns__String(), ecorePackage.getEIntegerObject(), "getCheckIns", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "date", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIReceptionistHandler__GetCheckIns__String_String(), ecorePackage.getEIntegerObject(), "getCheckIns", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIReceptionistHandler__GetCheckouts__String(), ecorePackage.getEIntegerObject(), "getCheckouts", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "date", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIReceptionistHandler__GetCheckouts__String_String(), ecorePackage.getEIntegerObject(), "getCheckouts", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIReceptionistHandler__CheckInBooking__int(), null, "checkInBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIReceptionistHandler__UpdateBooking__int_String_String(), null, "updateBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIReceptionistHandler__UpdateRooms__IRoomType_int_String(), null, "updateRooms", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theRoomPackage.getIRoomType(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "newCount", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "bookID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIReceptionistHandler__CancelBooking__int(), null, "cancelBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIReceptionistHandler__GetConfirmedBookings(), theBookingPackage.getIBooking(), "getConfirmedBookings", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIReceptionistHandler__AddExtraCost__int_String_String_Double(), ecorePackage.getEBooleanObject(), "addExtraCost", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDoubleObject(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);
	}

} //ReceptionistPackageImpl
