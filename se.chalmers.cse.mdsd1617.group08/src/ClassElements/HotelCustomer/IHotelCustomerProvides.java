/**
 */
package ClassElements.HotelCustomer;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IHotel Customer Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ClassElements.HotelCustomer.HotelCustomerPackage#getIHotelCustomerProvides()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IHotelCustomerProvides extends EObject {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" firstNameRequired="true" firstNameOrdered="false" startDateRequired="true" startDateOrdered="false" endDateRequired="true" endDateOrdered="false" lastNameRequired="true" lastNameOrdered="false"
	 * @generated
	 */
	int initiateBooking(String firstName, String startDate, String endDate, String lastName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" numBedsRequired="true" numBedsOrdered="false" startDateRequired="true" startDateOrdered="false" endDateRequired="true" endDateOrdered="false"
	 * @generated
	 */
	EList<FreeRoomTypesDTO> getFreeRooms(int numBeds, String startDate, String endDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	Boolean confirmBooking(int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeDescRequired="true" roomTypeDescOrdered="false" bookIDRequired="true" bookIDOrdered="false"
	 * @generated
	 */
	Boolean addRoomToBooking(String roomTypeDesc, int bookID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookIDRequired="true" bookIDOrdered="false"
	 * @generated
	 */
	Double initiateCheckout(int bookID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" ccRequired="true" ccOrdered="false" ccvRequired="true" ccvOrdered="false" expMonthRequired="true" expMonthOrdered="false" expYearRequired="true" expYearOrdered="false" firstNameRequired="true" firstNameOrdered="false" lastNameRequired="true" lastNameOrdered="false"
	 * @generated
	 */
	Boolean payDuringCheckout(String cc, String ccv, int expMonth, int expYear, String firstName, String lastName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeNameRequired="true" roomTypeNameOrdered="false" bookIDRequired="true" bookIDOrdered="false"
	 * @generated
	 */
	Integer checkInRoom(String roomTypeName, Integer bookID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookIDRequired="true" bookIDOrdered="false" roomNumberRequired="true" roomNumberOrdered="false"
	 * @generated
	 */
	double initiateRoomCheckout(Integer bookID, Integer roomNumber);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNumberRequired="true" roomNumberOrdered="false" ccRequired="true" ccOrdered="false" ccvRequired="true" ccvOrdered="false" expMonthRequired="true" expMonthOrdered="false" expYearRequired="true" expYearOrdered="false" firstNameRequired="true" firstNameOrdered="false" lastNameRequired="true" lastNameOrdered="false"
	 * @generated
	 */
	boolean payRoomDuringCheckout(Integer roomNumber, String cc, String ccv, Integer expMonth, Integer expYear, String firstName, String lastName);
} // IHotelCustomerProvides
