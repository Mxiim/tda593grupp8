/**
 */
package ClassElements.HotelCustomer.ExternalSupply.impl;

import ClassElements.HotelCustomer.ExternalSupply.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ExternalSupplyFactoryImpl extends EFactoryImpl implements ExternalSupplyFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ExternalSupplyFactory init() {
		try {
			ExternalSupplyFactory theExternalSupplyFactory = (ExternalSupplyFactory)EPackage.Registry.INSTANCE.getEFactory(ExternalSupplyPackage.eNS_URI);
			if (theExternalSupplyFactory != null) {
				return theExternalSupplyFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ExternalSupplyFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExternalSupplyFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ExternalSupplyPackage.EXTERNAL_SUPPLY: return createExternalSupply();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExternalSupply createExternalSupply() {
		ExternalSupplyImpl externalSupply = new ExternalSupplyImpl();
		return externalSupply;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExternalSupplyPackage getExternalSupplyPackage() {
		return (ExternalSupplyPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ExternalSupplyPackage getPackage() {
		return ExternalSupplyPackage.eINSTANCE;
	}

} //ExternalSupplyFactoryImpl
