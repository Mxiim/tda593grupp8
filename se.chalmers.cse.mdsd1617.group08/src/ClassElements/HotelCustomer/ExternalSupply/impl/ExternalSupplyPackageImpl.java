/**
 */
package ClassElements.HotelCustomer.ExternalSupply.impl;

import ClassElements.Admin.AdminPackage;

import ClassElements.Admin.impl.AdminPackageImpl;

import ClassElements.Booking.BookingPackage;

import ClassElements.Booking.impl.BookingPackageImpl;

import ClassElements.HotelCustomer.ExternalSupply.ExternalSupply;
import ClassElements.HotelCustomer.ExternalSupply.ExternalSupplyFactory;
import ClassElements.HotelCustomer.ExternalSupply.ExternalSupplyPackage;
import ClassElements.HotelCustomer.ExternalSupply.IExternalSupplyHandler;

import ClassElements.HotelCustomer.HotelCustomerPackage;

import ClassElements.HotelCustomer.Receptionist.ReceptionistPackage;

import ClassElements.HotelCustomer.Receptionist.impl.ReceptionistPackageImpl;

import ClassElements.HotelCustomer.impl.HotelCustomerPackageImpl;

import ClassElements.Room.RoomPackage;

import ClassElements.Room.impl.RoomPackageImpl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ExternalSupplyPackageImpl extends EPackageImpl implements ExternalSupplyPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass externalSupplyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iExternalSupplyHandlerEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ClassElements.HotelCustomer.ExternalSupply.ExternalSupplyPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ExternalSupplyPackageImpl() {
		super(eNS_URI, ExternalSupplyFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ExternalSupplyPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ExternalSupplyPackage init() {
		if (isInited) return (ExternalSupplyPackage)EPackage.Registry.INSTANCE.getEPackage(ExternalSupplyPackage.eNS_URI);

		// Obtain or create and register package
		ExternalSupplyPackageImpl theExternalSupplyPackage = (ExternalSupplyPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ExternalSupplyPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ExternalSupplyPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		HotelCustomerPackageImpl theHotelCustomerPackage = (HotelCustomerPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(HotelCustomerPackage.eNS_URI) instanceof HotelCustomerPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(HotelCustomerPackage.eNS_URI) : HotelCustomerPackage.eINSTANCE);
		ReceptionistPackageImpl theReceptionistPackage = (ReceptionistPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ReceptionistPackage.eNS_URI) instanceof ReceptionistPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ReceptionistPackage.eNS_URI) : ReceptionistPackage.eINSTANCE);
		BookingPackageImpl theBookingPackage = (BookingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) instanceof BookingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) : BookingPackage.eINSTANCE);
		RoomPackageImpl theRoomPackage = (RoomPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) instanceof RoomPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) : RoomPackage.eINSTANCE);
		AdminPackageImpl theAdminPackage = (AdminPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AdminPackage.eNS_URI) instanceof AdminPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AdminPackage.eNS_URI) : AdminPackage.eINSTANCE);

		// Create package meta-data objects
		theExternalSupplyPackage.createPackageContents();
		theHotelCustomerPackage.createPackageContents();
		theReceptionistPackage.createPackageContents();
		theBookingPackage.createPackageContents();
		theRoomPackage.createPackageContents();
		theAdminPackage.createPackageContents();

		// Initialize created meta-data
		theExternalSupplyPackage.initializePackageContents();
		theHotelCustomerPackage.initializePackageContents();
		theReceptionistPackage.initializePackageContents();
		theBookingPackage.initializePackageContents();
		theRoomPackage.initializePackageContents();
		theAdminPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theExternalSupplyPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ExternalSupplyPackage.eNS_URI, theExternalSupplyPackage);
		return theExternalSupplyPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExternalSupply() {
		return externalSupplyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExternalSupply_Ibookhandler() {
		return (EReference)externalSupplyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIExternalSupplyHandler() {
		return iExternalSupplyHandlerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExternalSupplyFactory getExternalSupplyFactory() {
		return (ExternalSupplyFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		externalSupplyEClass = createEClass(EXTERNAL_SUPPLY);
		createEReference(externalSupplyEClass, EXTERNAL_SUPPLY__IBOOKHANDLER);

		iExternalSupplyHandlerEClass = createEClass(IEXTERNAL_SUPPLY_HANDLER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		BookingPackage theBookingPackage = (BookingPackage)EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI);
		HotelCustomerPackage theHotelCustomerPackage = (HotelCustomerPackage)EPackage.Registry.INSTANCE.getEPackage(HotelCustomerPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		externalSupplyEClass.getESuperTypes().add(this.getIExternalSupplyHandler());
		iExternalSupplyHandlerEClass.getESuperTypes().add(theHotelCustomerPackage.getIHotelCustomerProvides());

		// Initialize classes, features, and operations; add parameters
		initEClass(externalSupplyEClass, ExternalSupply.class, "ExternalSupply", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getExternalSupply_Ibookhandler(), theBookingPackage.getIBookHandler(), null, "ibookhandler", null, 1, 1, ExternalSupply.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iExternalSupplyHandlerEClass, IExternalSupplyHandler.class, "IExternalSupplyHandler", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
	}

} //ExternalSupplyPackageImpl
