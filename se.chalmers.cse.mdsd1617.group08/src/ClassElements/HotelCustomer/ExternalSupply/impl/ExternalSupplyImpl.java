/**
 */
package ClassElements.HotelCustomer.ExternalSupply.impl;

import ClassElements.Booking.IBookHandler;
import ClassElements.Booking.impl.BookingFactoryImpl;
import ClassElements.HotelCustomer.ExternalSupply.ExternalSupply;
import ClassElements.HotelCustomer.ExternalSupply.ExternalSupplyPackage;

import ClassElements.HotelCustomer.FreeRoomTypesDTO;
import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>External Supply</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.HotelCustomer.ExternalSupply.impl.ExternalSupplyImpl#getIbookhandler <em>Ibookhandler</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExternalSupplyImpl extends MinimalEObjectImpl.Container implements ExternalSupply {
	/**
	 * The cached value of the '{@link #getIbookhandler() <em>Ibookhandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIbookhandler()
	 * @generated NOT
	 * @ordered
	 */
	protected IBookHandler ibookhandler = (IBookHandler) BookingFactoryImpl.init().createBookHandler();
	double amount = 0;
	double roomAmount = 0;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected ExternalSupplyImpl() {
		super();
//		this.ibookhandler = this.getIbookhandler();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExternalSupplyPackage.Literals.EXTERNAL_SUPPLY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IBookHandler getIbookhandler() {
		if (ibookhandler != null && ibookhandler.eIsProxy()) {
			InternalEObject oldIbookhandler = (InternalEObject)ibookhandler;
			ibookhandler = (IBookHandler)eResolveProxy(oldIbookhandler);
			if (ibookhandler != oldIbookhandler) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ExternalSupplyPackage.EXTERNAL_SUPPLY__IBOOKHANDLER, oldIbookhandler, ibookhandler));
			}
		}
		return ibookhandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IBookHandler basicGetIbookhandler() {
		return ibookhandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIbookhandler(IBookHandler newIbookhandler) {
		IBookHandler oldIbookhandler = ibookhandler;
		ibookhandler = newIbookhandler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExternalSupplyPackage.EXTERNAL_SUPPLY__IBOOKHANDLER, oldIbookhandler, ibookhandler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int initiateBooking(String firstName, String startDate, String endDate, String lastName) {
		return this.getIbookhandler().initiateBooking(firstName, lastName, startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<FreeRoomTypesDTO> getFreeRooms(int numBeds, String startDate, String endDate) {
		return this.getIbookhandler().getFreeRooms(numBeds, startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean confirmBooking(int bookingID) {
		return this.getIbookhandler().confirmBooking(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean addRoomToBooking(String roomTypeDesc, int bookID) {
		return this.getIbookhandler().addRoomToBooking(roomTypeDesc, bookID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Double initiateCheckout(int bookID) {
		this.amount = this.getIbookhandler().initiateCheckout(bookID);
		return this.amount;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean payDuringCheckout(String cc, String ccv, int expMonth, int expYear, String firstName, String lastName) {
		return this.getIbookhandler().payDuringCheckout(cc, ccv, expMonth, expYear, firstName, lastName, this.amount);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Integer checkInRoom(String roomTypeName, Integer bookID) {
		return this.getIbookhandler().checkInRoom(roomTypeName, bookID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateRoomCheckout(Integer bookID, Integer roomNumber) {
		this.roomAmount = this.getIbookhandler().initiateRoomCheckout(bookID, roomNumber);
		return this.roomAmount;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payRoomDuringCheckout(Integer roomNumber, String cc, String ccv, Integer expMonth, Integer expYear, String firstName, String lastName) {
		return this.getIbookhandler().payRoomDuringCheckout(roomNumber, cc, ccv, expMonth, expYear, firstName, lastName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ExternalSupplyPackage.EXTERNAL_SUPPLY__IBOOKHANDLER:
				if (resolve) return getIbookhandler();
				return basicGetIbookhandler();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ExternalSupplyPackage.EXTERNAL_SUPPLY__IBOOKHANDLER:
				setIbookhandler((IBookHandler)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ExternalSupplyPackage.EXTERNAL_SUPPLY__IBOOKHANDLER:
				setIbookhandler((IBookHandler)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ExternalSupplyPackage.EXTERNAL_SUPPLY__IBOOKHANDLER:
				return ibookhandler != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ExternalSupplyPackage.EXTERNAL_SUPPLY___INITIATE_BOOKING__STRING_STRING_STRING_STRING:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case ExternalSupplyPackage.EXTERNAL_SUPPLY___GET_FREE_ROOMS__INT_STRING_STRING:
				return getFreeRooms((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case ExternalSupplyPackage.EXTERNAL_SUPPLY___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case ExternalSupplyPackage.EXTERNAL_SUPPLY___ADD_ROOM_TO_BOOKING__STRING_INT:
				return addRoomToBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case ExternalSupplyPackage.EXTERNAL_SUPPLY___INITIATE_CHECKOUT__INT:
				return initiateCheckout((Integer)arguments.get(0));
			case ExternalSupplyPackage.EXTERNAL_SUPPLY___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING:
				return payDuringCheckout((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5));
			case ExternalSupplyPackage.EXTERNAL_SUPPLY___CHECK_IN_ROOM__STRING_INTEGER:
				return checkInRoom((String)arguments.get(0), (Integer)arguments.get(1));
			case ExternalSupplyPackage.EXTERNAL_SUPPLY___INITIATE_ROOM_CHECKOUT__INTEGER_INTEGER:
				return initiateRoomCheckout((Integer)arguments.get(0), (Integer)arguments.get(1));
			case ExternalSupplyPackage.EXTERNAL_SUPPLY___PAY_ROOM_DURING_CHECKOUT__INTEGER_STRING_STRING_INTEGER_INTEGER_STRING_STRING:
				return payRoomDuringCheckout((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3), (Integer)arguments.get(4), (String)arguments.get(5), (String)arguments.get(6));
		}
		return super.eInvoke(operationID, arguments);
	}

} //ExternalSupplyImpl
