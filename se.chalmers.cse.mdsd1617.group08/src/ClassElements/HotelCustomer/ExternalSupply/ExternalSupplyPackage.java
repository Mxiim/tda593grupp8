/**
 */
package ClassElements.HotelCustomer.ExternalSupply;

import ClassElements.HotelCustomer.HotelCustomerPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ClassElements.HotelCustomer.ExternalSupply.ExternalSupplyFactory
 * @model kind="package"
 * @generated
 */
public interface ExternalSupplyPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ExternalSupply";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///ClassElements/HotelCustomer/ExternalSupply.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ClassElements.HotelCustomer.ExternalSupply";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ExternalSupplyPackage eINSTANCE = ClassElements.HotelCustomer.ExternalSupply.impl.ExternalSupplyPackageImpl.init();

	/**
	 * The meta object id for the '{@link ClassElements.HotelCustomer.ExternalSupply.impl.ExternalSupplyImpl <em>External Supply</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.HotelCustomer.ExternalSupply.impl.ExternalSupplyImpl
	 * @see ClassElements.HotelCustomer.ExternalSupply.impl.ExternalSupplyPackageImpl#getExternalSupply()
	 * @generated
	 */
	int EXTERNAL_SUPPLY = 0;

	/**
	 * The meta object id for the '{@link ClassElements.HotelCustomer.ExternalSupply.IExternalSupplyHandler <em>IExternal Supply Handler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.HotelCustomer.ExternalSupply.IExternalSupplyHandler
	 * @see ClassElements.HotelCustomer.ExternalSupply.impl.ExternalSupplyPackageImpl#getIExternalSupplyHandler()
	 * @generated
	 */
	int IEXTERNAL_SUPPLY_HANDLER = 1;

	/**
	 * The number of structural features of the '<em>IExternal Supply Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEXTERNAL_SUPPLY_HANDLER_FEATURE_COUNT = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEXTERNAL_SUPPLY_HANDLER___INITIATE_BOOKING__STRING_STRING_STRING_STRING = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEXTERNAL_SUPPLY_HANDLER___GET_FREE_ROOMS__INT_STRING_STRING = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEXTERNAL_SUPPLY_HANDLER___CONFIRM_BOOKING__INT = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEXTERNAL_SUPPLY_HANDLER___ADD_ROOM_TO_BOOKING__STRING_INT = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEXTERNAL_SUPPLY_HANDLER___INITIATE_CHECKOUT__INT = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEXTERNAL_SUPPLY_HANDLER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEXTERNAL_SUPPLY_HANDLER___CHECK_IN_ROOM__STRING_INTEGER = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INTEGER;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEXTERNAL_SUPPLY_HANDLER___INITIATE_ROOM_CHECKOUT__INTEGER_INTEGER = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INTEGER_INTEGER;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEXTERNAL_SUPPLY_HANDLER___PAY_ROOM_DURING_CHECKOUT__INTEGER_STRING_STRING_INTEGER_INTEGER_STRING_STRING = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INTEGER_STRING_STRING_INTEGER_INTEGER_STRING_STRING;

	/**
	 * The number of operations of the '<em>IExternal Supply Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEXTERNAL_SUPPLY_HANDLER_OPERATION_COUNT = HotelCustomerPackage.IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Ibookhandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLY__IBOOKHANDLER = IEXTERNAL_SUPPLY_HANDLER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>External Supply</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLY_FEATURE_COUNT = IEXTERNAL_SUPPLY_HANDLER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLY___INITIATE_BOOKING__STRING_STRING_STRING_STRING = IEXTERNAL_SUPPLY_HANDLER___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLY___GET_FREE_ROOMS__INT_STRING_STRING = IEXTERNAL_SUPPLY_HANDLER___GET_FREE_ROOMS__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLY___CONFIRM_BOOKING__INT = IEXTERNAL_SUPPLY_HANDLER___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLY___ADD_ROOM_TO_BOOKING__STRING_INT = IEXTERNAL_SUPPLY_HANDLER___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLY___INITIATE_CHECKOUT__INT = IEXTERNAL_SUPPLY_HANDLER___INITIATE_CHECKOUT__INT;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLY___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = IEXTERNAL_SUPPLY_HANDLER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLY___CHECK_IN_ROOM__STRING_INTEGER = IEXTERNAL_SUPPLY_HANDLER___CHECK_IN_ROOM__STRING_INTEGER;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLY___INITIATE_ROOM_CHECKOUT__INTEGER_INTEGER = IEXTERNAL_SUPPLY_HANDLER___INITIATE_ROOM_CHECKOUT__INTEGER_INTEGER;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLY___PAY_ROOM_DURING_CHECKOUT__INTEGER_STRING_STRING_INTEGER_INTEGER_STRING_STRING = IEXTERNAL_SUPPLY_HANDLER___PAY_ROOM_DURING_CHECKOUT__INTEGER_STRING_STRING_INTEGER_INTEGER_STRING_STRING;

	/**
	 * The number of operations of the '<em>External Supply</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_SUPPLY_OPERATION_COUNT = IEXTERNAL_SUPPLY_HANDLER_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link ClassElements.HotelCustomer.ExternalSupply.ExternalSupply <em>External Supply</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>External Supply</em>'.
	 * @see ClassElements.HotelCustomer.ExternalSupply.ExternalSupply
	 * @generated
	 */
	EClass getExternalSupply();

	/**
	 * Returns the meta object for the reference '{@link ClassElements.HotelCustomer.ExternalSupply.ExternalSupply#getIbookhandler <em>Ibookhandler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ibookhandler</em>'.
	 * @see ClassElements.HotelCustomer.ExternalSupply.ExternalSupply#getIbookhandler()
	 * @see #getExternalSupply()
	 * @generated
	 */
	EReference getExternalSupply_Ibookhandler();

	/**
	 * Returns the meta object for class '{@link ClassElements.HotelCustomer.ExternalSupply.IExternalSupplyHandler <em>IExternal Supply Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IExternal Supply Handler</em>'.
	 * @see ClassElements.HotelCustomer.ExternalSupply.IExternalSupplyHandler
	 * @generated
	 */
	EClass getIExternalSupplyHandler();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ExternalSupplyFactory getExternalSupplyFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ClassElements.HotelCustomer.ExternalSupply.impl.ExternalSupplyImpl <em>External Supply</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.HotelCustomer.ExternalSupply.impl.ExternalSupplyImpl
		 * @see ClassElements.HotelCustomer.ExternalSupply.impl.ExternalSupplyPackageImpl#getExternalSupply()
		 * @generated
		 */
		EClass EXTERNAL_SUPPLY = eINSTANCE.getExternalSupply();

		/**
		 * The meta object literal for the '<em><b>Ibookhandler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXTERNAL_SUPPLY__IBOOKHANDLER = eINSTANCE.getExternalSupply_Ibookhandler();

		/**
		 * The meta object literal for the '{@link ClassElements.HotelCustomer.ExternalSupply.IExternalSupplyHandler <em>IExternal Supply Handler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.HotelCustomer.ExternalSupply.IExternalSupplyHandler
		 * @see ClassElements.HotelCustomer.ExternalSupply.impl.ExternalSupplyPackageImpl#getIExternalSupplyHandler()
		 * @generated
		 */
		EClass IEXTERNAL_SUPPLY_HANDLER = eINSTANCE.getIExternalSupplyHandler();

	}

} //ExternalSupplyPackage
