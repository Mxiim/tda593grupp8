/**
 */
package ClassElements.HotelCustomer.ExternalSupply;

import ClassElements.Booking.IBookHandler;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>External Supply</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.HotelCustomer.ExternalSupply.ExternalSupply#getIbookhandler <em>Ibookhandler</em>}</li>
 * </ul>
 *
 * @see ClassElements.HotelCustomer.ExternalSupply.ExternalSupplyPackage#getExternalSupply()
 * @model
 * @generated
 */
public interface ExternalSupply extends IExternalSupplyHandler {
	/**
	 * Returns the value of the '<em><b>Ibookhandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ibookhandler</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ibookhandler</em>' reference.
	 * @see #setIbookhandler(IBookHandler)
	 * @see ClassElements.HotelCustomer.ExternalSupply.ExternalSupplyPackage#getExternalSupply_Ibookhandler()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IBookHandler getIbookhandler();

	/**
	 * Sets the value of the '{@link ClassElements.HotelCustomer.ExternalSupply.ExternalSupply#getIbookhandler <em>Ibookhandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ibookhandler</em>' reference.
	 * @see #getIbookhandler()
	 * @generated
	 */
	void setIbookhandler(IBookHandler value);

} // ExternalSupply
