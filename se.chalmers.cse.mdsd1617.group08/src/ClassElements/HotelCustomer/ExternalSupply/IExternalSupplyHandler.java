/**
 */
package ClassElements.HotelCustomer.ExternalSupply;

import ClassElements.HotelCustomer.IHotelCustomerProvides;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IExternal Supply Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ClassElements.HotelCustomer.ExternalSupply.ExternalSupplyPackage#getIExternalSupplyHandler()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IExternalSupplyHandler extends IHotelCustomerProvides {
} // IExternalSupplyHandler
