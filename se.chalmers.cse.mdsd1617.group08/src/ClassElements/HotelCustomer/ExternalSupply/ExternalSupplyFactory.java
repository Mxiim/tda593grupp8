/**
 */
package ClassElements.HotelCustomer.ExternalSupply;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ClassElements.HotelCustomer.ExternalSupply.ExternalSupplyPackage
 * @generated
 */
public interface ExternalSupplyFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ExternalSupplyFactory eINSTANCE = ClassElements.HotelCustomer.ExternalSupply.impl.ExternalSupplyFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>External Supply</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>External Supply</em>'.
	 * @generated
	 */
	ExternalSupply createExternalSupply();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ExternalSupplyPackage getExternalSupplyPackage();

} //ExternalSupplyFactory
