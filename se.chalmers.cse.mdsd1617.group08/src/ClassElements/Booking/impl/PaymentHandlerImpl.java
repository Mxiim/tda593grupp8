/**
 */
package ClassElements.Booking.impl;

import ClassElements.Booking.BookingPackage;
import ClassElements.Booking.PaymentHandler;

import java.lang.reflect.InvocationTargetException;
import javax.xml.soap.SOAPException;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Payment Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PaymentHandlerImpl extends MinimalEObjectImpl.Container implements PaymentHandler {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	/**
	 * @generated NOT
	 */
	protected se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires customerBanking;
	
	protected PaymentHandlerImpl() {
		super();
		try {
			customerBanking = se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires.instance();
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	/**
	 * @generated NOT
	 */
	public boolean isCreditCardValid(String ccNumber, String ccv, int expiryMonth, 
									int expiryYear, String firstName, String lastName){
		try {
			return customerBanking.isCreditCardValid(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
		} catch (SOAPException e) {
			return false;
		}
	}
	
	/**
	 * @generated NOT
	 */
	public boolean makePayment(String ccNumber, String ccv, int expiryMonth, 
								int expiryYear, String firstName, String lastName, double sum){
		try {
			return customerBanking.makePayment(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName, sum);
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackage.Literals.PAYMENT_HANDLER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean pay(String cc, String ccv, int expMonth, int expYear, String firstName, String lastName, double moneyToPay) {
		if(isCreditCardValid(cc, ccv, expMonth, expYear, firstName, lastName)) {
			return makePayment(cc, ccv, expMonth, expYear, firstName, lastName, moneyToPay);
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean pay(String cc, String ccv, int expMonth, int expYear, String firstName, String lastName, int moneyToPay) {
		if(isCreditCardValid(cc, ccv, expMonth, expYear, firstName, lastName)){
			return makePayment(cc, ccv, expMonth, expYear, firstName, lastName, moneyToPay);
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BookingPackage.PAYMENT_HANDLER___PAY__STRING_STRING_INT_INT_STRING_STRING_DOUBLE:
				return pay((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5), (Double)arguments.get(6));
		}
		return super.eInvoke(operationID, arguments);
	}

} //PaymentHandlerImpl
