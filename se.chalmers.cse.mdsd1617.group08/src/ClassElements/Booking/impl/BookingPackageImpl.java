/**
 */
package ClassElements.Booking.impl;

import ClassElements.Admin.AdminPackage;

import ClassElements.Admin.impl.AdminPackageImpl;

import ClassElements.Booking.BookHandler;
import ClassElements.Booking.BookInfo;
import ClassElements.Booking.Booking;
import ClassElements.Booking.BookingFactory;
import ClassElements.Booking.BookingPackage;
import ClassElements.Booking.CountRoomType;
import ClassElements.Booking.IBookHandler;
import ClassElements.Booking.IBookInfo;
import ClassElements.Booking.IBooking;
import ClassElements.Booking.ICountRoomType;
import ClassElements.Booking.IPaymentHandler;
import ClassElements.Booking.IReservation;
import ClassElements.Booking.PaymentHandler;
import ClassElements.Booking.Reservation;

import ClassElements.HotelCustomer.ExternalSupply.ExternalSupplyPackage;

import ClassElements.HotelCustomer.ExternalSupply.impl.ExternalSupplyPackageImpl;

import ClassElements.HotelCustomer.HotelCustomerPackage;

import ClassElements.HotelCustomer.Receptionist.ReceptionistPackage;

import ClassElements.HotelCustomer.Receptionist.impl.ReceptionistPackageImpl;

import ClassElements.HotelCustomer.impl.HotelCustomerPackageImpl;

import ClassElements.Room.RoomPackage;

import ClassElements.Room.impl.RoomPackageImpl;
import java.util.Map;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BookingPackageImpl extends EPackageImpl implements BookingPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iBookHandlerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iPaymentHandlerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass paymentHandlerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iBookInfoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bookingAndReservationHashMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iBookingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bookInfoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bookingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iCountRoomTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iReservationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass reservationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass countRoomTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bookingAndRoomHashMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bookHandlerEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ClassElements.Booking.BookingPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private BookingPackageImpl() {
		super(eNS_URI, BookingFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link BookingPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static BookingPackage init() {
		if (isInited) return (BookingPackage)EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI);

		// Obtain or create and register package
		BookingPackageImpl theBookingPackage = (BookingPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof BookingPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new BookingPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		HotelCustomerPackageImpl theHotelCustomerPackage = (HotelCustomerPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(HotelCustomerPackage.eNS_URI) instanceof HotelCustomerPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(HotelCustomerPackage.eNS_URI) : HotelCustomerPackage.eINSTANCE);
		ReceptionistPackageImpl theReceptionistPackage = (ReceptionistPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ReceptionistPackage.eNS_URI) instanceof ReceptionistPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ReceptionistPackage.eNS_URI) : ReceptionistPackage.eINSTANCE);
		ExternalSupplyPackageImpl theExternalSupplyPackage = (ExternalSupplyPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExternalSupplyPackage.eNS_URI) instanceof ExternalSupplyPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExternalSupplyPackage.eNS_URI) : ExternalSupplyPackage.eINSTANCE);
		RoomPackageImpl theRoomPackage = (RoomPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) instanceof RoomPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) : RoomPackage.eINSTANCE);
		AdminPackageImpl theAdminPackage = (AdminPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AdminPackage.eNS_URI) instanceof AdminPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AdminPackage.eNS_URI) : AdminPackage.eINSTANCE);

		// Create package meta-data objects
		theBookingPackage.createPackageContents();
		theHotelCustomerPackage.createPackageContents();
		theReceptionistPackage.createPackageContents();
		theExternalSupplyPackage.createPackageContents();
		theRoomPackage.createPackageContents();
		theAdminPackage.createPackageContents();

		// Initialize created meta-data
		theBookingPackage.initializePackageContents();
		theHotelCustomerPackage.initializePackageContents();
		theReceptionistPackage.initializePackageContents();
		theExternalSupplyPackage.initializePackageContents();
		theRoomPackage.initializePackageContents();
		theAdminPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theBookingPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(BookingPackage.eNS_URI, theBookingPackage);
		return theBookingPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIBookHandler() {
		return iBookHandlerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookHandler__CheckInRoom__String_Integer() {
		return iBookHandlerEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookHandler__AddExtraCost__int_String_String_Double() {
		return iBookHandlerEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookHandler__GetCheckouts__String() {
		return iBookHandlerEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookHandler__GetCheckouts__String_String() {
		return iBookHandlerEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookHandler__UpdateBooking__int_String_String() {
		return iBookHandlerEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookHandler__UpdateRooms__IRoomType_int_String() {
		return iBookHandlerEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookHandler__GetConfirmedBookings() {
		return iBookHandlerEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookHandler__GetBooking__int() {
		return iBookHandlerEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookHandler__GetCheckIns__String() {
		return iBookHandlerEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookHandler__GetCheckIns__String_String() {
		return iBookHandlerEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookHandler__GetOccupiedRooms__String() {
		return iBookHandlerEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookHandler__CancelBooking__int() {
		return iBookHandlerEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookHandler__CheckInBooking__int() {
		return iBookHandlerEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookHandler__InitiateBooking__String_String_String_String() {
		return iBookHandlerEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookHandler__GetFreeRooms__int_String_String() {
		return iBookHandlerEClass.getEOperations().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookHandler__ConfirmBooking__int() {
		return iBookHandlerEClass.getEOperations().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookHandler__AddRoomToBooking__String_int() {
		return iBookHandlerEClass.getEOperations().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookHandler__InitiateCheckout__int() {
		return iBookHandlerEClass.getEOperations().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookHandler__PayDuringCheckout__String_String_int_int_String_String_double() {
		return iBookHandlerEClass.getEOperations().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookHandler__InitiateRoomCheckout__Integer_Integer() {
		return iBookHandlerEClass.getEOperations().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookHandler__ClearData() {
		return iBookHandlerEClass.getEOperations().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookHandler__PayRoomDuringCheckout__Integer_String_String_Integer_Integer_String_String() {
		return iBookHandlerEClass.getEOperations().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIPaymentHandler() {
		return iPaymentHandlerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIPaymentHandler__Pay__String_String_int_int_String_String_double() {
		return iPaymentHandlerEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPaymentHandler() {
		return paymentHandlerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIBookInfo() {
		return iBookInfoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookInfo__GetReservations() {
		return iBookInfoEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookInfo__GetBookings() {
		return iBookInfoEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookInfo__AddExtraCost__int_String_String_Double() {
		return iBookInfoEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBookingAndReservationHashMap() {
		return bookingAndReservationHashMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBookingAndReservationHashMap_Key() {
		return (EReference)bookingAndReservationHashMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBookingAndReservationHashMap_Value() {
		return (EReference)bookingAndReservationHashMapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIBooking() {
		return iBookingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__AddRoomType__IRoomType() {
		return iBookingEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__GetCountRoomTypes() {
		return iBookingEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBookInfo() {
		return bookInfoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBookInfo_Ibooking() {
		return (EReference)bookInfoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBookInfo_Reservations() {
		return (EReference)bookInfoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBooking() {
		return bookingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBooking_Icountroomtype() {
		return (EReference)bookingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooking_BookID() {
		return (EAttribute)bookingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooking_UniqueID() {
		return (EAttribute)bookingEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooking_IsConfirmed() {
		return (EAttribute)bookingEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBooking__Booking__String_String_String_String() {
		return bookingEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getICountRoomType() {
		return iCountRoomTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getICountRoomType__GetRoomType() {
		return iCountRoomTypeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getICountRoomType__GetCount() {
		return iCountRoomTypeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIReservation() {
		return iReservationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIReservation__GetRoom() {
		return iReservationEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIReservation__IsCheckedOut() {
		return iReservationEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIReservation__GetCheckedInTime() {
		return iReservationEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIReservation__GetCheckedOutTime() {
		return iReservationEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIReservation__GetExtraCostNames() {
		return iReservationEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIReservation__GetExtraCostPrice() {
		return iReservationEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIReservation__AddExtraCost__String_Double() {
		return iReservationEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReservation() {
		return reservationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReservation_Iroom() {
		return (EReference)reservationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReservation_CheckedOut() {
		return (EAttribute)reservationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReservation_CheckedInTime() {
		return (EAttribute)reservationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReservation_CheckedOutTime() {
		return (EAttribute)reservationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCountRoomType() {
		return countRoomTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCountRoomType_RoomType() {
		return (EReference)countRoomTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCountRoomType_Count() {
		return (EAttribute)countRoomTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBookingAndRoomHashMap() {
		return bookingAndRoomHashMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBookingAndRoomHashMap_Key() {
		return (EReference)bookingAndRoomHashMapEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBookingAndRoomHashMap_Value() {
		return (EAttribute)bookingAndRoomHashMapEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBookHandler() {
		return bookHandlerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBookHandler_SingletonInstance() {
		return (EReference)bookHandlerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBookHandler_Ipaymenthandler() {
		return (EReference)bookHandlerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBookHandler_Ibookinfo() {
		return (EReference)bookHandlerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBookHandler__BookHandler() {
		return bookHandlerEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBookHandler__GetInstance() {
		return bookHandlerEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingFactory getBookingFactory() {
		return (BookingFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		bookHandlerEClass = createEClass(BOOK_HANDLER);
		createEReference(bookHandlerEClass, BOOK_HANDLER__SINGLETON_INSTANCE);
		createEReference(bookHandlerEClass, BOOK_HANDLER__IPAYMENTHANDLER);
		createEReference(bookHandlerEClass, BOOK_HANDLER__IBOOKINFO);
		createEOperation(bookHandlerEClass, BOOK_HANDLER___BOOK_HANDLER);
		createEOperation(bookHandlerEClass, BOOK_HANDLER___GET_INSTANCE);

		iPaymentHandlerEClass = createEClass(IPAYMENT_HANDLER);
		createEOperation(iPaymentHandlerEClass, IPAYMENT_HANDLER___PAY__STRING_STRING_INT_INT_STRING_STRING_DOUBLE);

		iBookInfoEClass = createEClass(IBOOK_INFO);
		createEOperation(iBookInfoEClass, IBOOK_INFO___GET_RESERVATIONS);
		createEOperation(iBookInfoEClass, IBOOK_INFO___GET_BOOKINGS);
		createEOperation(iBookInfoEClass, IBOOK_INFO___ADD_EXTRA_COST__INT_STRING_STRING_DOUBLE);

		bookingAndReservationHashMapEClass = createEClass(BOOKING_AND_RESERVATION_HASH_MAP);
		createEReference(bookingAndReservationHashMapEClass, BOOKING_AND_RESERVATION_HASH_MAP__KEY);
		createEReference(bookingAndReservationHashMapEClass, BOOKING_AND_RESERVATION_HASH_MAP__VALUE);

		iBookingEClass = createEClass(IBOOKING);
		createEOperation(iBookingEClass, IBOOKING___ADD_ROOM_TYPE__IROOMTYPE);
		createEOperation(iBookingEClass, IBOOKING___GET_COUNT_ROOM_TYPES);

		iCountRoomTypeEClass = createEClass(ICOUNT_ROOM_TYPE);
		createEOperation(iCountRoomTypeEClass, ICOUNT_ROOM_TYPE___GET_ROOM_TYPE);
		createEOperation(iCountRoomTypeEClass, ICOUNT_ROOM_TYPE___GET_COUNT);

		iReservationEClass = createEClass(IRESERVATION);
		createEOperation(iReservationEClass, IRESERVATION___GET_ROOM);
		createEOperation(iReservationEClass, IRESERVATION___IS_CHECKED_OUT);
		createEOperation(iReservationEClass, IRESERVATION___GET_CHECKED_IN_TIME);
		createEOperation(iReservationEClass, IRESERVATION___GET_CHECKED_OUT_TIME);
		createEOperation(iReservationEClass, IRESERVATION___GET_EXTRA_COST_NAMES);
		createEOperation(iReservationEClass, IRESERVATION___GET_EXTRA_COST_PRICE);
		createEOperation(iReservationEClass, IRESERVATION___ADD_EXTRA_COST__STRING_DOUBLE);

		paymentHandlerEClass = createEClass(PAYMENT_HANDLER);

		bookInfoEClass = createEClass(BOOK_INFO);
		createEReference(bookInfoEClass, BOOK_INFO__IBOOKING);
		createEReference(bookInfoEClass, BOOK_INFO__RESERVATIONS);

		bookingEClass = createEClass(BOOKING);
		createEReference(bookingEClass, BOOKING__ICOUNTROOMTYPE);
		createEAttribute(bookingEClass, BOOKING__BOOK_ID);
		createEAttribute(bookingEClass, BOOKING__UNIQUE_ID);
		createEAttribute(bookingEClass, BOOKING__IS_CONFIRMED);
		createEOperation(bookingEClass, BOOKING___BOOKING__STRING_STRING_STRING_STRING);

		reservationEClass = createEClass(RESERVATION);
		createEReference(reservationEClass, RESERVATION__IROOM);
		createEAttribute(reservationEClass, RESERVATION__CHECKED_OUT);
		createEAttribute(reservationEClass, RESERVATION__CHECKED_IN_TIME);
		createEAttribute(reservationEClass, RESERVATION__CHECKED_OUT_TIME);

		countRoomTypeEClass = createEClass(COUNT_ROOM_TYPE);
		createEReference(countRoomTypeEClass, COUNT_ROOM_TYPE__ROOM_TYPE);
		createEAttribute(countRoomTypeEClass, COUNT_ROOM_TYPE__COUNT);

		bookingAndRoomHashMapEClass = createEClass(BOOKING_AND_ROOM_HASH_MAP);
		createEReference(bookingAndRoomHashMapEClass, BOOKING_AND_ROOM_HASH_MAP__KEY);
		createEAttribute(bookingAndRoomHashMapEClass, BOOKING_AND_ROOM_HASH_MAP__VALUE);

		iBookHandlerEClass = createEClass(IBOOK_HANDLER);
		createEOperation(iBookHandlerEClass, IBOOK_HANDLER___CHECK_IN_ROOM__STRING_INTEGER);
		createEOperation(iBookHandlerEClass, IBOOK_HANDLER___ADD_EXTRA_COST__INT_STRING_STRING_DOUBLE);
		createEOperation(iBookHandlerEClass, IBOOK_HANDLER___GET_CHECKOUTS__STRING);
		createEOperation(iBookHandlerEClass, IBOOK_HANDLER___GET_CHECKOUTS__STRING_STRING);
		createEOperation(iBookHandlerEClass, IBOOK_HANDLER___UPDATE_BOOKING__INT_STRING_STRING);
		createEOperation(iBookHandlerEClass, IBOOK_HANDLER___UPDATE_ROOMS__IROOMTYPE_INT_STRING);
		createEOperation(iBookHandlerEClass, IBOOK_HANDLER___GET_CONFIRMED_BOOKINGS);
		createEOperation(iBookHandlerEClass, IBOOK_HANDLER___GET_BOOKING__INT);
		createEOperation(iBookHandlerEClass, IBOOK_HANDLER___GET_CHECK_INS__STRING);
		createEOperation(iBookHandlerEClass, IBOOK_HANDLER___GET_CHECK_INS__STRING_STRING);
		createEOperation(iBookHandlerEClass, IBOOK_HANDLER___GET_OCCUPIED_ROOMS__STRING);
		createEOperation(iBookHandlerEClass, IBOOK_HANDLER___CANCEL_BOOKING__INT);
		createEOperation(iBookHandlerEClass, IBOOK_HANDLER___CHECK_IN_BOOKING__INT);
		createEOperation(iBookHandlerEClass, IBOOK_HANDLER___INITIATE_BOOKING__STRING_STRING_STRING_STRING);
		createEOperation(iBookHandlerEClass, IBOOK_HANDLER___GET_FREE_ROOMS__INT_STRING_STRING);
		createEOperation(iBookHandlerEClass, IBOOK_HANDLER___CONFIRM_BOOKING__INT);
		createEOperation(iBookHandlerEClass, IBOOK_HANDLER___ADD_ROOM_TO_BOOKING__STRING_INT);
		createEOperation(iBookHandlerEClass, IBOOK_HANDLER___INITIATE_CHECKOUT__INT);
		createEOperation(iBookHandlerEClass, IBOOK_HANDLER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING_DOUBLE);
		createEOperation(iBookHandlerEClass, IBOOK_HANDLER___INITIATE_ROOM_CHECKOUT__INTEGER_INTEGER);
		createEOperation(iBookHandlerEClass, IBOOK_HANDLER___CLEAR_DATA);
		createEOperation(iBookHandlerEClass, IBOOK_HANDLER___PAY_ROOM_DURING_CHECKOUT__INTEGER_STRING_STRING_INTEGER_INTEGER_STRING_STRING);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		RoomPackage theRoomPackage = (RoomPackage)EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI);
		HotelCustomerPackage theHotelCustomerPackage = (HotelCustomerPackage)EPackage.Registry.INSTANCE.getEPackage(HotelCustomerPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		bookHandlerEClass.getESuperTypes().add(this.getIBookHandler());
		paymentHandlerEClass.getESuperTypes().add(this.getIPaymentHandler());
		bookInfoEClass.getESuperTypes().add(this.getIBookInfo());
		bookingEClass.getESuperTypes().add(this.getIBooking());
		reservationEClass.getESuperTypes().add(this.getIReservation());
		countRoomTypeEClass.getESuperTypes().add(this.getICountRoomType());

		// Initialize classes, features, and operations; add parameters
		initEClass(bookHandlerEClass, BookHandler.class, "BookHandler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBookHandler_SingletonInstance(), this.getBookHandler(), null, "singletonInstance", null, 1, 1, BookHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getBookHandler_Ipaymenthandler(), this.getIPaymentHandler(), null, "ipaymenthandler", null, 1, 1, BookHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getBookHandler_Ibookinfo(), this.getIBookInfo(), null, "ibookinfo", null, 1, 1, BookHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEOperation(getBookHandler__BookHandler(), null, "BookHandler", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getBookHandler__GetInstance(), this.getBookHandler(), "getInstance", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(iPaymentHandlerEClass, IPaymentHandler.class, "IPaymentHandler", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getIPaymentHandler__Pay__String_String_int_int_String_String_double(), ecorePackage.getEBooleanObject(), "pay", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "cc", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "ccv", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "expMonth", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "expYear", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "firstName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "lastName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "moneyToPay", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(iBookInfoEClass, IBookInfo.class, "IBookInfo", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getIBookInfo__GetReservations(), this.getBookingAndReservationHashMap(), "getReservations", 0, -1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBookInfo__GetBookings(), this.getIBooking(), "getBookings", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookInfo__AddExtraCost__int_String_String_Double(), ecorePackage.getEBooleanObject(), "addExtraCost", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDoubleObject(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(bookingAndReservationHashMapEClass, Map.Entry.class, "BookingAndReservationHashMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBookingAndReservationHashMap_Key(), this.getIBooking(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getBookingAndReservationHashMap_Value(), this.getIReservation(), null, "value", null, 0, -1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iBookingEClass, IBooking.class, "IBooking", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getIBooking__AddRoomType__IRoomType(), ecorePackage.getEBooleanObject(), "addRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theRoomPackage.getIRoomType(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBooking__GetCountRoomTypes(), this.getICountRoomType(), "getCountRoomTypes", 0, -1, IS_UNIQUE, !IS_ORDERED);

		initEClass(iCountRoomTypeEClass, ICountRoomType.class, "ICountRoomType", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getICountRoomType__GetRoomType(), theRoomPackage.getIRoomType(), "getRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getICountRoomType__GetCount(), ecorePackage.getEIntegerObject(), "getCount", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(iReservationEClass, IReservation.class, "IReservation", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getIReservation__GetRoom(), theRoomPackage.getIRoom(), "getRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIReservation__IsCheckedOut(), ecorePackage.getEBooleanObject(), "isCheckedOut", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIReservation__GetCheckedInTime(), ecorePackage.getEString(), "getCheckedInTime", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIReservation__GetCheckedOutTime(), ecorePackage.getEString(), "getCheckedOutTime", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIReservation__GetExtraCostNames(), ecorePackage.getEString(), "getExtraCostNames", 0, -1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIReservation__GetExtraCostPrice(), ecorePackage.getEDoubleObject(), "getExtraCostPrice", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIReservation__AddExtraCost__String_Double(), ecorePackage.getEBooleanObject(), "addExtraCost", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDoubleObject(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(paymentHandlerEClass, PaymentHandler.class, "PaymentHandler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(bookInfoEClass, BookInfo.class, "BookInfo", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBookInfo_Ibooking(), this.getIBooking(), null, "ibooking", null, 0, -1, BookInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getBookInfo_Reservations(), this.getBookingAndReservationHashMap(), null, "reservations", null, 0, -1, BookInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(bookingEClass, Booking.class, "Booking", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBooking_Icountroomtype(), this.getICountRoomType(), null, "icountroomtype", null, 0, -1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBooking_BookID(), ecorePackage.getEString(), "bookID", null, 1, 1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBooking_UniqueID(), ecorePackage.getEInt(), "uniqueID", "0", 1, 1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBooking_IsConfirmed(), ecorePackage.getEBooleanObject(), "isConfirmed", null, 1, 1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		op = initEOperation(getBooking__Booking__String_String_String_String(), null, "Booking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "firstName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "lastName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(reservationEClass, Reservation.class, "Reservation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReservation_Iroom(), theRoomPackage.getIRoom(), null, "iroom", null, 1, 1, Reservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getReservation_CheckedOut(), ecorePackage.getEBooleanObject(), "checkedOut", null, 1, 1, Reservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getReservation_CheckedInTime(), ecorePackage.getEString(), "checkedInTime", null, 1, 1, Reservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getReservation_CheckedOutTime(), ecorePackage.getEString(), "checkedOutTime", null, 1, 1, Reservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(countRoomTypeEClass, CountRoomType.class, "CountRoomType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCountRoomType_RoomType(), theRoomPackage.getIRoomType(), null, "roomType", null, 1, 1, CountRoomType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getCountRoomType_Count(), ecorePackage.getEIntegerObject(), "count", "0", 1, 1, CountRoomType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(bookingAndRoomHashMapEClass, Map.Entry.class, "BookingAndRoomHashMap", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBookingAndRoomHashMap_Key(), theRoomPackage.getIRoom(), null, "key", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBookingAndRoomHashMap_Value(), ecorePackage.getEIntegerObject(), "value", null, 1, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iBookHandlerEClass, IBookHandler.class, "IBookHandler", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getIBookHandler__CheckInRoom__String_Integer(), ecorePackage.getEIntegerObject(), "checkInRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEIntegerObject(), "bookID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookHandler__AddExtraCost__int_String_String_Double(), ecorePackage.getEBooleanObject(), "addExtraCost", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDoubleObject(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookHandler__GetCheckouts__String(), ecorePackage.getEIntegerObject(), "getCheckouts", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookHandler__GetCheckouts__String_String(), ecorePackage.getEIntegerObject(), "getCheckouts", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookHandler__UpdateBooking__int_String_String(), null, "updateBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookHandler__UpdateRooms__IRoomType_int_String(), null, "updateRooms", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theRoomPackage.getIRoomType(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "newCount", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "bookID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBookHandler__GetConfirmedBookings(), this.getIBooking(), "getConfirmedBookings", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookHandler__GetBooking__int(), this.getIBooking(), "getBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookHandler__GetCheckIns__String(), ecorePackage.getEIntegerObject(), "getCheckIns", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "date", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookHandler__GetCheckIns__String_String(), ecorePackage.getEIntegerObject(), "getCheckIns", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookHandler__GetOccupiedRooms__String(), this.getBookingAndRoomHashMap(), "getOccupiedRooms", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "date", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookHandler__CancelBooking__int(), null, "cancelBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookHandler__CheckInBooking__int(), null, "checkInBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookHandler__InitiateBooking__String_String_String_String(), ecorePackage.getEInt(), "initiateBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "firstName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "lastName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookHandler__GetFreeRooms__int_String_String(), theHotelCustomerPackage.getFreeRoomTypesDTO(), "getFreeRooms", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "numBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookHandler__ConfirmBooking__int(), ecorePackage.getEBooleanObject(), "confirmBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookHandler__AddRoomToBooking__String_int(), ecorePackage.getEBooleanObject(), "addRoomToBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDesc", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookHandler__InitiateCheckout__int(), ecorePackage.getEDoubleObject(), "initiateCheckout", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookHandler__PayDuringCheckout__String_String_int_int_String_String_double(), ecorePackage.getEBooleanObject(), "payDuringCheckout", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "cc", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "ccv", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "expMonth", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "expYear", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "firstName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "lastname", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "amount", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookHandler__InitiateRoomCheckout__Integer_Integer(), ecorePackage.getEDouble(), "initiateRoomCheckout", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEIntegerObject(), "bookID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEIntegerObject(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBookHandler__ClearData(), null, "clearData", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookHandler__PayRoomDuringCheckout__Integer_String_String_Integer_Integer_String_String(), ecorePackage.getEBoolean(), "payRoomDuringCheckout", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEIntegerObject(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "cc", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "ccv", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEIntegerObject(), "expMonth", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEIntegerObject(), "expYear", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "firstName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "lastName", 1, 1, IS_UNIQUE, !IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //BookingPackageImpl
