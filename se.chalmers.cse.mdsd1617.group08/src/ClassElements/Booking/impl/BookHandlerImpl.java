/**
 */
package ClassElements.Booking.impl;

import ClassElements.Booking.BookHandler;
import ClassElements.Booking.Booking;
import ClassElements.Booking.BookingPackage;
import ClassElements.Booking.IBookInfo;
import ClassElements.Booking.IBooking;
import ClassElements.Booking.ICountRoomType;
import ClassElements.Booking.IPaymentHandler;
import ClassElements.Booking.IReservation;
import ClassElements.HotelCustomer.FreeRoomTypesDTO;
import ClassElements.Room.IRoom;
import ClassElements.Room.IRoomType;
import ClassElements.Room.IRoomTypeManager;
import ClassElements.Room.Room;
import ClassElements.Room.RoomType;
import ClassElements.Room.impl.RoomFactoryImpl;
import ClassElements.Room.impl.RoomImpl;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.resource.Resource;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Book Handler</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.Booking.impl.BookHandlerImpl#getSingletonInstance <em>Singleton Instance</em>}</li>
 *   <li>{@link ClassElements.Booking.impl.BookHandlerImpl#getIpaymenthandler <em>Ipaymenthandler</em>}</li>
 *   <li>{@link ClassElements.Booking.impl.BookHandlerImpl#getIbookinfo <em>Ibookinfo</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookHandlerImpl extends MinimalEObjectImpl.Container implements BookHandler {
	/**
	 * The cached value of the '{@link #getSingletonInstance() <em>Singleton Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSingletonInstance()
	 * @generated NOT
	 * @ordered
	 */
	private BookHandler singletonInstance;

	/**
	 * The cached value of the '{@link #getIpaymenthandler() <em>Ipaymenthandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIpaymenthandler()
	 * @generated NOT
	 * @ordered
	 */
	protected IPaymentHandler ipaymenthandler = new PaymentHandlerImpl();

	/**
	 * The cached value of the '{@link #getIbookinfo() <em>Ibookinfo</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIbookinfo()
	 * @generated NOT
	 * @ordered
	 */
//	protected IBookInfo ibookinfo = (IBookInfo) new BookingFactoryImpl().getBookingPackage().getBookInfo();
	protected IBookInfo ibookinfo = BookingFactoryImpl.init().createBookInfo();
	protected IRoomTypeManager iroomtypemanager = RoomFactoryImpl.init().createRoomTypeManager();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected BookHandlerImpl() {
		// Exists only to stop prevent more instantiation...
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackage.Literals.BOOK_HANDLER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookHandler getSingletonInstance() {
		if (singletonInstance != null && singletonInstance.eIsProxy()) {
			InternalEObject oldSingletonInstance = (InternalEObject)singletonInstance;
			singletonInstance = (BookHandler)eResolveProxy(oldSingletonInstance);
			if (singletonInstance != oldSingletonInstance) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BookingPackage.BOOK_HANDLER__SINGLETON_INSTANCE, oldSingletonInstance, singletonInstance));
			}
		}
		return singletonInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookHandler basicGetSingletonInstance() {
		return singletonInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSingletonInstance(BookHandler newSingletonInstance) {
		BookHandler oldSingletonInstance = singletonInstance;
		singletonInstance = newSingletonInstance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOK_HANDLER__SINGLETON_INSTANCE, oldSingletonInstance, singletonInstance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPaymentHandler getIpaymenthandler() {
		if (ipaymenthandler != null && ipaymenthandler.eIsProxy()) {
			InternalEObject oldIpaymenthandler = (InternalEObject)ipaymenthandler;
			ipaymenthandler = (IPaymentHandler)eResolveProxy(oldIpaymenthandler);
			if (ipaymenthandler != oldIpaymenthandler) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BookingPackage.BOOK_HANDLER__IPAYMENTHANDLER, oldIpaymenthandler, ipaymenthandler));
			}
		}
		return ipaymenthandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPaymentHandler basicGetIpaymenthandler() {
		return ipaymenthandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIpaymenthandler(IPaymentHandler newIpaymenthandler) {
		IPaymentHandler oldIpaymenthandler = ipaymenthandler;
		ipaymenthandler = newIpaymenthandler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOK_HANDLER__IPAYMENTHANDLER, oldIpaymenthandler, ipaymenthandler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IBookInfo getIbookinfo() {
		if (ibookinfo != null && ibookinfo.eIsProxy()) {
			InternalEObject oldIbookinfo = (InternalEObject)ibookinfo;
			ibookinfo = (IBookInfo)eResolveProxy(oldIbookinfo);
			if (ibookinfo != oldIbookinfo) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BookingPackage.BOOK_HANDLER__IBOOKINFO, oldIbookinfo, ibookinfo));
			}
		}
		return ibookinfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IBookInfo basicGetIbookinfo() {
		return ibookinfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIbookinfo(IBookInfo newIbookinfo) {
		IBookInfo oldIbookinfo = ibookinfo;
		ibookinfo = newIbookinfo;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOK_HANDLER__IBOOKINFO, oldIbookinfo, ibookinfo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	private Map<Integer, IReservation> currentOccupiedRooms = new HashMap<>();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Integer checkInRoom(String roomTypeName, Integer bookID) {
		
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		IBooking currentBooking;
		EList<IRoom> roomsList = new BasicEList<IRoom>();
		roomsList.addAll(new RoomFactoryImpl().createRoomManager().getRooms());
		for (int i= 0; i<roomsList.size(); i++) {
			if (currentOccupiedRooms.containsKey(Integer.parseInt(roomsList.get(i).getRoomNumber()))) {
				roomsList.remove(i);
				i--;
			}
		}
		
		for (IBooking booking : this.ibookinfo.getBookings()) {
			BookingImpl temp = (BookingImpl)booking;
			if (temp.getBookID().equals(bookID+"")) {
				currentBooking = temp;
				EList<ICountRoomType> s = temp.getCountRoomTypes();
				for (ICountRoomType x : s) {
					if(x.getRoomType().getName().equals(roomTypeName)) {
						for(EList<IReservation> reservationList : this.ibookinfo.getReservations().values()){ // loop through all bookings
							for(IReservation reservation : reservationList){ // loop through all reservations
								try {
									Date checkedInDate = df.parse(reservation.getCheckedInTime());
									Date checkedOutDate = df.parse(reservation.getCheckedOutTime());
									Date currentBookingCheckedInDate = df.parse(((BookingImpl)currentBooking).getStartDate());
									Date currentBookingCheckedOutDate = df.parse(((BookingImpl)currentBooking).getEndDate());
									
									if(!((RoomImpl)reservation.getRoom()).getIroomtype().getName().equals(roomTypeName) || // check if the room type is incorrect
											((checkedInDate.after(currentBookingCheckedInDate) && // 		check if the start/end date of a reservation 
											checkedInDate.before(currentBookingCheckedOutDate)) // 	collides with the start/end date of the current booking
											|| 
											(checkedOutDate.after(currentBookingCheckedInDate) &&
													checkedOutDate.before(currentBookingCheckedOutDate)))) {
										for(int i = 0; i<roomsList.size(); i++){
											if(roomsList.get(i).getRoomNumber().equals(reservation.getRoom().getRoomNumber())){
												roomsList.remove(i); 	// if they do collide, the room of that reservation is removed from the list
												
											} 						// after the loop is finished, roomsList i supposed to contain
																	// all available rooms for the time period of the current booking
										}
									}
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
						if (roomsList.size() == 0) {
							return -1;
						}
						IRoom roomToCheckIn = roomsList.get(0); // roomsList contains only available rooms, so we pick the first one
						/*for(IRoom tmpRoom : getOccupiedRooms(Calendar.getInstance().getTime().toString()){
							
						}*/
						boolean isCheckedOut = false;
						IReservation res = new ReservationImpl(roomToCheckIn, temp.getStartDate(), temp.getEndDate(), isCheckedOut);
						if (this.ibookinfo.getReservations().containsKey(booking)) {
							EList<IReservation> reservs = this.ibookinfo.getReservations().get(booking);
							reservs.add(res);//if this doesnt work - remove and add new list.
						} else {
							EList<IReservation> reservs = new BasicEList<>();
							reservs.add(res);
							this.ibookinfo.getReservations().put(booking, reservs);
						}
						currentOccupiedRooms.put(Integer.parseInt(roomToCheckIn.getRoomNumber()), res);
						int count = x.getCount()-1;
						IRoomType type = x.getRoomType();
						s.remove(x);
						if (count != 0) { 
 							s.add(new CountRoomTypeImpl(type, count));//if this doesn't work, we have to redo whole list.
						}
						return Integer.parseInt(roomToCheckIn.getRoomNumber());
					}
				}
			}
		}
		return -1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean addExtraCost(int bookID, String roomNumber, String name, Double price) {
		if(roomNumber==null||name==null||price==null){
			return false;
		}
		return this.ibookinfo.addExtraCost(bookID, roomNumber, name, price);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> getCheckouts(String startDate) {
		EList<Integer> temp = new BasicEList<Integer>();
		for(EList<IReservation> reservationList : this.ibookinfo.getReservations().values()){
			for(IReservation reservation : reservationList){
				if(reservation.getCheckedOutTime().equals(startDate)){
					temp.add(Integer.parseInt(reservation.getRoom().getRoomNumber()));
				}
			}
		}
		return temp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> getCheckouts(String startDate, String endDate) {
		EList<Integer> temp = new BasicEList<Integer>();
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		for(EList<IReservation> reservationList : this.ibookinfo.getReservations().values()){
			for(IReservation reservation : reservationList){
		        try {
					Date searchStartDate = df.parse(startDate);
					Date searchEndDate = df.parse(endDate);
					Date checkedOutDate = df.parse(reservation.getCheckedOutTime());
					if((checkedOutDate.after(searchStartDate) && checkedOutDate.before(searchEndDate)) 
							|| (checkedOutDate.equals(searchStartDate) || checkedOutDate.equals(searchEndDate)) ){
						temp.add(Integer.parseInt(reservation.getRoom().getRoomNumber()));
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return temp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void updateBooking(int bookID, String startDate, String endDate) {
		if(startDate ==null||endDate==null){
			return;
		}
		this.getBooking(bookID).setStartDate(startDate);
		this.getBooking(bookID).setEndDate(endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void updateRooms(IRoomType roomType, int newCount, String bookID) {
		if(roomType==null||bookID==null||newCount < 0){
			return;
		}
		
		BookingImpl booking = (BookingImpl) this.getBooking(Integer.parseInt(bookID));
		
		EList<ICountRoomType> countRoom = booking.getCountRoomTypes();
		
		for(ICountRoomType crtI : countRoom) {
			CountRoomTypeImpl crt = (CountRoomTypeImpl) crtI;
			if(crt.getRoomType().getName().equals(roomType.getName())) {
				crt.setCount(newCount);
				return;
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<IBooking> getConfirmedBookings() {
		EList<IBooking> bookings = this.getIbookinfo().getBookings();
		EList<IBooking> confirmedBookings = new BasicEList<IBooking>();
		for(IBooking booking : bookings){
			BookingImpl realBooking = (BookingImpl) booking;
			if(realBooking.getIsConfirmed()) confirmedBookings.add(realBooking);
		}
		return confirmedBookings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public IBooking getBooking(int bookID) {
		EList<IBooking> bookingList = this.getIbookinfo().getBookings();
		for(int i = 0; i < bookingList.size(); i++) {
			Booking booking = (Booking) bookingList.get(i);
			if(booking.getBookID().equals(Integer.toString(bookID))) return booking;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> getCheckIns(String date) {
		EList<Integer> temp = new BasicEList<Integer>();
		for(EList<IReservation> reservationList : this.ibookinfo.getReservations().values()){
			for(IReservation reservation : reservationList){
				if(reservation.getCheckedInTime().equals(date) && !temp.contains(Integer.parseInt(reservation.getRoom().getRoomNumber()))){
					temp.add(Integer.parseInt(reservation.getRoom().getRoomNumber()));
				}
			}
		}
		return temp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> getCheckIns(String startDate, String endDate) {
		EList<Integer> temp = new BasicEList<Integer>();
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		for(EList<IReservation> reservationList : this.ibookinfo.getReservations().values()){
			for(IReservation reservation : reservationList){
		        try {
					Date searchStartDate = df.parse(startDate);
					Date searchEndDate = df.parse(endDate);
					Date checkedInDate = df.parse(reservation.getCheckedInTime());
					if(((checkedInDate.equals(searchStartDate) || checkedInDate.equals(searchEndDate)) || (checkedInDate.after(searchStartDate) 
							&& checkedInDate.before(searchEndDate))) && !temp.contains(Integer.parseInt(reservation.getRoom().getRoomNumber()))){
						temp.add(Integer.parseInt(reservation.getRoom().getRoomNumber()));
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return temp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EMap<IRoom, Integer> getOccupiedRooms(String date) {
		if(date==null){
			EMap<IRoom, Integer> emptyMap = new BasicEMap<>();
			return emptyMap;
		}
		
		EMap<IRoom, Integer> occupiedRooms = new BasicEMap<>();
		
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		try{
			int i = 0;
			for(EList<IReservation> reservationList : this.ibookinfo.getReservations().values()){
				
				String tmpBookID = ((BookingImpl)ibookinfo.getReservations().get(i).getKey()).getBookID();
				for(IReservation reservation : reservationList){
				
					Date startDate = df.parse(reservation.getCheckedInTime());
					Date endDate = df.parse(reservation.getCheckedOutTime());
					Date checkDate;
					checkDate = df.parse(date);
					
					if(startDate.before(checkDate) && endDate.after(checkDate) && (checkDate.equals(startDate)
							|| checkDate.equals(endDate)))
						occupiedRooms.put(reservation.getRoom(), Integer.parseInt(tmpBookID));
				}
				i++;
			}
		}
		 catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void cancelBooking(int bookID) {
		IBooking booking = this.getBooking(bookID);
		this.getIbookinfo().getBookings().remove(booking);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void checkInBooking(int bookID) {
		for(IBooking booking : this.ibookinfo.getBookings()){
			if(((BookingImpl)booking).getBookID().equals(Integer.toString(bookID))){
				List<String> derp = new ArrayList<>();
				for(ICountRoomType crt :((BookingImpl)booking).getCountRoomTypes()){
					for(int i = 0; i < crt.getCount(); i++) {
						derp.add(crt.getRoomType().getName());
					}
				}
				for (String x : derp) {
					this.checkInRoom(x, bookID);
				}
				return;
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int initiateBooking(String firstName, String lastName, String startDate, String endDate) {
		if(firstName == null || lastName == null || startDate == null || endDate == null) return -1;
		if(startDate.length() == 8 && endDate.length() == 8) {
			try {
				IBookInfo iBookInfo = this.getIbookinfo();
				Booking booking = new BookingImpl(firstName, lastName, startDate, endDate);
				
				int startyear = Integer.parseInt(startDate.substring(0, 4));
				int startmonth = Integer.parseInt(startDate.substring(4, 6));
				int startday = Integer.parseInt(startDate.substring(6, 8));
				
				int endyear = Integer.parseInt(endDate.substring(0, 4));
				int endmonth = Integer.parseInt(endDate.substring(4, 6));
				int endday = Integer.parseInt(endDate.substring(6, 8));
				
				if (startyear < 0 || startmonth <= 0 || startmonth > 12 || startday <= 0 || startday > 31) return -1;
				if (endyear < 0 || endmonth <= 0 || endmonth > 12 || endday <= 0 || endday > 31) return -1;
				
				DateFormat df = new SimpleDateFormat("yyyyMMdd");
				
				try {
					Date searchStartDate = df.parse(startDate);
					Date searchEndDate = df.parse(endDate);
					
					if(searchStartDate.after(searchEndDate)) return -1;
					
					Calendar today = Calendar.getInstance();
					today.clear(Calendar.HOUR); today.clear(Calendar.MINUTE); today.clear(Calendar.SECOND);
					Date todayDate = today.getTime();
					
					if(searchStartDate.before(todayDate) || searchEndDate.before(todayDate)) return -1;
					
				} catch (ParseException e) {
					return -1;
				}
				
				iBookInfo.getBookings().add(booking);
				int id  = Integer.parseInt(booking.getBookID());
				return id;
				
			} catch (NumberFormatException e) {
				return -1;
			}
		} else return -1;
	}
	
	private boolean isDateValid(String date) {
		if(date.length() != 8) return false;
		try {
			int year = Integer.parseInt(date.substring(0, 4));
			int month = Integer.parseInt(date.substring(4, 6));
			int day = Integer.parseInt(date.substring(6, 8));
			return (year > 0 && (month > 0 && month <= 12) && (day > 0 && day <= 31));
			
		} catch (NumberFormatException e) {
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 * todo: I just added return null to get rid of the red stuff, dont forget to remove after the method is implemented
	 */
	public EList<FreeRoomTypesDTO> getFreeRooms(int numBeds, String startDate, String endDate) {
		if(startDate==null||endDate ==null){
			EList<FreeRoomTypesDTO> emptyList = new BasicEList<FreeRoomTypesDTO>();
			return emptyList;
		}
		EList<IRoom> allRoomList = new RoomFactoryImpl().createRoomManager().getRooms();
		HashMap<IRoomType, Integer> roomTypesOccupiedList = new HashMap<IRoomType, Integer>();
		HashMap<IRoomType, Integer> roomTypesMap = new HashMap<IRoomType, Integer>();
		HashMap<IRoomType, Integer> roomTypesRemainingList = new HashMap<IRoomType, Integer>();

		DateFormat df = new SimpleDateFormat("yyyyMMdd");

		try {
			Date searchStartDate = df.parse(startDate);
			Date searchEndDate = df.parse(endDate);
			
			if(searchStartDate.after(searchEndDate)) return new BasicEList<FreeRoomTypesDTO>();

			// Parse through all the bookings, if the bookings are made in the specified date range then
			// add their rooms & room types to the occupied room list.
			for (IBooking booking : this.getIbookinfo().getBookings()) { 
				// Check if the booking is in the given time range
				// If it is in the time range, add the occupied room to the
				// occupied room list
				BookingImpl realBooking = (BookingImpl) booking;
				Date currBookingStartDate = df.parse(realBooking.getStartDate());
				Date currBookingEndDate = df.parse(realBooking.getEndDate());

				if ((currBookingStartDate.after(searchStartDate) && currBookingStartDate.before(searchEndDate)) // If the booking is within the given date range, add its rooms to the list of occupied rooms
						|| (currBookingEndDate.before(searchEndDate) && currBookingStartDate.after(searchStartDate)
								|| (currBookingEndDate.equals(searchEndDate) && currBookingStartDate.equals(searchStartDate)))) {
					
					EList<ICountRoomType> countRoomTypeList = realBooking.getCountRoomTypes();
					for(ICountRoomType countRoomType : countRoomTypeList) {
						IRoomType roomType = countRoomType.getRoomType();
						if (roomTypesOccupiedList.containsKey(roomType)) {
							int numRooms = roomTypesOccupiedList.get(roomType);
							numRooms += countRoomType.getCount();
							roomTypesOccupiedList.put(roomType, numRooms);
						} else {
							roomTypesOccupiedList.put(roomType, countRoomType.getCount());
						}
					}
				}
			}
			
			// Parse through all the reservations, if the reservations are checkedin/checkedout in 
			// the specified date range then add their rooms & room types to the occupied room list.
			for (EList<IReservation> reservList : this.getIbookinfo().getReservations().values()) {
				for (IReservation reserv : reservList) {
					Date reservCheckInTime;
					Date reservCheckOutTime;
					if (reserv.getCheckedInTime() != "" && reserv.getCheckedInTime() != null
							&& reserv.getCheckedOutTime() != "" && reserv.getCheckedOutTime() != null) {
						reservCheckInTime = df.parse(reserv.getCheckedInTime());
						reservCheckOutTime = df.parse(reserv.getCheckedOutTime());
						if ((reservCheckInTime.after(searchStartDate) && reservCheckInTime.before(searchEndDate))
								|| (reservCheckOutTime.after(searchStartDate)
										&& reservCheckOutTime.before(searchEndDate))) {
							RoomImpl room = (RoomImpl) reserv.getRoom();
							IRoomType roomType = room.getIroomtype();
							if (roomTypesOccupiedList.containsKey(roomType)) {
								int numRooms = roomTypesOccupiedList.get(roomType);
								numRooms += 1;
								roomTypesOccupiedList.put(roomType, numRooms);
							} else {
								roomTypesOccupiedList.put(roomType, 1);
							}
						}
					}
				}
			}

			// Now we compare the occupied room list to the full room list to
			// determine how many of each type of room remain
			EList<IRoomType> allRoomTypes = new BasicEList<IRoomType>();
			for (IRoom room : allRoomList) {
				RoomImpl realRoom = (RoomImpl) room;
				if(!allRoomTypes.contains(realRoom.getIroomtype())){
					allRoomTypes.add(realRoom.getIroomtype());
				}	
			}
			for (IRoomType type : allRoomTypes){
				int sum =0;
				for (IRoom room : allRoomList) {
					RoomImpl realRoom = (RoomImpl) room;
					if(type.getName().equals(realRoom.getIroomtype().getName())){
						sum++;
					}
				}
				roomTypesMap.put(type, sum);
			}
			for (IRoomType rt : roomTypesMap.keySet()) {	
				int totalRoomsOfType =roomTypesMap.get(rt);				
				for (IRoomType ort : roomTypesOccupiedList.keySet()) {
					if(rt.getName().equals(ort.getName())){
						totalRoomsOfType-=roomTypesOccupiedList.get(ort);
					}
				}
				roomTypesRemainingList.put(rt, totalRoomsOfType);
			}
			

			// Now we make a list of type FreeRoomTypesDTO to return as the interface requires.
			EList<FreeRoomTypesDTO> freeRoomList = new BasicEList<FreeRoomTypesDTO>();
			for (IRoomType rt : roomTypesRemainingList.keySet()) {
				if(rt.getNumOfBeds() >= numBeds){
					int count = roomTypesRemainingList.get(rt);
					// Make FreeRoomTypeDTO
					// Add freeRoomTypeDTO to freeRoomList
					final class frtdto implements FreeRoomTypesDTO{
						private double price;
						private int numBeds;
						private String desc;
						private int freeRooms;
						@Override
						public EClass eClass() {
							// TODO Auto-generated method stub
							return null;
						}
						@Override
						public Resource eResource() {
							// TODO Auto-generated method stub
							return null;
						}
						@Override
						public EObject eContainer() {
							// TODO Auto-generated method stub
							return null;
						}
						@Override
						public EStructuralFeature eContainingFeature() {
							// TODO Auto-generated method stub
							return null;
						}
						@Override
						public EReference eContainmentFeature() {
							// TODO Auto-generated method stub
							return null;
						}
						@Override
						public EList<EObject> eContents() {
							// TODO Auto-generated method stub
							return null;
						}
						@Override
						public TreeIterator<EObject> eAllContents() {
							// TODO Auto-generated method stub
							return null;
						}
						@Override
						public boolean eIsProxy() {
							// TODO Auto-generated method stub
							return false;
						}
						@Override
						public EList<EObject> eCrossReferences() {
							// TODO Auto-generated method stub
							return null;
						}
						@Override
						public Object eGet(EStructuralFeature feature) {
							// TODO Auto-generated method stub
							return null;
						}
						@Override
						public Object eGet(EStructuralFeature feature, boolean resolve) {
							// TODO Auto-generated method stub
							return null;
						}
						@Override
						public void eSet(EStructuralFeature feature, Object newValue) {
							// TODO Auto-generated method stub
							
						}
						@Override
						public boolean eIsSet(EStructuralFeature feature) {
							// TODO Auto-generated method stub
							return false;
						}
						@Override
						public void eUnset(EStructuralFeature feature) {
							// TODO Auto-generated method stub
							
						}
						@Override
						public Object eInvoke(EOperation operation, EList<?> arguments)
								throws InvocationTargetException {
							// TODO Auto-generated method stub
							return null;
						}
						@Override
						public EList<Adapter> eAdapters() {
							// TODO Auto-generated method stub
							return null;
						}
						@Override
						public boolean eDeliver() {
							// TODO Auto-generated method stub
							return false;
						}
						@Override
						public void eSetDeliver(boolean deliver) {
							// TODO Auto-generated method stub
							
						}
						@Override
						public void eNotify(Notification notification) {
							// TODO Auto-generated method stub
							
						}
						@Override
						public String getRoomTypeDescription() {
							return this.desc;
						}
						@Override
						public void setRoomTypeDescription(String value) {
							this.desc=value;
							
						}
						@Override
						public int getNumBeds() {
							return this.numBeds;
						}
						@Override
						public void setNumBeds(int value) {
							this.numBeds=value;
							
						}
						@Override
						public Double getPricePerNight() {
							return this.price;
						}
						@Override
						public void setPricePerNight(Double value) {
							this.price=value;
						}
						@Override
						public int getNumFreeRooms() {
							return this.freeRooms;
						}
						@Override
						public void setNumFreeRooms(int value) {
							this.freeRooms=value;
						}

						}
				
						
					
					frtdto frt = new frtdto();
					frt.setNumBeds(rt.getNumOfBeds());
					frt.setPricePerNight(rt.getPrice());
					frt.setRoomTypeDescription(rt.getName());
					frt.setNumFreeRooms(count);
					freeRoomList.add(frt);
				}
			}
			
			return freeRoomList;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean confirmBooking(int bookID) {
		// TODO: Use getBooking method instead.
		BookingImpl booking = (BookingImpl) this.getBooking(bookID);
		
		if(booking != null) {
			
			EList<ICountRoomType> countRoomTypes = booking.getIcountroomtype();
			boolean hasRooms = false;
			
			for(ICountRoomType crt : countRoomTypes) {
				if(crt.getCount() > 0) hasRooms = true;
			}
			
			if(hasRooms) {
				booking.setIsConfirmed(true);
				return true;
			}
			
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean addRoomToBooking(String roomTypeDesc, int bookID) {
		IBooking booking = this.getBooking(bookID);
		
		if(booking != null) {
			BookingImpl realBooking = (BookingImpl) booking;
			EList<ICountRoomType> s = realBooking.getCountRoomTypes();
			EList<IRoomType> roomTypeList = this.iroomtypemanager.getRoomTypes();
			 
			for(IRoomType rt : roomTypeList) {
				if(rt.getName().equals(roomTypeDesc)) { // If a roomtype of that type exists
					EList<FreeRoomTypesDTO> freeRooms = this.getFreeRooms(rt.getNumOfBeds(), realBooking.getStartDate(), realBooking.getEndDate());
					for(FreeRoomTypesDTO frt : freeRooms) {
						if(frt.getRoomTypeDescription().equals(roomTypeDesc) && frt.getNumFreeRooms() > 0) { // If there is a free room of that roomtype
							realBooking.addRoomType(rt);
							return true;
						}
					}

				}
			}
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Double initiateCheckout(int bookID) {
		IBooking booking = this.getBooking(bookID);
		EList<IReservation> roomList = this.getIbookinfo().getReservations().get(booking);
		this.getIbookinfo().getReservations().remove(booking); // Drop reservations so the rooms are unassigned
		Double price = 0.0;
		for(IReservation reserv : roomList) {
			price += reserv.getExtraCostPrice();
			Room room = (Room) reserv.getRoom();
			currentOccupiedRooms.remove(Integer.parseInt(room.getRoomNumber()));
			RoomType roomType = (RoomType) room.getIroomtype();
			price += roomType.getPrice();
		}
		return price;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean payDuringCheckout(String cc, String ccv, int expMonth, int expYear, String firstName, String lastname, double amount) {
		PaymentHandlerImpl paymentHandler = (PaymentHandlerImpl) this.getIpaymenthandler();
		if(paymentHandler.isCreditCardValid(cc, ccv, expMonth, expYear, firstName, lastname)){
			return this.getIpaymenthandler().pay(cc, ccv, expMonth, expYear, firstName, lastname, amount);
		}
		else {
			return false;
		}
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateRoomCheckout(Integer bookID, Integer roomNumber) {
		BookingImpl booking = (BookingImpl) this.getBooking(bookID);
		EList<IReservation> reservations = this.ibookinfo.getReservations().get(booking);
		double price = 0;
		
		if(booking == null) return price;
		Iterator<IReservation> it = reservations.iterator();
		while (it.hasNext()) {
			IReservation reserv = it.next();
			if(reserv.getRoom().getRoomNumber().equals(Integer.toString(roomNumber))) { // Find the reservation on the given room number
				price += reserv.getExtraCostPrice(); // Calculate the price
				Room room = (Room) reserv.getRoom();
				RoomType roomType = (RoomType) room.getIroomtype();
				price += roomType.getPrice();		
				it.remove();; // Remove reservaiton from list of reservaitons
			}
		}
		this.ibookinfo.getReservations().put(booking, reservations); // Update list of reservations for that booking
		return price;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void clearData() {
		this.getIbookinfo().getBookings().clear();
		this.getIbookinfo().getReservations().clear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payRoomDuringCheckout(Integer roomNumber, String cc, String ccv, Integer expMonth, Integer expYear, String firstName, String lastName) {
		double price = 0;
		IReservation res = currentOccupiedRooms.get(roomNumber);
		price += res.getExtraCostPrice();
		Room room = (Room) res.getRoom();
		RoomType roomType = (RoomType) room.getIroomtype();
		price += roomType.getPrice();
		PaymentHandlerImpl paymentHandler = (PaymentHandlerImpl) this.getIpaymenthandler();
		if(paymentHandler.isCreditCardValid(cc, ccv, expMonth, expYear, firstName, lastName)){
			return this.getIpaymenthandler().pay(cc, ccv, expMonth, expYear, firstName, lastName, price);
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void BookHandler() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public BookHandler getInstance() {
		if(singletonInstance == null){
			singletonInstance = new BookHandlerImpl();
		}
		return singletonInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackage.BOOK_HANDLER__SINGLETON_INSTANCE:
				if (resolve) return getSingletonInstance();
				return basicGetSingletonInstance();
			case BookingPackage.BOOK_HANDLER__IPAYMENTHANDLER:
				if (resolve) return getIpaymenthandler();
				return basicGetIpaymenthandler();
			case BookingPackage.BOOK_HANDLER__IBOOKINFO:
				if (resolve) return getIbookinfo();
				return basicGetIbookinfo();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackage.BOOK_HANDLER__SINGLETON_INSTANCE:
				setSingletonInstance((BookHandler)newValue);
				return;
			case BookingPackage.BOOK_HANDLER__IPAYMENTHANDLER:
				setIpaymenthandler((IPaymentHandler)newValue);
				return;
			case BookingPackage.BOOK_HANDLER__IBOOKINFO:
				setIbookinfo((IBookInfo)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOK_HANDLER__SINGLETON_INSTANCE:
				setSingletonInstance((BookHandler)null);
				return;
			case BookingPackage.BOOK_HANDLER__IPAYMENTHANDLER:
				setIpaymenthandler((IPaymentHandler)null);
				return;
			case BookingPackage.BOOK_HANDLER__IBOOKINFO:
				setIbookinfo((IBookInfo)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOK_HANDLER__SINGLETON_INSTANCE:
				return singletonInstance != null;
			case BookingPackage.BOOK_HANDLER__IPAYMENTHANDLER:
				return ipaymenthandler != null;
			case BookingPackage.BOOK_HANDLER__IBOOKINFO:
				return ibookinfo != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BookingPackage.BOOK_HANDLER___CHECK_IN_ROOM__STRING_INTEGER:
				return checkInRoom((String)arguments.get(0), (Integer)arguments.get(1));
			case BookingPackage.BOOK_HANDLER___ADD_EXTRA_COST__INT_STRING_STRING_DOUBLE:
				return addExtraCost((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Double)arguments.get(3));
			case BookingPackage.BOOK_HANDLER___GET_CHECKOUTS__STRING:
				return getCheckouts((String)arguments.get(0));
			case BookingPackage.BOOK_HANDLER___GET_CHECKOUTS__STRING_STRING:
				return getCheckouts((String)arguments.get(0), (String)arguments.get(1));
			case BookingPackage.BOOK_HANDLER___UPDATE_BOOKING__INT_STRING_STRING:
				updateBooking((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
				return null;
			case BookingPackage.BOOK_HANDLER___UPDATE_ROOMS__IROOMTYPE_INT_STRING:
				updateRooms((IRoomType)arguments.get(0), (Integer)arguments.get(1), (String)arguments.get(2));
				return null;
			case BookingPackage.BOOK_HANDLER___GET_CONFIRMED_BOOKINGS:
				return getConfirmedBookings();
			case BookingPackage.BOOK_HANDLER___GET_BOOKING__INT:
				return getBooking((Integer)arguments.get(0));
			case BookingPackage.BOOK_HANDLER___GET_CHECK_INS__STRING:
				return getCheckIns((String)arguments.get(0));
			case BookingPackage.BOOK_HANDLER___GET_CHECK_INS__STRING_STRING:
				return getCheckIns((String)arguments.get(0), (String)arguments.get(1));
			case BookingPackage.BOOK_HANDLER___GET_OCCUPIED_ROOMS__STRING:
				return getOccupiedRooms((String)arguments.get(0));
			case BookingPackage.BOOK_HANDLER___CANCEL_BOOKING__INT:
				cancelBooking((Integer)arguments.get(0));
				return null;
			case BookingPackage.BOOK_HANDLER___CHECK_IN_BOOKING__INT:
				checkInBooking((Integer)arguments.get(0));
				return null;
			case BookingPackage.BOOK_HANDLER___INITIATE_BOOKING__STRING_STRING_STRING_STRING:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case BookingPackage.BOOK_HANDLER___GET_FREE_ROOMS__INT_STRING_STRING:
				return getFreeRooms((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case BookingPackage.BOOK_HANDLER___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case BookingPackage.BOOK_HANDLER___ADD_ROOM_TO_BOOKING__STRING_INT:
				return addRoomToBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case BookingPackage.BOOK_HANDLER___INITIATE_CHECKOUT__INT:
				return initiateCheckout((Integer)arguments.get(0));
			case BookingPackage.BOOK_HANDLER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING_DOUBLE:
				return payDuringCheckout((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5), (Double)arguments.get(6));
			case BookingPackage.BOOK_HANDLER___INITIATE_ROOM_CHECKOUT__INTEGER_INTEGER:
				return initiateRoomCheckout((Integer)arguments.get(0), (Integer)arguments.get(1));
			case BookingPackage.BOOK_HANDLER___CLEAR_DATA:
				clearData();
				return null;
			case BookingPackage.BOOK_HANDLER___PAY_ROOM_DURING_CHECKOUT__INTEGER_STRING_STRING_INTEGER_INTEGER_STRING_STRING:
				return payRoomDuringCheckout((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3), (Integer)arguments.get(4), (String)arguments.get(5), (String)arguments.get(6));
			case BookingPackage.BOOK_HANDLER___BOOK_HANDLER:
				BookHandler();
				return null;
			case BookingPackage.BOOK_HANDLER___GET_INSTANCE:
				return getInstance();
		}
		return super.eInvoke(operationID, arguments);
	}

} //BookHandlerImpl
