/**
 */
package ClassElements.Booking.impl;

import ClassElements.Booking.BookInfo;
import ClassElements.Booking.BookingPackage;
import ClassElements.Booking.IBooking;
import ClassElements.Booking.IReservation;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Map;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Book Info</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.Booking.impl.BookInfoImpl#getIbooking <em>Ibooking</em>}</li>
 *   <li>{@link ClassElements.Booking.impl.BookInfoImpl#getReservations <em>Reservations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookInfoImpl extends MinimalEObjectImpl.Container implements BookInfo {
	/**
	 * The cached value of the '{@link #getIbooking() <em>Ibooking</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIbooking()
	 * @generated NOT
	 * @ordered
	 */
	protected EList<IBooking> ibooking = new BasicEList<>();

	/**
	 * The cached value of the '{@link #getReservations() <em>Reservations</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReservations()
	 * @generated NOT
	 * @ordered
	 */
	protected EMap<IBooking, EList<IReservation>> reservations = new BasicEMap<>();
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BookInfoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackage.Literals.BOOK_INFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IBooking> getIbooking() {
		if (ibooking == null) {
			ibooking = new EObjectResolvingEList<IBooking>(IBooking.class, this, BookingPackage.BOOK_INFO__IBOOKING);
		}
		return ibooking;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EMap<IBooking, EList<IReservation>> getReservations() {
		return (EMap<IBooking, EList<IReservation>>) reservations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<IBooking> getBookings() {
		return this.ibooking;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean addExtraCost(int bookID, String roomNumber, String name, Double price) {
		for (Map.Entry<IBooking, EList<IReservation>> entry : reservations.entrySet()) {
			BookingImpl temp = (BookingImpl)entry.getKey();
			if (temp.getBookID().equals(bookID+"")) {
				EList<IReservation> reservs = entry.getValue();
				
				for (IReservation res : reservs) {
					if (res.getRoom().getRoomNumber().equals(roomNumber)) {
						return res.addExtraCost(name,  price);
					}
				}
			}
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackage.BOOK_INFO__IBOOKING:
				return getIbooking();
			case BookingPackage.BOOK_INFO__RESERVATIONS:
				return getReservations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackage.BOOK_INFO__IBOOKING:
				getIbooking().clear();
				getIbooking().addAll((Collection<? extends IBooking>)newValue);
				return;
			case BookingPackage.BOOK_INFO__RESERVATIONS:
				getReservations().clear();
				getReservations().addAll((Collection<? extends Map.Entry<IBooking, EList<IReservation>>>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOK_INFO__IBOOKING:
				getIbooking().clear();
				return;
			case BookingPackage.BOOK_INFO__RESERVATIONS:
				getReservations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOK_INFO__IBOOKING:
				return ibooking != null && !ibooking.isEmpty();
			case BookingPackage.BOOK_INFO__RESERVATIONS:
				return reservations != null && !reservations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BookingPackage.BOOK_INFO___GET_BOOKINGS:
				return getBookings();
			case BookingPackage.BOOK_INFO___ADD_EXTRA_COST__INT_STRING_STRING_DOUBLE:
				return addExtraCost((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Double)arguments.get(3));
		}
		return super.eInvoke(operationID, arguments);
	}

} //BookInfoImpl
