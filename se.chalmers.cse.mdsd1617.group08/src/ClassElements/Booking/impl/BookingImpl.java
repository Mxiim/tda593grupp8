/**
 */
package ClassElements.Booking.impl;

import ClassElements.Booking.Booking;
import ClassElements.Booking.BookingPackage;
import ClassElements.Booking.ICountRoomType;

import ClassElements.Room.IRoomType;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Booking</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.Booking.impl.BookingImpl#getIcountroomtype <em>Icountroomtype</em>}</li>
 *   <li>{@link ClassElements.Booking.impl.BookingImpl#getBookID <em>Book ID</em>}</li>
 *   <li>{@link ClassElements.Booking.impl.BookingImpl#getUniqueID <em>Unique ID</em>}</li>
 *   <li>{@link ClassElements.Booking.impl.BookingImpl#getIsConfirmed <em>Is Confirmed</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookingImpl extends MinimalEObjectImpl.Container implements Booking {
	/**
	 * The cached value of the '{@link #getIcountroomtype() <em>Icountroomtype</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIcountroomtype()
	 * @generated NOT
	 * @ordered
	 */
	protected EList<ICountRoomType> icountroomtype = new BasicEList<>();

	/**
	 * The default value of the '{@link #getBookID() <em>Book ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookID()
	 * @generated
	 * @ordered
	 */
	protected static final String BOOK_ID_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getBookID() <em>Book ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookID()
	 * @generated
	 * @ordered
	 */
	protected String bookID = BOOK_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getUniqueID() <em>Unique ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUniqueID()
	 * @generated NOT
	 * @ordered
	 */
	protected static int UNIQUE_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getUniqueID() <em>Unique ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUniqueID()
	 * @generated
	 * @ordered
	 */
	protected int uniqueID = UNIQUE_ID_EDEFAULT;
	
	/**
	 * The default value of the '{@link #getIsConfirmed() <em>Is Confirmed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsConfirmed()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_CONFIRMED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #getIsConfirmed() <em>Is Confirmed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsConfirmed()
	 * @generated
	 * @ordered
	 */
	protected Boolean isConfirmed = IS_CONFIRMED_EDEFAULT;

	/**
	 * @generated NOT
	 */
	protected String firstName;
	
	/**
	 * @generated NOT
	 */
	protected String lastName;
	
	/**
	 * @generated NOT
	 */
	protected String startDate;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @generated NOT
	 */
	protected String endDate;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BookingImpl() {
		super();
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected BookingImpl(String firstName, String lastName, String startDate, String endDate) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.startDate = startDate;
		this.endDate = endDate;
		this.uniqueID = UNIQUE_ID_EDEFAULT; 
		UNIQUE_ID_EDEFAULT++;
		bookID = uniqueID + "";
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getStartDate() {
		return this.startDate;
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getEndDate() {
		return this.endDate;
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackage.Literals.BOOKING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ICountRoomType> getIcountroomtype() {
		if (icountroomtype == null) {
			icountroomtype = new EObjectResolvingEList<ICountRoomType>(ICountRoomType.class, this, BookingPackage.BOOKING__ICOUNTROOMTYPE);
		}
		return icountroomtype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<ICountRoomType> getCountRoomTypes() {
		return this.icountroomtype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBookID() {
		return bookID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookID(String newBookID) {
		String oldBookID = bookID;
		bookID = newBookID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__BOOK_ID, oldBookID, bookID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getUniqueID() {
		return uniqueID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUniqueID(int newUniqueID) {
		int oldUniqueID = uniqueID;
		uniqueID = newUniqueID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__UNIQUE_ID, oldUniqueID, uniqueID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsConfirmed() {
		return isConfirmed;
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsConfirmed(Boolean newIsConfirmed) {
		Boolean oldIsConfirmed = isConfirmed;
		isConfirmed = newIsConfirmed;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__IS_CONFIRMED, oldIsConfirmed, isConfirmed));
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean addRoomType(IRoomType roomType) {
		for (ICountRoomType x : icountroomtype) {
			if (x.getRoomType().getName().equals(roomType.getName())) {//names will be unique, Lex is responsible.
				int count = x.getCount()+1; //we obv add a new one
				icountroomtype.remove(x);
				ICountRoomType temp = new CountRoomTypeImpl(roomType, count);
				icountroomtype.add(temp);
				return true;
			}
		}
		ICountRoomType temp = new CountRoomTypeImpl(roomType, 1);
		icountroomtype.add(temp);
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void Booking(String firstName, String lastName, String startDate, String endDate) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackage.BOOKING__ICOUNTROOMTYPE:
				return getIcountroomtype();
			case BookingPackage.BOOKING__BOOK_ID:
				return getBookID();
			case BookingPackage.BOOKING__UNIQUE_ID:
				return getUniqueID();
			case BookingPackage.BOOKING__IS_CONFIRMED:
				return getIsConfirmed();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackage.BOOKING__ICOUNTROOMTYPE:
				getIcountroomtype().clear();
				getIcountroomtype().addAll((Collection<? extends ICountRoomType>)newValue);
				return;
			case BookingPackage.BOOKING__BOOK_ID:
				setBookID((String)newValue);
				return;
			case BookingPackage.BOOKING__UNIQUE_ID:
				setUniqueID((Integer)newValue);
				return;
			case BookingPackage.BOOKING__IS_CONFIRMED:
				setIsConfirmed((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING__ICOUNTROOMTYPE:
				getIcountroomtype().clear();
				return;
			case BookingPackage.BOOKING__BOOK_ID:
				setBookID(BOOK_ID_EDEFAULT);
				return;
			case BookingPackage.BOOKING__UNIQUE_ID:
				setUniqueID(UNIQUE_ID_EDEFAULT);
				return;
			case BookingPackage.BOOKING__IS_CONFIRMED:
				setIsConfirmed(IS_CONFIRMED_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING__ICOUNTROOMTYPE:
				return icountroomtype != null && !icountroomtype.isEmpty();
			case BookingPackage.BOOKING__BOOK_ID:
				return BOOK_ID_EDEFAULT == null ? bookID != null : !BOOK_ID_EDEFAULT.equals(bookID);
			case BookingPackage.BOOKING__UNIQUE_ID:
				return uniqueID != UNIQUE_ID_EDEFAULT;
			case BookingPackage.BOOKING__IS_CONFIRMED:
				return IS_CONFIRMED_EDEFAULT == null ? isConfirmed != null : !IS_CONFIRMED_EDEFAULT.equals(isConfirmed);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BookingPackage.BOOKING___ADD_ROOM_TYPE__IROOMTYPE:
				return addRoomType((IRoomType)arguments.get(0));
			case BookingPackage.BOOKING___GET_COUNT_ROOM_TYPES:
				return getCountRoomTypes();
			case BookingPackage.BOOKING___BOOKING__STRING_STRING_STRING_STRING:
				Booking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (bookID: ");
		result.append(bookID);
		result.append(", uniqueID: ");
		result.append(uniqueID);
		result.append(", isConfirmed: ");
		result.append(isConfirmed);
		result.append(')');
		return result.toString();
	}

} //BookingImpl
