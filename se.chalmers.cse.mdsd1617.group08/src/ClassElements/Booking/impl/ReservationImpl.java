/**
 */
package ClassElements.Booking.impl;

import ClassElements.Booking.BookingPackage;
import ClassElements.Booking.Reservation;

import ClassElements.Room.IRoom;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reservation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.Booking.impl.ReservationImpl#getIroom <em>Iroom</em>}</li>
 *   <li>{@link ClassElements.Booking.impl.ReservationImpl#getCheckedOut <em>Checked Out</em>}</li>
 *   <li>{@link ClassElements.Booking.impl.ReservationImpl#getCheckedInTime <em>Checked In Time</em>}</li>
 *   <li>{@link ClassElements.Booking.impl.ReservationImpl#getCheckedOutTime <em>Checked Out Time</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReservationImpl extends MinimalEObjectImpl.Container implements Reservation {
	/**
	 * The cached value of the '{@link #getIroom() <em>Iroom</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIroom()
	 * @generated
	 * @ordered
	 */
	protected IRoom iroom;

	/**
	 * The default value of the '{@link #getCheckedOut() <em>Checked Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckedOut()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CHECKED_OUT_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getCheckedOut() <em>Checked Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckedOut()
	 * @generated
	 * @ordered
	 */
	protected Boolean checkedOut = CHECKED_OUT_EDEFAULT;
	/**
	 * The default value of the '{@link #getCheckedInTime() <em>Checked In Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckedInTime()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKED_IN_TIME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getCheckedInTime() <em>Checked In Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckedInTime()
	 * @generated
	 * @ordered
	 */
	protected String checkedInTime = CHECKED_IN_TIME_EDEFAULT;
	/**
	 * The default value of the '{@link #getCheckedOutTime() <em>Checked Out Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckedOutTime()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKED_OUT_TIME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getCheckedOutTime() <em>Checked Out Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckedOutTime()
	 * @generated
	 * @ordered
	 */
	protected String checkedOutTime = CHECKED_OUT_TIME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReservationImpl() {
		super();
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected ReservationImpl(IRoom room, String checkInDate, String checkOutDate, boolean isCheckedOut) {
		this.iroom = room;
		this.checkedInTime = checkInDate;
		this.checkedOutTime = checkOutDate;
		this.checkedOut = isCheckedOut;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	private Map<String, Double> extras = new HashMap<>();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackage.Literals.RESERVATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoom getIroom() {
		if (iroom != null && iroom.eIsProxy()) {
			InternalEObject oldIroom = (InternalEObject)iroom;
			iroom = (IRoom)eResolveProxy(oldIroom);
			if (iroom != oldIroom) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BookingPackage.RESERVATION__IROOM, oldIroom, iroom));
			}
		}
		return iroom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoom basicGetIroom() {
		return iroom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIroom(IRoom newIroom) {
		IRoom oldIroom = iroom;
		iroom = newIroom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.RESERVATION__IROOM, oldIroom, iroom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean getCheckedOut() {
		return checkedOut;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCheckedOut(Boolean newCheckedOut) {
		Boolean oldCheckedOut = checkedOut;
		checkedOut = newCheckedOut;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.RESERVATION__CHECKED_OUT, oldCheckedOut, checkedOut));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public IRoom getRoom() {
		return this.iroom;		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getCheckedInTime() {
		return checkedInTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCheckedInTime(String newCheckedInTime) {
		String oldCheckedInTime = checkedInTime;
		checkedInTime = newCheckedInTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.RESERVATION__CHECKED_IN_TIME, oldCheckedInTime, checkedInTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getCheckedOutTime() {
		return checkedOutTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCheckedOutTime(String newCheckedOutTime) {
		String oldCheckedOutTime = checkedOutTime;
		checkedOutTime = newCheckedOutTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.RESERVATION__CHECKED_OUT_TIME, oldCheckedOutTime, checkedOutTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean isCheckedOut() {
		return this.checkedOut;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<String> getExtraCostNames() {
		EList<String> result = new BasicEList<>();
		for (String x : extras.keySet()) {
			result.add(x);
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Double getExtraCostPrice() {
		double result = 0;
		for (Double x : extras.values()) {
			result += x;
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean addExtraCost(String name, Double price) {
		if (extras.containsKey(name)) { // I decide that we cant change cost of already existing name.
			return false;
		}
		extras.put(name, price);
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackage.RESERVATION__IROOM:
				if (resolve) return getIroom();
				return basicGetIroom();
			case BookingPackage.RESERVATION__CHECKED_OUT:
				return getCheckedOut();
			case BookingPackage.RESERVATION__CHECKED_IN_TIME:
				return getCheckedInTime();
			case BookingPackage.RESERVATION__CHECKED_OUT_TIME:
				return getCheckedOutTime();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackage.RESERVATION__IROOM:
				setIroom((IRoom)newValue);
				return;
			case BookingPackage.RESERVATION__CHECKED_OUT:
				setCheckedOut((Boolean)newValue);
				return;
			case BookingPackage.RESERVATION__CHECKED_IN_TIME:
				setCheckedInTime((String)newValue);
				return;
			case BookingPackage.RESERVATION__CHECKED_OUT_TIME:
				setCheckedOutTime((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackage.RESERVATION__IROOM:
				setIroom((IRoom)null);
				return;
			case BookingPackage.RESERVATION__CHECKED_OUT:
				setCheckedOut(CHECKED_OUT_EDEFAULT);
				return;
			case BookingPackage.RESERVATION__CHECKED_IN_TIME:
				setCheckedInTime(CHECKED_IN_TIME_EDEFAULT);
				return;
			case BookingPackage.RESERVATION__CHECKED_OUT_TIME:
				setCheckedOutTime(CHECKED_OUT_TIME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackage.RESERVATION__IROOM:
				return iroom != null;
			case BookingPackage.RESERVATION__CHECKED_OUT:
				return CHECKED_OUT_EDEFAULT == null ? checkedOut != null : !CHECKED_OUT_EDEFAULT.equals(checkedOut);
			case BookingPackage.RESERVATION__CHECKED_IN_TIME:
				return CHECKED_IN_TIME_EDEFAULT == null ? checkedInTime != null : !CHECKED_IN_TIME_EDEFAULT.equals(checkedInTime);
			case BookingPackage.RESERVATION__CHECKED_OUT_TIME:
				return CHECKED_OUT_TIME_EDEFAULT == null ? checkedOutTime != null : !CHECKED_OUT_TIME_EDEFAULT.equals(checkedOutTime);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BookingPackage.RESERVATION___GET_ROOM:
				return getRoom();
			case BookingPackage.RESERVATION___IS_CHECKED_OUT:
				return isCheckedOut();
			case BookingPackage.RESERVATION___GET_EXTRA_COST_NAMES:
				return getExtraCostNames();
			case BookingPackage.RESERVATION___GET_EXTRA_COST_PRICE:
				return getExtraCostPrice();
			case BookingPackage.RESERVATION___ADD_EXTRA_COST__STRING_DOUBLE:
				return addExtraCost((String)arguments.get(0), (Double)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checkedOut: ");
		result.append(checkedOut);
		result.append(", checkedInTime: ");
		result.append(checkedInTime);
		result.append(", checkedOutTime: ");
		result.append(checkedOutTime);
		result.append(')');
		return result.toString();
	}

} //ReservationImpl
