/**
 */
package ClassElements.Booking;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Book Info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.Booking.BookInfo#getIbooking <em>Ibooking</em>}</li>
 *   <li>{@link ClassElements.Booking.BookInfo#getReservations <em>Reservations</em>}</li>
 * </ul>
 *
 * @see ClassElements.Booking.BookingPackage#getBookInfo()
 * @model
 * @generated
 */
public interface BookInfo extends IBookInfo {
	/**
	 * Returns the value of the '<em><b>Ibooking</b></em>' reference list.
	 * The list contents are of type {@link ClassElements.Booking.IBooking}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ibooking</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ibooking</em>' reference list.
	 * @see ClassElements.Booking.BookingPackage#getBookInfo_Ibooking()
	 * @model ordered="false"
	 * @generated
	 */
	EList<IBooking> getIbooking();

	/**
	 * Returns the value of the '<em><b>Reservations</b></em>' reference list.
	 * The list contents are of type {@link java.util.Map.Entry}&lt;ClassElements.Booking.IBooking, ClassElements.Booking.IReservation>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reservations</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reservations</em>' reference list.
	 * @see ClassElements.Booking.BookingPackage#getBookInfo_Reservations()
	 * @model mapType="ClassElements.Booking.BookingAndReservationHashMap<ClassElements.Booking.IBooking, ClassElements.Booking.IReservation>" ordered="false"
	 * @generated NOT
	 */
	EMap<IBooking, EList<IReservation>> getReservations();

} // BookInfo
