/**
 */
package ClassElements.Booking;

import ClassElements.Room.IRoomType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Count Room Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.Booking.CountRoomType#getRoomType <em>Room Type</em>}</li>
 *   <li>{@link ClassElements.Booking.CountRoomType#getCount <em>Count</em>}</li>
 * </ul>
 *
 * @see ClassElements.Booking.BookingPackage#getCountRoomType()
 * @model
 * @generated
 */
public interface CountRoomType extends ICountRoomType {

	/**
	 * Returns the value of the '<em><b>Room Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Type</em>' reference.
	 * @see #setRoomType(IRoomType)
	 * @see ClassElements.Booking.BookingPackage#getCountRoomType_RoomType()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomType getRoomType();

	/**
	 * Sets the value of the '{@link ClassElements.Booking.CountRoomType#getRoomType <em>Room Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Type</em>' reference.
	 * @see #getRoomType()
	 * @generated
	 */
	void setRoomType(IRoomType value);

	/**
	 * Returns the value of the '<em><b>Count</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Count</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Count</em>' attribute.
	 * @see #setCount(Integer)
	 * @see ClassElements.Booking.BookingPackage#getCountRoomType_Count()
	 * @model default="0" required="true" ordered="false"
	 * @generated
	 */
	Integer getCount();

	/**
	 * Sets the value of the '{@link ClassElements.Booking.CountRoomType#getCount <em>Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Count</em>' attribute.
	 * @see #getCount()
	 * @generated
	 */
	void setCount(Integer value);
} // CountRoomType
