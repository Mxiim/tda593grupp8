/**
 */
package ClassElements.Booking;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Payment Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ClassElements.Booking.BookingPackage#getPaymentHandler()
 * @model
 * @generated
 */
public interface PaymentHandler extends IPaymentHandler {
} // PaymentHandler
