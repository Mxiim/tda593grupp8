/**
 */
package ClassElements.Booking;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>And Reservation Hash Map</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.Booking.BookingAndReservationHashMap#getKey <em>Key</em>}</li>
 *   <li>{@link ClassElements.Booking.BookingAndReservationHashMap#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see ClassElements.Booking.BookingPackage#getBookingAndReservationHashMap()
 * @model
 * @generated
 */
public interface BookingAndReservationHashMap extends EObject {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' reference.
	 * @see #setKey(IBooking)
	 * @see ClassElements.Booking.BookingPackage#getBookingAndReservationHashMap_Key()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IBooking getKey();

	/**
	 * Sets the value of the '{@link ClassElements.Booking.BookingAndReservationHashMap#getKey <em>Key</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' reference.
	 * @see #getKey()
	 * @generated
	 */
	void setKey(IBooking value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' reference list.
	 * The list contents are of type {@link ClassElements.Booking.IReservation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' reference list.
	 * @see ClassElements.Booking.BookingPackage#getBookingAndReservationHashMap_Value()
	 * @model ordered="false"
	 * @generated
	 */
	EList<IReservation> getValue();

} // BookingAndReservationHashMap
