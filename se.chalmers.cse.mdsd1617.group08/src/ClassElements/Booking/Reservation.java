/**
 */
package ClassElements.Booking;

import ClassElements.Room.IRoom;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reservation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.Booking.Reservation#getIroom <em>Iroom</em>}</li>
 *   <li>{@link ClassElements.Booking.Reservation#getCheckedOut <em>Checked Out</em>}</li>
 *   <li>{@link ClassElements.Booking.Reservation#getCheckedInTime <em>Checked In Time</em>}</li>
 *   <li>{@link ClassElements.Booking.Reservation#getCheckedOutTime <em>Checked Out Time</em>}</li>
 * </ul>
 *
 * @see ClassElements.Booking.BookingPackage#getReservation()
 * @model
 * @generated
 */
public interface Reservation extends IReservation {
	/**
	 * Returns the value of the '<em><b>Iroom</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iroom</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iroom</em>' reference.
	 * @see #setIroom(IRoom)
	 * @see ClassElements.Booking.BookingPackage#getReservation_Iroom()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoom getIroom();

	/**
	 * Sets the value of the '{@link ClassElements.Booking.Reservation#getIroom <em>Iroom</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iroom</em>' reference.
	 * @see #getIroom()
	 * @generated
	 */
	void setIroom(IRoom value);

	/**
	 * Returns the value of the '<em><b>Checked Out</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checked Out</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checked Out</em>' attribute.
	 * @see #setCheckedOut(Boolean)
	 * @see ClassElements.Booking.BookingPackage#getReservation_CheckedOut()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Boolean getCheckedOut();

	/**
	 * Sets the value of the '{@link ClassElements.Booking.Reservation#getCheckedOut <em>Checked Out</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checked Out</em>' attribute.
	 * @see #getCheckedOut()
	 * @generated
	 */
	void setCheckedOut(Boolean value);

	/**
	 * Returns the value of the '<em><b>Checked In Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checked In Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checked In Time</em>' attribute.
	 * @see #setCheckedInTime(String)
	 * @see ClassElements.Booking.BookingPackage#getReservation_CheckedInTime()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getCheckedInTime();

	/**
	 * Sets the value of the '{@link ClassElements.Booking.Reservation#getCheckedInTime <em>Checked In Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checked In Time</em>' attribute.
	 * @see #getCheckedInTime()
	 * @generated
	 */
	void setCheckedInTime(String value);

	/**
	 * Returns the value of the '<em><b>Checked Out Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checked Out Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checked Out Time</em>' attribute.
	 * @see #setCheckedOutTime(String)
	 * @see ClassElements.Booking.BookingPackage#getReservation_CheckedOutTime()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getCheckedOutTime();

	/**
	 * Sets the value of the '{@link ClassElements.Booking.Reservation#getCheckedOutTime <em>Checked Out Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checked Out Time</em>' attribute.
	 * @see #getCheckedOutTime()
	 * @generated
	 */
	void setCheckedOutTime(String value);

} // Reservation
