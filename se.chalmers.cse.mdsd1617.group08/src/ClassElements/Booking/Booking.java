/**
 */
package ClassElements.Booking;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Booking</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.Booking.Booking#getIcountroomtype <em>Icountroomtype</em>}</li>
 *   <li>{@link ClassElements.Booking.Booking#getBookID <em>Book ID</em>}</li>
 *   <li>{@link ClassElements.Booking.Booking#getUniqueID <em>Unique ID</em>}</li>
 *   <li>{@link ClassElements.Booking.Booking#getIsConfirmed <em>Is Confirmed</em>}</li>
 * </ul>
 *
 * @see ClassElements.Booking.BookingPackage#getBooking()
 * @model
 * @generated
 */
public interface Booking extends IBooking {
	/**
	 * Returns the value of the '<em><b>Icountroomtype</b></em>' reference list.
	 * The list contents are of type {@link ClassElements.Booking.ICountRoomType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Icountroomtype</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Icountroomtype</em>' reference list.
	 * @see ClassElements.Booking.BookingPackage#getBooking_Icountroomtype()
	 * @model ordered="false"
	 * @generated
	 */
	EList<ICountRoomType> getIcountroomtype();

	/**
	 * Returns the value of the '<em><b>Book ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Book ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Book ID</em>' attribute.
	 * @see #setBookID(String)
	 * @see ClassElements.Booking.BookingPackage#getBooking_BookID()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getBookID();

	/**
	 * Sets the value of the '{@link ClassElements.Booking.Booking#getBookID <em>Book ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Book ID</em>' attribute.
	 * @see #getBookID()
	 * @generated
	 */
	void setBookID(String value);

	/**
	 * Returns the value of the '<em><b>Unique ID</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unique ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unique ID</em>' attribute.
	 * @see #setUniqueID(int)
	 * @see ClassElements.Booking.BookingPackage#getBooking_UniqueID()
	 * @model default="0" required="true" ordered="false"
	 * @generated
	 */
	int getUniqueID();

	/**
	 * Sets the value of the '{@link ClassElements.Booking.Booking#getUniqueID <em>Unique ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unique ID</em>' attribute.
	 * @see #getUniqueID()
	 * @generated
	 */
	void setUniqueID(int value);

	/**
	 * Returns the value of the '<em><b>Is Confirmed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Confirmed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Confirmed</em>' attribute.
	 * @see #setIsConfirmed(Boolean)
	 * @see ClassElements.Booking.BookingPackage#getBooking_IsConfirmed()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Boolean getIsConfirmed();

	/**
	 * Sets the value of the '{@link ClassElements.Booking.Booking#getIsConfirmed <em>Is Confirmed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Confirmed</em>' attribute.
	 * @see #getIsConfirmed()
	 * @generated
	 */
	void setIsConfirmed(Boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model firstNameRequired="true" firstNameOrdered="false" lastNameRequired="true" lastNameOrdered="false" startDateRequired="true" startDateOrdered="false" endDateRequired="true" endDateOrdered="false"
	 * @generated
	 */
	void Booking(String firstName, String lastName, String startDate, String endDate);

} // Booking
