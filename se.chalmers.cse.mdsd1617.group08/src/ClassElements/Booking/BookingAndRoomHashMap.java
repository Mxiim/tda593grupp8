/**
 */
package ClassElements.Booking;

import ClassElements.Room.IRoom;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>And Room Hash Map</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.Booking.BookingAndRoomHashMap#getKey <em>Key</em>}</li>
 *   <li>{@link ClassElements.Booking.BookingAndRoomHashMap#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see ClassElements.Booking.BookingPackage#getBookingAndRoomHashMap()
 * @model
 * @generated
 */
public interface BookingAndRoomHashMap extends EObject {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' reference.
	 * @see #setKey(IRoom)
	 * @see ClassElements.Booking.BookingPackage#getBookingAndRoomHashMap_Key()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoom getKey();

	/**
	 * Sets the value of the '{@link ClassElements.Booking.BookingAndRoomHashMap#getKey <em>Key</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' reference.
	 * @see #getKey()
	 * @generated
	 */
	void setKey(IRoom value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(int)
	 * @see ClassElements.Booking.BookingPackage#getBookingAndRoomHashMap_Value()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getValue();

	/**
	 * Sets the value of the '{@link ClassElements.Booking.BookingAndRoomHashMap#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(int value);

} // BookingAndRoomHashMap
