/**
 */
package ClassElements.Booking;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IBook Info</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ClassElements.Booking.BookingPackage#getIBookInfo()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IBookInfo extends EObject {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" mapType="ClassElements.Booking.BookingAndReservationHashMap<ClassElements.Booking.IBooking, ClassElements.Booking.IReservation>" ordered="false"
	 * @generated
	 */
	EMap<IBooking, EList<IReservation>> getReservations();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" ordered="false"
	 * @generated
	 */
	EList<IBooking> getBookings();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookIDRequired="true" bookIDOrdered="false" roomNumberRequired="true" roomNumberOrdered="false" nameRequired="true" nameOrdered="false" priceRequired="true" priceOrdered="false"
	 * @generated
	 */
	Boolean addExtraCost(int bookID, String roomNumber, String name, Double price);
} // IBookInfo
