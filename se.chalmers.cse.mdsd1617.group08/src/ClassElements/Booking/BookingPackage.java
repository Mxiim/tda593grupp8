/**
 */
package ClassElements.Booking;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ClassElements.Booking.BookingFactory
 * @model kind="package"
 * @generated
 */
public interface BookingPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Booking";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///ClassElements/Booking.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ClassElements.Booking";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BookingPackage eINSTANCE = ClassElements.Booking.impl.BookingPackageImpl.init();

	/**
	 * The meta object id for the '{@link ClassElements.Booking.IBookHandler <em>IBook Handler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Booking.IBookHandler
	 * @see ClassElements.Booking.impl.BookingPackageImpl#getIBookHandler()
	 * @generated
	 */
	int IBOOK_HANDLER = 13;

	/**
	 * The meta object id for the '{@link ClassElements.Booking.IPaymentHandler <em>IPayment Handler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Booking.IPaymentHandler
	 * @see ClassElements.Booking.impl.BookingPackageImpl#getIPaymentHandler()
	 * @generated
	 */
	int IPAYMENT_HANDLER = 1;

	/**
	 * The meta object id for the '{@link ClassElements.Booking.impl.PaymentHandlerImpl <em>Payment Handler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Booking.impl.PaymentHandlerImpl
	 * @see ClassElements.Booking.impl.BookingPackageImpl#getPaymentHandler()
	 * @generated
	 */
	int PAYMENT_HANDLER = 7;

	/**
	 * The meta object id for the '{@link ClassElements.Booking.IBookInfo <em>IBook Info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Booking.IBookInfo
	 * @see ClassElements.Booking.impl.BookingPackageImpl#getIBookInfo()
	 * @generated
	 */
	int IBOOK_INFO = 2;

	/**
	 * The meta object id for the '{@link ClassElements.Booking.impl.BookingAndReservationHashMapImpl <em>And Reservation Hash Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Booking.impl.BookingAndReservationHashMapImpl
	 * @see ClassElements.Booking.impl.BookingPackageImpl#getBookingAndReservationHashMap()
	 * @generated
	 */
	int BOOKING_AND_RESERVATION_HASH_MAP = 3;

	/**
	 * The meta object id for the '{@link ClassElements.Booking.IBooking <em>IBooking</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Booking.IBooking
	 * @see ClassElements.Booking.impl.BookingPackageImpl#getIBooking()
	 * @generated
	 */
	int IBOOKING = 4;

	/**
	 * The meta object id for the '{@link ClassElements.Booking.impl.BookInfoImpl <em>Book Info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Booking.impl.BookInfoImpl
	 * @see ClassElements.Booking.impl.BookingPackageImpl#getBookInfo()
	 * @generated
	 */
	int BOOK_INFO = 8;

	/**
	 * The meta object id for the '{@link ClassElements.Booking.impl.BookingImpl <em>Booking</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Booking.impl.BookingImpl
	 * @see ClassElements.Booking.impl.BookingPackageImpl#getBooking()
	 * @generated
	 */
	int BOOKING = 9;

	/**
	 * The meta object id for the '{@link ClassElements.Booking.ICountRoomType <em>ICount Room Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Booking.ICountRoomType
	 * @see ClassElements.Booking.impl.BookingPackageImpl#getICountRoomType()
	 * @generated
	 */
	int ICOUNT_ROOM_TYPE = 5;

	/**
	 * The meta object id for the '{@link ClassElements.Booking.IReservation <em>IReservation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Booking.IReservation
	 * @see ClassElements.Booking.impl.BookingPackageImpl#getIReservation()
	 * @generated
	 */
	int IRESERVATION = 6;

	/**
	 * The meta object id for the '{@link ClassElements.Booking.impl.ReservationImpl <em>Reservation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Booking.impl.ReservationImpl
	 * @see ClassElements.Booking.impl.BookingPackageImpl#getReservation()
	 * @generated
	 */
	int RESERVATION = 10;

	/**
	 * The meta object id for the '{@link ClassElements.Booking.impl.CountRoomTypeImpl <em>Count Room Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Booking.impl.CountRoomTypeImpl
	 * @see ClassElements.Booking.impl.BookingPackageImpl#getCountRoomType()
	 * @generated
	 */
	int COUNT_ROOM_TYPE = 11;

	/**
	 * The meta object id for the '{@link ClassElements.Booking.impl.BookingAndRoomHashMapImpl <em>And Room Hash Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Booking.impl.BookingAndRoomHashMapImpl
	 * @see ClassElements.Booking.impl.BookingPackageImpl#getBookingAndRoomHashMap()
	 * @generated
	 */
	int BOOKING_AND_ROOM_HASH_MAP = 12;

	/**
	 * The meta object id for the '{@link ClassElements.Booking.impl.BookHandlerImpl <em>Book Handler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ClassElements.Booking.impl.BookHandlerImpl
	 * @see ClassElements.Booking.impl.BookingPackageImpl#getBookHandler()
	 * @generated
	 */
	int BOOK_HANDLER = 0;

	/**
	 * The number of structural features of the '<em>IBook Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER___CHECK_IN_ROOM__STRING_INTEGER = 0;

	/**
	 * The operation id for the '<em>Add Extra Cost</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER___ADD_EXTRA_COST__INT_STRING_STRING_DOUBLE = 1;

	/**
	 * The operation id for the '<em>Get Checkouts</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER___GET_CHECKOUTS__STRING = 2;

	/**
	 * The operation id for the '<em>Get Checkouts</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER___GET_CHECKOUTS__STRING_STRING = 3;

	/**
	 * The operation id for the '<em>Update Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER___UPDATE_BOOKING__INT_STRING_STRING = 4;

	/**
	 * The operation id for the '<em>Update Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER___UPDATE_ROOMS__IROOMTYPE_INT_STRING = 5;

	/**
	 * The operation id for the '<em>Get Confirmed Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER___GET_CONFIRMED_BOOKINGS = 6;

	/**
	 * The operation id for the '<em>Get Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER___GET_BOOKING__INT = 7;

	/**
	 * The operation id for the '<em>Get Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER___GET_CHECK_INS__STRING = 8;

	/**
	 * The operation id for the '<em>Get Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER___GET_CHECK_INS__STRING_STRING = 9;

	/**
	 * The operation id for the '<em>Get Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER___GET_OCCUPIED_ROOMS__STRING = 10;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER___CANCEL_BOOKING__INT = 11;

	/**
	 * The operation id for the '<em>Check In Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER___CHECK_IN_BOOKING__INT = 12;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER___INITIATE_BOOKING__STRING_STRING_STRING_STRING = 13;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER___GET_FREE_ROOMS__INT_STRING_STRING = 14;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER___CONFIRM_BOOKING__INT = 15;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER___ADD_ROOM_TO_BOOKING__STRING_INT = 16;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER___INITIATE_CHECKOUT__INT = 17;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING_DOUBLE = 18;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER___INITIATE_ROOM_CHECKOUT__INTEGER_INTEGER = 19;

	/**
	 * The operation id for the '<em>Clear Data</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER___CLEAR_DATA = 20;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER___PAY_ROOM_DURING_CHECKOUT__INTEGER_STRING_STRING_INTEGER_INTEGER_STRING_STRING = 21;

	/**
	 * The number of operations of the '<em>IBook Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_HANDLER_OPERATION_COUNT = 22;

	/**
	 * The feature id for the '<em><b>Singleton Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER__SINGLETON_INSTANCE = IBOOK_HANDLER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Ipaymenthandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER__IPAYMENTHANDLER = IBOOK_HANDLER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Ibookinfo</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER__IBOOKINFO = IBOOK_HANDLER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Book Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER_FEATURE_COUNT = IBOOK_HANDLER_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___CHECK_IN_ROOM__STRING_INTEGER = IBOOK_HANDLER___CHECK_IN_ROOM__STRING_INTEGER;

	/**
	 * The operation id for the '<em>Add Extra Cost</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___ADD_EXTRA_COST__INT_STRING_STRING_DOUBLE = IBOOK_HANDLER___ADD_EXTRA_COST__INT_STRING_STRING_DOUBLE;

	/**
	 * The operation id for the '<em>Get Checkouts</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___GET_CHECKOUTS__STRING = IBOOK_HANDLER___GET_CHECKOUTS__STRING;

	/**
	 * The operation id for the '<em>Get Checkouts</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___GET_CHECKOUTS__STRING_STRING = IBOOK_HANDLER___GET_CHECKOUTS__STRING_STRING;

	/**
	 * The operation id for the '<em>Update Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___UPDATE_BOOKING__INT_STRING_STRING = IBOOK_HANDLER___UPDATE_BOOKING__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Update Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___UPDATE_ROOMS__IROOMTYPE_INT_STRING = IBOOK_HANDLER___UPDATE_ROOMS__IROOMTYPE_INT_STRING;

	/**
	 * The operation id for the '<em>Get Confirmed Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___GET_CONFIRMED_BOOKINGS = IBOOK_HANDLER___GET_CONFIRMED_BOOKINGS;

	/**
	 * The operation id for the '<em>Get Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___GET_BOOKING__INT = IBOOK_HANDLER___GET_BOOKING__INT;

	/**
	 * The operation id for the '<em>Get Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___GET_CHECK_INS__STRING = IBOOK_HANDLER___GET_CHECK_INS__STRING;

	/**
	 * The operation id for the '<em>Get Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___GET_CHECK_INS__STRING_STRING = IBOOK_HANDLER___GET_CHECK_INS__STRING_STRING;

	/**
	 * The operation id for the '<em>Get Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___GET_OCCUPIED_ROOMS__STRING = IBOOK_HANDLER___GET_OCCUPIED_ROOMS__STRING;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___CANCEL_BOOKING__INT = IBOOK_HANDLER___CANCEL_BOOKING__INT;

	/**
	 * The operation id for the '<em>Check In Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___CHECK_IN_BOOKING__INT = IBOOK_HANDLER___CHECK_IN_BOOKING__INT;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___INITIATE_BOOKING__STRING_STRING_STRING_STRING = IBOOK_HANDLER___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___GET_FREE_ROOMS__INT_STRING_STRING = IBOOK_HANDLER___GET_FREE_ROOMS__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___CONFIRM_BOOKING__INT = IBOOK_HANDLER___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___ADD_ROOM_TO_BOOKING__STRING_INT = IBOOK_HANDLER___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___INITIATE_CHECKOUT__INT = IBOOK_HANDLER___INITIATE_CHECKOUT__INT;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING_DOUBLE = IBOOK_HANDLER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING_DOUBLE;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___INITIATE_ROOM_CHECKOUT__INTEGER_INTEGER = IBOOK_HANDLER___INITIATE_ROOM_CHECKOUT__INTEGER_INTEGER;

	/**
	 * The operation id for the '<em>Clear Data</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___CLEAR_DATA = IBOOK_HANDLER___CLEAR_DATA;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___PAY_ROOM_DURING_CHECKOUT__INTEGER_STRING_STRING_INTEGER_INTEGER_STRING_STRING = IBOOK_HANDLER___PAY_ROOM_DURING_CHECKOUT__INTEGER_STRING_STRING_INTEGER_INTEGER_STRING_STRING;

	/**
	 * The operation id for the '<em>Book Handler</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___BOOK_HANDLER = IBOOK_HANDLER_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Instance</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER___GET_INSTANCE = IBOOK_HANDLER_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Book Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_HANDLER_OPERATION_COUNT = IBOOK_HANDLER_OPERATION_COUNT + 2;

	/**
	 * The number of structural features of the '<em>IPayment Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPAYMENT_HANDLER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Pay</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPAYMENT_HANDLER___PAY__STRING_STRING_INT_INT_STRING_STRING_DOUBLE = 0;

	/**
	 * The number of operations of the '<em>IPayment Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPAYMENT_HANDLER_OPERATION_COUNT = 1;

	/**
	 * The number of structural features of the '<em>IBook Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_INFO_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Reservations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_INFO___GET_RESERVATIONS = 0;

	/**
	 * The operation id for the '<em>Get Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_INFO___GET_BOOKINGS = 1;

	/**
	 * The operation id for the '<em>Add Extra Cost</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_INFO___ADD_EXTRA_COST__INT_STRING_STRING_DOUBLE = 2;

	/**
	 * The number of operations of the '<em>IBook Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOK_INFO_OPERATION_COUNT = 3;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_AND_RESERVATION_HASH_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_AND_RESERVATION_HASH_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>And Reservation Hash Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_AND_RESERVATION_HASH_MAP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>And Reservation Hash Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_AND_RESERVATION_HASH_MAP_OPERATION_COUNT = 0;

	/**
	 * The number of structural features of the '<em>IBooking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___ADD_ROOM_TYPE__IROOMTYPE = 0;

	/**
	 * The operation id for the '<em>Get Count Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___GET_COUNT_ROOM_TYPES = 1;

	/**
	 * The number of operations of the '<em>IBooking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_OPERATION_COUNT = 2;

	/**
	 * The number of structural features of the '<em>ICount Room Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICOUNT_ROOM_TYPE_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICOUNT_ROOM_TYPE___GET_ROOM_TYPE = 0;

	/**
	 * The operation id for the '<em>Get Count</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICOUNT_ROOM_TYPE___GET_COUNT = 1;

	/**
	 * The number of operations of the '<em>ICount Room Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICOUNT_ROOM_TYPE_OPERATION_COUNT = 2;

	/**
	 * The number of structural features of the '<em>IReservation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRESERVATION_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRESERVATION___GET_ROOM = 0;

	/**
	 * The operation id for the '<em>Is Checked Out</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRESERVATION___IS_CHECKED_OUT = 1;

	/**
	 * The operation id for the '<em>Get Checked In Time</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRESERVATION___GET_CHECKED_IN_TIME = 2;

	/**
	 * The operation id for the '<em>Get Checked Out Time</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRESERVATION___GET_CHECKED_OUT_TIME = 3;

	/**
	 * The operation id for the '<em>Get Extra Cost Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRESERVATION___GET_EXTRA_COST_NAMES = 4;

	/**
	 * The operation id for the '<em>Get Extra Cost Price</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRESERVATION___GET_EXTRA_COST_PRICE = 5;

	/**
	 * The operation id for the '<em>Add Extra Cost</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRESERVATION___ADD_EXTRA_COST__STRING_DOUBLE = 6;

	/**
	 * The number of operations of the '<em>IReservation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IRESERVATION_OPERATION_COUNT = 7;

	/**
	 * The number of structural features of the '<em>Payment Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYMENT_HANDLER_FEATURE_COUNT = IPAYMENT_HANDLER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Pay</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYMENT_HANDLER___PAY__STRING_STRING_INT_INT_STRING_STRING_DOUBLE = IPAYMENT_HANDLER___PAY__STRING_STRING_INT_INT_STRING_STRING_DOUBLE;

	/**
	 * The number of operations of the '<em>Payment Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYMENT_HANDLER_OPERATION_COUNT = IPAYMENT_HANDLER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Ibooking</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_INFO__IBOOKING = IBOOK_INFO_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Reservations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_INFO__RESERVATIONS = IBOOK_INFO_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Book Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_INFO_FEATURE_COUNT = IBOOK_INFO_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Reservations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_INFO___GET_RESERVATIONS = IBOOK_INFO___GET_RESERVATIONS;

	/**
	 * The operation id for the '<em>Get Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_INFO___GET_BOOKINGS = IBOOK_INFO___GET_BOOKINGS;

	/**
	 * The operation id for the '<em>Add Extra Cost</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_INFO___ADD_EXTRA_COST__INT_STRING_STRING_DOUBLE = IBOOK_INFO___ADD_EXTRA_COST__INT_STRING_STRING_DOUBLE;

	/**
	 * The number of operations of the '<em>Book Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOK_INFO_OPERATION_COUNT = IBOOK_INFO_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Icountroomtype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__ICOUNTROOMTYPE = IBOOKING_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Book ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__BOOK_ID = IBOOKING_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Unique ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__UNIQUE_ID = IBOOKING_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Is Confirmed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__IS_CONFIRMED = IBOOKING_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_FEATURE_COUNT = IBOOKING_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___ADD_ROOM_TYPE__IROOMTYPE = IBOOKING___ADD_ROOM_TYPE__IROOMTYPE;

	/**
	 * The operation id for the '<em>Get Count Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_COUNT_ROOM_TYPES = IBOOKING___GET_COUNT_ROOM_TYPES;

	/**
	 * The operation id for the '<em>Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___BOOKING__STRING_STRING_STRING_STRING = IBOOKING_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_OPERATION_COUNT = IBOOKING_OPERATION_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Iroom</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION__IROOM = IRESERVATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Checked Out</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION__CHECKED_OUT = IRESERVATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Checked In Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION__CHECKED_IN_TIME = IRESERVATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Checked Out Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION__CHECKED_OUT_TIME = IRESERVATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Reservation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION_FEATURE_COUNT = IRESERVATION_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION___GET_ROOM = IRESERVATION___GET_ROOM;

	/**
	 * The operation id for the '<em>Is Checked Out</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION___IS_CHECKED_OUT = IRESERVATION___IS_CHECKED_OUT;

	/**
	 * The operation id for the '<em>Get Checked In Time</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION___GET_CHECKED_IN_TIME = IRESERVATION___GET_CHECKED_IN_TIME;

	/**
	 * The operation id for the '<em>Get Checked Out Time</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION___GET_CHECKED_OUT_TIME = IRESERVATION___GET_CHECKED_OUT_TIME;

	/**
	 * The operation id for the '<em>Get Extra Cost Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION___GET_EXTRA_COST_NAMES = IRESERVATION___GET_EXTRA_COST_NAMES;

	/**
	 * The operation id for the '<em>Get Extra Cost Price</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION___GET_EXTRA_COST_PRICE = IRESERVATION___GET_EXTRA_COST_PRICE;

	/**
	 * The operation id for the '<em>Add Extra Cost</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION___ADD_EXTRA_COST__STRING_DOUBLE = IRESERVATION___ADD_EXTRA_COST__STRING_DOUBLE;

	/**
	 * The number of operations of the '<em>Reservation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION_OPERATION_COUNT = IRESERVATION_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Room Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COUNT_ROOM_TYPE__ROOM_TYPE = ICOUNT_ROOM_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COUNT_ROOM_TYPE__COUNT = ICOUNT_ROOM_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Count Room Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COUNT_ROOM_TYPE_FEATURE_COUNT = ICOUNT_ROOM_TYPE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COUNT_ROOM_TYPE___GET_ROOM_TYPE = ICOUNT_ROOM_TYPE___GET_ROOM_TYPE;

	/**
	 * The operation id for the '<em>Get Count</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COUNT_ROOM_TYPE___GET_COUNT = ICOUNT_ROOM_TYPE___GET_COUNT;

	/**
	 * The number of operations of the '<em>Count Room Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COUNT_ROOM_TYPE_OPERATION_COUNT = ICOUNT_ROOM_TYPE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_AND_ROOM_HASH_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_AND_ROOM_HASH_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>And Room Hash Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_AND_ROOM_HASH_MAP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>And Room Hash Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_AND_ROOM_HASH_MAP_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link ClassElements.Booking.IBookHandler <em>IBook Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IBook Handler</em>'.
	 * @see ClassElements.Booking.IBookHandler
	 * @generated
	 */
	EClass getIBookHandler();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookHandler#checkInRoom(java.lang.String, java.lang.Integer) <em>Check In Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Room</em>' operation.
	 * @see ClassElements.Booking.IBookHandler#checkInRoom(java.lang.String, java.lang.Integer)
	 * @generated
	 */
	EOperation getIBookHandler__CheckInRoom__String_Integer();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookHandler#addExtraCost(int, java.lang.String, java.lang.String, java.lang.Double) <em>Add Extra Cost</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra Cost</em>' operation.
	 * @see ClassElements.Booking.IBookHandler#addExtraCost(int, java.lang.String, java.lang.String, java.lang.Double)
	 * @generated
	 */
	EOperation getIBookHandler__AddExtraCost__int_String_String_Double();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookHandler#getCheckouts(java.lang.String) <em>Get Checkouts</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Checkouts</em>' operation.
	 * @see ClassElements.Booking.IBookHandler#getCheckouts(java.lang.String)
	 * @generated
	 */
	EOperation getIBookHandler__GetCheckouts__String();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookHandler#getCheckouts(java.lang.String, java.lang.String) <em>Get Checkouts</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Checkouts</em>' operation.
	 * @see ClassElements.Booking.IBookHandler#getCheckouts(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookHandler__GetCheckouts__String_String();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookHandler#updateBooking(int, java.lang.String, java.lang.String) <em>Update Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Booking</em>' operation.
	 * @see ClassElements.Booking.IBookHandler#updateBooking(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookHandler__UpdateBooking__int_String_String();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookHandler#updateRooms(ClassElements.Room.IRoomType, int, java.lang.String) <em>Update Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Rooms</em>' operation.
	 * @see ClassElements.Booking.IBookHandler#updateRooms(ClassElements.Room.IRoomType, int, java.lang.String)
	 * @generated
	 */
	EOperation getIBookHandler__UpdateRooms__IRoomType_int_String();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookHandler#getConfirmedBookings() <em>Get Confirmed Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Confirmed Bookings</em>' operation.
	 * @see ClassElements.Booking.IBookHandler#getConfirmedBookings()
	 * @generated
	 */
	EOperation getIBookHandler__GetConfirmedBookings();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookHandler#getBooking(int) <em>Get Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Booking</em>' operation.
	 * @see ClassElements.Booking.IBookHandler#getBooking(int)
	 * @generated
	 */
	EOperation getIBookHandler__GetBooking__int();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookHandler#getCheckIns(java.lang.String) <em>Get Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Check Ins</em>' operation.
	 * @see ClassElements.Booking.IBookHandler#getCheckIns(java.lang.String)
	 * @generated
	 */
	EOperation getIBookHandler__GetCheckIns__String();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookHandler#getCheckIns(java.lang.String, java.lang.String) <em>Get Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Check Ins</em>' operation.
	 * @see ClassElements.Booking.IBookHandler#getCheckIns(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookHandler__GetCheckIns__String_String();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookHandler#getOccupiedRooms(java.lang.String) <em>Get Occupied Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Occupied Rooms</em>' operation.
	 * @see ClassElements.Booking.IBookHandler#getOccupiedRooms(java.lang.String)
	 * @generated
	 */
	EOperation getIBookHandler__GetOccupiedRooms__String();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookHandler#cancelBooking(int) <em>Cancel Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Booking</em>' operation.
	 * @see ClassElements.Booking.IBookHandler#cancelBooking(int)
	 * @generated
	 */
	EOperation getIBookHandler__CancelBooking__int();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookHandler#checkInBooking(int) <em>Check In Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Booking</em>' operation.
	 * @see ClassElements.Booking.IBookHandler#checkInBooking(int)
	 * @generated
	 */
	EOperation getIBookHandler__CheckInBooking__int();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookHandler#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Initiate Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Booking</em>' operation.
	 * @see ClassElements.Booking.IBookHandler#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookHandler__InitiateBooking__String_String_String_String();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookHandler#getFreeRooms(int, java.lang.String, java.lang.String) <em>Get Free Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Free Rooms</em>' operation.
	 * @see ClassElements.Booking.IBookHandler#getFreeRooms(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookHandler__GetFreeRooms__int_String_String();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookHandler#confirmBooking(int) <em>Confirm Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Confirm Booking</em>' operation.
	 * @see ClassElements.Booking.IBookHandler#confirmBooking(int)
	 * @generated
	 */
	EOperation getIBookHandler__ConfirmBooking__int();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookHandler#addRoomToBooking(java.lang.String, int) <em>Add Room To Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room To Booking</em>' operation.
	 * @see ClassElements.Booking.IBookHandler#addRoomToBooking(java.lang.String, int)
	 * @generated
	 */
	EOperation getIBookHandler__AddRoomToBooking__String_int();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookHandler#initiateCheckout(int) <em>Initiate Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Checkout</em>' operation.
	 * @see ClassElements.Booking.IBookHandler#initiateCheckout(int)
	 * @generated
	 */
	EOperation getIBookHandler__InitiateCheckout__int();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookHandler#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String, double) <em>Pay During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay During Checkout</em>' operation.
	 * @see ClassElements.Booking.IBookHandler#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String, double)
	 * @generated
	 */
	EOperation getIBookHandler__PayDuringCheckout__String_String_int_int_String_String_double();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookHandler#initiateRoomCheckout(java.lang.Integer, java.lang.Integer) <em>Initiate Room Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Room Checkout</em>' operation.
	 * @see ClassElements.Booking.IBookHandler#initiateRoomCheckout(java.lang.Integer, java.lang.Integer)
	 * @generated
	 */
	EOperation getIBookHandler__InitiateRoomCheckout__Integer_Integer();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookHandler#clearData() <em>Clear Data</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clear Data</em>' operation.
	 * @see ClassElements.Booking.IBookHandler#clearData()
	 * @generated
	 */
	EOperation getIBookHandler__ClearData();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookHandler#payRoomDuringCheckout(java.lang.Integer, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer, java.lang.String, java.lang.String) <em>Pay Room During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay Room During Checkout</em>' operation.
	 * @see ClassElements.Booking.IBookHandler#payRoomDuringCheckout(java.lang.Integer, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookHandler__PayRoomDuringCheckout__Integer_String_String_Integer_Integer_String_String();

	/**
	 * Returns the meta object for class '{@link ClassElements.Booking.IPaymentHandler <em>IPayment Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IPayment Handler</em>'.
	 * @see ClassElements.Booking.IPaymentHandler
	 * @generated
	 */
	EClass getIPaymentHandler();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IPaymentHandler#pay(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String, double) <em>Pay</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay</em>' operation.
	 * @see ClassElements.Booking.IPaymentHandler#pay(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String, double)
	 * @generated
	 */
	EOperation getIPaymentHandler__Pay__String_String_int_int_String_String_double();

	/**
	 * Returns the meta object for class '{@link ClassElements.Booking.PaymentHandler <em>Payment Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Payment Handler</em>'.
	 * @see ClassElements.Booking.PaymentHandler
	 * @generated
	 */
	EClass getPaymentHandler();

	/**
	 * Returns the meta object for class '{@link ClassElements.Booking.IBookInfo <em>IBook Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IBook Info</em>'.
	 * @see ClassElements.Booking.IBookInfo
	 * @generated
	 */
	EClass getIBookInfo();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookInfo#getReservations() <em>Get Reservations</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Reservations</em>' operation.
	 * @see ClassElements.Booking.IBookInfo#getReservations()
	 * @generated
	 */
	EOperation getIBookInfo__GetReservations();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookInfo#getBookings() <em>Get Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Bookings</em>' operation.
	 * @see ClassElements.Booking.IBookInfo#getBookings()
	 * @generated
	 */
	EOperation getIBookInfo__GetBookings();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBookInfo#addExtraCost(int, java.lang.String, java.lang.String, java.lang.Double) <em>Add Extra Cost</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra Cost</em>' operation.
	 * @see ClassElements.Booking.IBookInfo#addExtraCost(int, java.lang.String, java.lang.String, java.lang.Double)
	 * @generated
	 */
	EOperation getIBookInfo__AddExtraCost__int_String_String_Double();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>And Reservation Hash Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>And Reservation Hash Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="ClassElements.Booking.IBooking" keyRequired="true" keyOrdered="false"
	 *        valueType="ClassElements.Booking.IReservation" valueMany="true" valueOrdered="false"
	 * @generated
	 */
	EClass getBookingAndReservationHashMap();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getBookingAndReservationHashMap()
	 * @generated
	 */
	EReference getBookingAndReservationHashMap_Key();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getBookingAndReservationHashMap()
	 * @generated
	 */
	EReference getBookingAndReservationHashMap_Value();

	/**
	 * Returns the meta object for class '{@link ClassElements.Booking.IBooking <em>IBooking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IBooking</em>'.
	 * @see ClassElements.Booking.IBooking
	 * @generated
	 */
	EClass getIBooking();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBooking#addRoomType(ClassElements.Room.IRoomType) <em>Add Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Type</em>' operation.
	 * @see ClassElements.Booking.IBooking#addRoomType(ClassElements.Room.IRoomType)
	 * @generated
	 */
	EOperation getIBooking__AddRoomType__IRoomType();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IBooking#getCountRoomTypes() <em>Get Count Room Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Count Room Types</em>' operation.
	 * @see ClassElements.Booking.IBooking#getCountRoomTypes()
	 * @generated
	 */
	EOperation getIBooking__GetCountRoomTypes();

	/**
	 * Returns the meta object for class '{@link ClassElements.Booking.BookInfo <em>Book Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Book Info</em>'.
	 * @see ClassElements.Booking.BookInfo
	 * @generated
	 */
	EClass getBookInfo();

	/**
	 * Returns the meta object for the reference list '{@link ClassElements.Booking.BookInfo#getIbooking <em>Ibooking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Ibooking</em>'.
	 * @see ClassElements.Booking.BookInfo#getIbooking()
	 * @see #getBookInfo()
	 * @generated
	 */
	EReference getBookInfo_Ibooking();

	/**
	 * Returns the meta object for the reference list '{@link ClassElements.Booking.BookInfo#getReservations <em>Reservations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Reservations</em>'.
	 * @see ClassElements.Booking.BookInfo#getReservations()
	 * @see #getBookInfo()
	 * @generated
	 */
	EReference getBookInfo_Reservations();

	/**
	 * Returns the meta object for class '{@link ClassElements.Booking.Booking <em>Booking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Booking</em>'.
	 * @see ClassElements.Booking.Booking
	 * @generated
	 */
	EClass getBooking();

	/**
	 * Returns the meta object for the reference list '{@link ClassElements.Booking.Booking#getIcountroomtype <em>Icountroomtype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Icountroomtype</em>'.
	 * @see ClassElements.Booking.Booking#getIcountroomtype()
	 * @see #getBooking()
	 * @generated
	 */
	EReference getBooking_Icountroomtype();

	/**
	 * Returns the meta object for the attribute '{@link ClassElements.Booking.Booking#getBookID <em>Book ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Book ID</em>'.
	 * @see ClassElements.Booking.Booking#getBookID()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_BookID();

	/**
	 * Returns the meta object for the attribute '{@link ClassElements.Booking.Booking#getUniqueID <em>Unique ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unique ID</em>'.
	 * @see ClassElements.Booking.Booking#getUniqueID()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_UniqueID();

	/**
	 * Returns the meta object for the attribute '{@link ClassElements.Booking.Booking#getIsConfirmed <em>Is Confirmed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Confirmed</em>'.
	 * @see ClassElements.Booking.Booking#getIsConfirmed()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_IsConfirmed();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.Booking#Booking(java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Booking</em>' operation.
	 * @see ClassElements.Booking.Booking#Booking(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getBooking__Booking__String_String_String_String();

	/**
	 * Returns the meta object for class '{@link ClassElements.Booking.ICountRoomType <em>ICount Room Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ICount Room Type</em>'.
	 * @see ClassElements.Booking.ICountRoomType
	 * @generated
	 */
	EClass getICountRoomType();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.ICountRoomType#getRoomType() <em>Get Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room Type</em>' operation.
	 * @see ClassElements.Booking.ICountRoomType#getRoomType()
	 * @generated
	 */
	EOperation getICountRoomType__GetRoomType();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.ICountRoomType#getCount() <em>Get Count</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Count</em>' operation.
	 * @see ClassElements.Booking.ICountRoomType#getCount()
	 * @generated
	 */
	EOperation getICountRoomType__GetCount();

	/**
	 * Returns the meta object for class '{@link ClassElements.Booking.IReservation <em>IReservation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IReservation</em>'.
	 * @see ClassElements.Booking.IReservation
	 * @generated
	 */
	EClass getIReservation();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IReservation#getRoom() <em>Get Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room</em>' operation.
	 * @see ClassElements.Booking.IReservation#getRoom()
	 * @generated
	 */
	EOperation getIReservation__GetRoom();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IReservation#isCheckedOut() <em>Is Checked Out</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Checked Out</em>' operation.
	 * @see ClassElements.Booking.IReservation#isCheckedOut()
	 * @generated
	 */
	EOperation getIReservation__IsCheckedOut();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IReservation#getCheckedInTime() <em>Get Checked In Time</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Checked In Time</em>' operation.
	 * @see ClassElements.Booking.IReservation#getCheckedInTime()
	 * @generated
	 */
	EOperation getIReservation__GetCheckedInTime();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IReservation#getCheckedOutTime() <em>Get Checked Out Time</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Checked Out Time</em>' operation.
	 * @see ClassElements.Booking.IReservation#getCheckedOutTime()
	 * @generated
	 */
	EOperation getIReservation__GetCheckedOutTime();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IReservation#getExtraCostNames() <em>Get Extra Cost Names</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Extra Cost Names</em>' operation.
	 * @see ClassElements.Booking.IReservation#getExtraCostNames()
	 * @generated
	 */
	EOperation getIReservation__GetExtraCostNames();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IReservation#getExtraCostPrice() <em>Get Extra Cost Price</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Extra Cost Price</em>' operation.
	 * @see ClassElements.Booking.IReservation#getExtraCostPrice()
	 * @generated
	 */
	EOperation getIReservation__GetExtraCostPrice();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.IReservation#addExtraCost(java.lang.String, java.lang.Double) <em>Add Extra Cost</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra Cost</em>' operation.
	 * @see ClassElements.Booking.IReservation#addExtraCost(java.lang.String, java.lang.Double)
	 * @generated
	 */
	EOperation getIReservation__AddExtraCost__String_Double();

	/**
	 * Returns the meta object for class '{@link ClassElements.Booking.Reservation <em>Reservation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reservation</em>'.
	 * @see ClassElements.Booking.Reservation
	 * @generated
	 */
	EClass getReservation();

	/**
	 * Returns the meta object for the reference '{@link ClassElements.Booking.Reservation#getIroom <em>Iroom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Iroom</em>'.
	 * @see ClassElements.Booking.Reservation#getIroom()
	 * @see #getReservation()
	 * @generated
	 */
	EReference getReservation_Iroom();

	/**
	 * Returns the meta object for the attribute '{@link ClassElements.Booking.Reservation#getCheckedOut <em>Checked Out</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Checked Out</em>'.
	 * @see ClassElements.Booking.Reservation#getCheckedOut()
	 * @see #getReservation()
	 * @generated
	 */
	EAttribute getReservation_CheckedOut();

	/**
	 * Returns the meta object for the attribute '{@link ClassElements.Booking.Reservation#getCheckedInTime <em>Checked In Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Checked In Time</em>'.
	 * @see ClassElements.Booking.Reservation#getCheckedInTime()
	 * @see #getReservation()
	 * @generated
	 */
	EAttribute getReservation_CheckedInTime();

	/**
	 * Returns the meta object for the attribute '{@link ClassElements.Booking.Reservation#getCheckedOutTime <em>Checked Out Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Checked Out Time</em>'.
	 * @see ClassElements.Booking.Reservation#getCheckedOutTime()
	 * @see #getReservation()
	 * @generated
	 */
	EAttribute getReservation_CheckedOutTime();

	/**
	 * Returns the meta object for class '{@link ClassElements.Booking.CountRoomType <em>Count Room Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Count Room Type</em>'.
	 * @see ClassElements.Booking.CountRoomType
	 * @generated
	 */
	EClass getCountRoomType();

	/**
	 * Returns the meta object for the reference '{@link ClassElements.Booking.CountRoomType#getRoomType <em>Room Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room Type</em>'.
	 * @see ClassElements.Booking.CountRoomType#getRoomType()
	 * @see #getCountRoomType()
	 * @generated
	 */
	EReference getCountRoomType_RoomType();

	/**
	 * Returns the meta object for the attribute '{@link ClassElements.Booking.CountRoomType#getCount <em>Count</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Count</em>'.
	 * @see ClassElements.Booking.CountRoomType#getCount()
	 * @see #getCountRoomType()
	 * @generated
	 */
	EAttribute getCountRoomType_Count();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>And Room Hash Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>And Room Hash Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="ClassElements.Room.IRoom" keyRequired="true" keyOrdered="false"
	 *        valueDataType="org.eclipse.emf.ecore.EIntegerObject" valueRequired="true" valueOrdered="false"
	 * @generated
	 */
	EClass getBookingAndRoomHashMap();

	/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getBookingAndRoomHashMap()
	 * @generated
	 */
	EReference getBookingAndRoomHashMap_Key();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getBookingAndRoomHashMap()
	 * @generated
	 */
	EAttribute getBookingAndRoomHashMap_Value();

	/**
	 * Returns the meta object for class '{@link ClassElements.Booking.BookHandler <em>Book Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Book Handler</em>'.
	 * @see ClassElements.Booking.BookHandler
	 * @generated
	 */
	EClass getBookHandler();

	/**
	 * Returns the meta object for the reference '{@link ClassElements.Booking.BookHandler#getSingletonInstance <em>Singleton Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Singleton Instance</em>'.
	 * @see ClassElements.Booking.BookHandler#getSingletonInstance()
	 * @see #getBookHandler()
	 * @generated
	 */
	EReference getBookHandler_SingletonInstance();

	/**
	 * Returns the meta object for the reference '{@link ClassElements.Booking.BookHandler#getIpaymenthandler <em>Ipaymenthandler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ipaymenthandler</em>'.
	 * @see ClassElements.Booking.BookHandler#getIpaymenthandler()
	 * @see #getBookHandler()
	 * @generated
	 */
	EReference getBookHandler_Ipaymenthandler();

	/**
	 * Returns the meta object for the reference '{@link ClassElements.Booking.BookHandler#getIbookinfo <em>Ibookinfo</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ibookinfo</em>'.
	 * @see ClassElements.Booking.BookHandler#getIbookinfo()
	 * @see #getBookHandler()
	 * @generated
	 */
	EReference getBookHandler_Ibookinfo();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.BookHandler#BookHandler() <em>Book Handler</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Book Handler</em>' operation.
	 * @see ClassElements.Booking.BookHandler#BookHandler()
	 * @generated
	 */
	EOperation getBookHandler__BookHandler();

	/**
	 * Returns the meta object for the '{@link ClassElements.Booking.BookHandler#getInstance() <em>Get Instance</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Instance</em>' operation.
	 * @see ClassElements.Booking.BookHandler#getInstance()
	 * @generated
	 */
	EOperation getBookHandler__GetInstance();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BookingFactory getBookingFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ClassElements.Booking.IBookHandler <em>IBook Handler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Booking.IBookHandler
		 * @see ClassElements.Booking.impl.BookingPackageImpl#getIBookHandler()
		 * @generated
		 */
		EClass IBOOK_HANDLER = eINSTANCE.getIBookHandler();

		/**
		 * The meta object literal for the '<em><b>Check In Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_HANDLER___CHECK_IN_ROOM__STRING_INTEGER = eINSTANCE.getIBookHandler__CheckInRoom__String_Integer();

		/**
		 * The meta object literal for the '<em><b>Add Extra Cost</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_HANDLER___ADD_EXTRA_COST__INT_STRING_STRING_DOUBLE = eINSTANCE.getIBookHandler__AddExtraCost__int_String_String_Double();

		/**
		 * The meta object literal for the '<em><b>Get Checkouts</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_HANDLER___GET_CHECKOUTS__STRING = eINSTANCE.getIBookHandler__GetCheckouts__String();

		/**
		 * The meta object literal for the '<em><b>Get Checkouts</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_HANDLER___GET_CHECKOUTS__STRING_STRING = eINSTANCE.getIBookHandler__GetCheckouts__String_String();

		/**
		 * The meta object literal for the '<em><b>Update Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_HANDLER___UPDATE_BOOKING__INT_STRING_STRING = eINSTANCE.getIBookHandler__UpdateBooking__int_String_String();

		/**
		 * The meta object literal for the '<em><b>Update Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_HANDLER___UPDATE_ROOMS__IROOMTYPE_INT_STRING = eINSTANCE.getIBookHandler__UpdateRooms__IRoomType_int_String();

		/**
		 * The meta object literal for the '<em><b>Get Confirmed Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_HANDLER___GET_CONFIRMED_BOOKINGS = eINSTANCE.getIBookHandler__GetConfirmedBookings();

		/**
		 * The meta object literal for the '<em><b>Get Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_HANDLER___GET_BOOKING__INT = eINSTANCE.getIBookHandler__GetBooking__int();

		/**
		 * The meta object literal for the '<em><b>Get Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_HANDLER___GET_CHECK_INS__STRING = eINSTANCE.getIBookHandler__GetCheckIns__String();

		/**
		 * The meta object literal for the '<em><b>Get Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_HANDLER___GET_CHECK_INS__STRING_STRING = eINSTANCE.getIBookHandler__GetCheckIns__String_String();

		/**
		 * The meta object literal for the '<em><b>Get Occupied Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_HANDLER___GET_OCCUPIED_ROOMS__STRING = eINSTANCE.getIBookHandler__GetOccupiedRooms__String();

		/**
		 * The meta object literal for the '<em><b>Cancel Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_HANDLER___CANCEL_BOOKING__INT = eINSTANCE.getIBookHandler__CancelBooking__int();

		/**
		 * The meta object literal for the '<em><b>Check In Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_HANDLER___CHECK_IN_BOOKING__INT = eINSTANCE.getIBookHandler__CheckInBooking__int();

		/**
		 * The meta object literal for the '<em><b>Initiate Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_HANDLER___INITIATE_BOOKING__STRING_STRING_STRING_STRING = eINSTANCE.getIBookHandler__InitiateBooking__String_String_String_String();

		/**
		 * The meta object literal for the '<em><b>Get Free Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_HANDLER___GET_FREE_ROOMS__INT_STRING_STRING = eINSTANCE.getIBookHandler__GetFreeRooms__int_String_String();

		/**
		 * The meta object literal for the '<em><b>Confirm Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_HANDLER___CONFIRM_BOOKING__INT = eINSTANCE.getIBookHandler__ConfirmBooking__int();

		/**
		 * The meta object literal for the '<em><b>Add Room To Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_HANDLER___ADD_ROOM_TO_BOOKING__STRING_INT = eINSTANCE.getIBookHandler__AddRoomToBooking__String_int();

		/**
		 * The meta object literal for the '<em><b>Initiate Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_HANDLER___INITIATE_CHECKOUT__INT = eINSTANCE.getIBookHandler__InitiateCheckout__int();

		/**
		 * The meta object literal for the '<em><b>Pay During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_HANDLER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING_DOUBLE = eINSTANCE.getIBookHandler__PayDuringCheckout__String_String_int_int_String_String_double();

		/**
		 * The meta object literal for the '<em><b>Initiate Room Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_HANDLER___INITIATE_ROOM_CHECKOUT__INTEGER_INTEGER = eINSTANCE.getIBookHandler__InitiateRoomCheckout__Integer_Integer();

		/**
		 * The meta object literal for the '<em><b>Clear Data</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_HANDLER___CLEAR_DATA = eINSTANCE.getIBookHandler__ClearData();

		/**
		 * The meta object literal for the '<em><b>Pay Room During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_HANDLER___PAY_ROOM_DURING_CHECKOUT__INTEGER_STRING_STRING_INTEGER_INTEGER_STRING_STRING = eINSTANCE.getIBookHandler__PayRoomDuringCheckout__Integer_String_String_Integer_Integer_String_String();

		/**
		 * The meta object literal for the '{@link ClassElements.Booking.IPaymentHandler <em>IPayment Handler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Booking.IPaymentHandler
		 * @see ClassElements.Booking.impl.BookingPackageImpl#getIPaymentHandler()
		 * @generated
		 */
		EClass IPAYMENT_HANDLER = eINSTANCE.getIPaymentHandler();

		/**
		 * The meta object literal for the '<em><b>Pay</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IPAYMENT_HANDLER___PAY__STRING_STRING_INT_INT_STRING_STRING_DOUBLE = eINSTANCE.getIPaymentHandler__Pay__String_String_int_int_String_String_double();

		/**
		 * The meta object literal for the '{@link ClassElements.Booking.impl.PaymentHandlerImpl <em>Payment Handler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Booking.impl.PaymentHandlerImpl
		 * @see ClassElements.Booking.impl.BookingPackageImpl#getPaymentHandler()
		 * @generated
		 */
		EClass PAYMENT_HANDLER = eINSTANCE.getPaymentHandler();

		/**
		 * The meta object literal for the '{@link ClassElements.Booking.IBookInfo <em>IBook Info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Booking.IBookInfo
		 * @see ClassElements.Booking.impl.BookingPackageImpl#getIBookInfo()
		 * @generated
		 */
		EClass IBOOK_INFO = eINSTANCE.getIBookInfo();

		/**
		 * The meta object literal for the '<em><b>Get Reservations</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_INFO___GET_RESERVATIONS = eINSTANCE.getIBookInfo__GetReservations();

		/**
		 * The meta object literal for the '<em><b>Get Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_INFO___GET_BOOKINGS = eINSTANCE.getIBookInfo__GetBookings();

		/**
		 * The meta object literal for the '<em><b>Add Extra Cost</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOK_INFO___ADD_EXTRA_COST__INT_STRING_STRING_DOUBLE = eINSTANCE.getIBookInfo__AddExtraCost__int_String_String_Double();

		/**
		 * The meta object literal for the '{@link ClassElements.Booking.impl.BookingAndReservationHashMapImpl <em>And Reservation Hash Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Booking.impl.BookingAndReservationHashMapImpl
		 * @see ClassElements.Booking.impl.BookingPackageImpl#getBookingAndReservationHashMap()
		 * @generated
		 */
		EClass BOOKING_AND_RESERVATION_HASH_MAP = eINSTANCE.getBookingAndReservationHashMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_AND_RESERVATION_HASH_MAP__KEY = eINSTANCE.getBookingAndReservationHashMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_AND_RESERVATION_HASH_MAP__VALUE = eINSTANCE.getBookingAndReservationHashMap_Value();

		/**
		 * The meta object literal for the '{@link ClassElements.Booking.IBooking <em>IBooking</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Booking.IBooking
		 * @see ClassElements.Booking.impl.BookingPackageImpl#getIBooking()
		 * @generated
		 */
		EClass IBOOKING = eINSTANCE.getIBooking();

		/**
		 * The meta object literal for the '<em><b>Add Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___ADD_ROOM_TYPE__IROOMTYPE = eINSTANCE.getIBooking__AddRoomType__IRoomType();

		/**
		 * The meta object literal for the '<em><b>Get Count Room Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___GET_COUNT_ROOM_TYPES = eINSTANCE.getIBooking__GetCountRoomTypes();

		/**
		 * The meta object literal for the '{@link ClassElements.Booking.impl.BookInfoImpl <em>Book Info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Booking.impl.BookInfoImpl
		 * @see ClassElements.Booking.impl.BookingPackageImpl#getBookInfo()
		 * @generated
		 */
		EClass BOOK_INFO = eINSTANCE.getBookInfo();

		/**
		 * The meta object literal for the '<em><b>Ibooking</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOK_INFO__IBOOKING = eINSTANCE.getBookInfo_Ibooking();

		/**
		 * The meta object literal for the '<em><b>Reservations</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOK_INFO__RESERVATIONS = eINSTANCE.getBookInfo_Reservations();

		/**
		 * The meta object literal for the '{@link ClassElements.Booking.impl.BookingImpl <em>Booking</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Booking.impl.BookingImpl
		 * @see ClassElements.Booking.impl.BookingPackageImpl#getBooking()
		 * @generated
		 */
		EClass BOOKING = eINSTANCE.getBooking();

		/**
		 * The meta object literal for the '<em><b>Icountroomtype</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING__ICOUNTROOMTYPE = eINSTANCE.getBooking_Icountroomtype();

		/**
		 * The meta object literal for the '<em><b>Book ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__BOOK_ID = eINSTANCE.getBooking_BookID();

		/**
		 * The meta object literal for the '<em><b>Unique ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__UNIQUE_ID = eINSTANCE.getBooking_UniqueID();

		/**
		 * The meta object literal for the '<em><b>Is Confirmed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__IS_CONFIRMED = eINSTANCE.getBooking_IsConfirmed();

		/**
		 * The meta object literal for the '<em><b>Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___BOOKING__STRING_STRING_STRING_STRING = eINSTANCE.getBooking__Booking__String_String_String_String();

		/**
		 * The meta object literal for the '{@link ClassElements.Booking.ICountRoomType <em>ICount Room Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Booking.ICountRoomType
		 * @see ClassElements.Booking.impl.BookingPackageImpl#getICountRoomType()
		 * @generated
		 */
		EClass ICOUNT_ROOM_TYPE = eINSTANCE.getICountRoomType();

		/**
		 * The meta object literal for the '<em><b>Get Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ICOUNT_ROOM_TYPE___GET_ROOM_TYPE = eINSTANCE.getICountRoomType__GetRoomType();

		/**
		 * The meta object literal for the '<em><b>Get Count</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ICOUNT_ROOM_TYPE___GET_COUNT = eINSTANCE.getICountRoomType__GetCount();

		/**
		 * The meta object literal for the '{@link ClassElements.Booking.IReservation <em>IReservation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Booking.IReservation
		 * @see ClassElements.Booking.impl.BookingPackageImpl#getIReservation()
		 * @generated
		 */
		EClass IRESERVATION = eINSTANCE.getIReservation();

		/**
		 * The meta object literal for the '<em><b>Get Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRESERVATION___GET_ROOM = eINSTANCE.getIReservation__GetRoom();

		/**
		 * The meta object literal for the '<em><b>Is Checked Out</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRESERVATION___IS_CHECKED_OUT = eINSTANCE.getIReservation__IsCheckedOut();

		/**
		 * The meta object literal for the '<em><b>Get Checked In Time</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRESERVATION___GET_CHECKED_IN_TIME = eINSTANCE.getIReservation__GetCheckedInTime();

		/**
		 * The meta object literal for the '<em><b>Get Checked Out Time</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRESERVATION___GET_CHECKED_OUT_TIME = eINSTANCE.getIReservation__GetCheckedOutTime();

		/**
		 * The meta object literal for the '<em><b>Get Extra Cost Names</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRESERVATION___GET_EXTRA_COST_NAMES = eINSTANCE.getIReservation__GetExtraCostNames();

		/**
		 * The meta object literal for the '<em><b>Get Extra Cost Price</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRESERVATION___GET_EXTRA_COST_PRICE = eINSTANCE.getIReservation__GetExtraCostPrice();

		/**
		 * The meta object literal for the '<em><b>Add Extra Cost</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IRESERVATION___ADD_EXTRA_COST__STRING_DOUBLE = eINSTANCE.getIReservation__AddExtraCost__String_Double();

		/**
		 * The meta object literal for the '{@link ClassElements.Booking.impl.ReservationImpl <em>Reservation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Booking.impl.ReservationImpl
		 * @see ClassElements.Booking.impl.BookingPackageImpl#getReservation()
		 * @generated
		 */
		EClass RESERVATION = eINSTANCE.getReservation();

		/**
		 * The meta object literal for the '<em><b>Iroom</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESERVATION__IROOM = eINSTANCE.getReservation_Iroom();

		/**
		 * The meta object literal for the '<em><b>Checked Out</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESERVATION__CHECKED_OUT = eINSTANCE.getReservation_CheckedOut();

		/**
		 * The meta object literal for the '<em><b>Checked In Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESERVATION__CHECKED_IN_TIME = eINSTANCE.getReservation_CheckedInTime();

		/**
		 * The meta object literal for the '<em><b>Checked Out Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESERVATION__CHECKED_OUT_TIME = eINSTANCE.getReservation_CheckedOutTime();

		/**
		 * The meta object literal for the '{@link ClassElements.Booking.impl.CountRoomTypeImpl <em>Count Room Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Booking.impl.CountRoomTypeImpl
		 * @see ClassElements.Booking.impl.BookingPackageImpl#getCountRoomType()
		 * @generated
		 */
		EClass COUNT_ROOM_TYPE = eINSTANCE.getCountRoomType();

		/**
		 * The meta object literal for the '<em><b>Room Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COUNT_ROOM_TYPE__ROOM_TYPE = eINSTANCE.getCountRoomType_RoomType();

		/**
		 * The meta object literal for the '<em><b>Count</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COUNT_ROOM_TYPE__COUNT = eINSTANCE.getCountRoomType_Count();

		/**
		 * The meta object literal for the '{@link ClassElements.Booking.impl.BookingAndRoomHashMapImpl <em>And Room Hash Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Booking.impl.BookingAndRoomHashMapImpl
		 * @see ClassElements.Booking.impl.BookingPackageImpl#getBookingAndRoomHashMap()
		 * @generated
		 */
		EClass BOOKING_AND_ROOM_HASH_MAP = eINSTANCE.getBookingAndRoomHashMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_AND_ROOM_HASH_MAP__KEY = eINSTANCE.getBookingAndRoomHashMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING_AND_ROOM_HASH_MAP__VALUE = eINSTANCE.getBookingAndRoomHashMap_Value();

		/**
		 * The meta object literal for the '{@link ClassElements.Booking.impl.BookHandlerImpl <em>Book Handler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ClassElements.Booking.impl.BookHandlerImpl
		 * @see ClassElements.Booking.impl.BookingPackageImpl#getBookHandler()
		 * @generated
		 */
		EClass BOOK_HANDLER = eINSTANCE.getBookHandler();

		/**
		 * The meta object literal for the '<em><b>Singleton Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOK_HANDLER__SINGLETON_INSTANCE = eINSTANCE.getBookHandler_SingletonInstance();

		/**
		 * The meta object literal for the '<em><b>Ipaymenthandler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOK_HANDLER__IPAYMENTHANDLER = eINSTANCE.getBookHandler_Ipaymenthandler();

		/**
		 * The meta object literal for the '<em><b>Ibookinfo</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOK_HANDLER__IBOOKINFO = eINSTANCE.getBookHandler_Ibookinfo();

		/**
		 * The meta object literal for the '<em><b>Book Handler</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOK_HANDLER___BOOK_HANDLER = eINSTANCE.getBookHandler__BookHandler();

		/**
		 * The meta object literal for the '<em><b>Get Instance</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOK_HANDLER___GET_INSTANCE = eINSTANCE.getBookHandler__GetInstance();

	}

} //BookingPackage
