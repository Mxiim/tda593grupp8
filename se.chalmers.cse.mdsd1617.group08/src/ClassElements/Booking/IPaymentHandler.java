/**
 */
package ClassElements.Booking;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IPayment Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ClassElements.Booking.BookingPackage#getIPaymentHandler()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IPaymentHandler extends EObject {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" ccRequired="true" ccOrdered="false" ccvRequired="true" ccvOrdered="false" expMonthRequired="true" expMonthOrdered="false" expYearRequired="true" expYearOrdered="false" firstNameRequired="true" firstNameOrdered="false" lastNameRequired="true" lastNameOrdered="false" moneyToPayRequired="true" moneyToPayOrdered="false"
	 * @generated
	 */
	Boolean pay(String cc, String ccv, int expMonth, int expYear, String firstName, String lastName, double moneyToPay);
} // IPaymentHandler
