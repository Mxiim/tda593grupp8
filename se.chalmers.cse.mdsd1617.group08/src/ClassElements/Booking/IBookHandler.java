/**
 */
package ClassElements.Booking;

import ClassElements.HotelCustomer.FreeRoomTypesDTO;
import ClassElements.Room.IRoom;
import ClassElements.Room.IRoomType;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IBook Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ClassElements.Booking.BookingPackage#getIBookHandler()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IBookHandler extends EObject {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeNameRequired="true" roomTypeNameOrdered="false" bookIDRequired="true" bookIDOrdered="false"
	 * @generated
	 */
	Integer checkInRoom(String roomTypeName, Integer bookID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookIDRequired="true" bookIDOrdered="false" roomNumberRequired="true" roomNumberOrdered="false" nameRequired="true" nameOrdered="false" priceRequired="true" priceOrdered="false"
	 * @generated
	 */
	Boolean addExtraCost(int bookID, String roomNumber, String name, Double price);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" startDateRequired="true" startDateOrdered="false"
	 * @generated
	 */
	EList<Integer> getCheckouts(String startDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" startDateRequired="true" startDateOrdered="false" endDateRequired="true" endDateOrdered="false"
	 * @generated
	 */
	EList<Integer> getCheckouts(String startDate, String endDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bookIDRequired="true" bookIDOrdered="false" startDateRequired="true" startDateOrdered="false" endDateRequired="true" endDateOrdered="false"
	 * @generated
	 */
	void updateBooking(int bookID, String startDate, String endDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomTypeRequired="true" roomTypeOrdered="false" newCountRequired="true" newCountOrdered="false" bookIDRequired="true" bookIDOrdered="false"
	 * @generated
	 */
	void updateRooms(IRoomType roomType, int newCount, String bookID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" ordered="false"
	 * @generated
	 */
	EList<IBooking> getConfirmedBookings();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookIDRequired="true" bookIDOrdered="false"
	 * @generated
	 */
	IBooking getBooking(int bookID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" dateRequired="true" dateOrdered="false"
	 * @generated
	 */
	EList<Integer> getCheckIns(String date);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" startDateRequired="true" startDateOrdered="false" endDateRequired="true" endDateOrdered="false"
	 * @generated
	 */
	EList<Integer> getCheckIns(String startDate, String endDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model mapType="ClassElements.Booking.BookingAndRoomHashMap<ClassElements.Room.IRoom, org.eclipse.emf.ecore.EIntegerObject>" ordered="false" dateRequired="true" dateOrdered="false"
	 * @generated
	 */
	EMap<IRoom, Integer> getOccupiedRooms(String date);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bookIDRequired="true" bookIDOrdered="false"
	 * @generated
	 */
	void cancelBooking(int bookID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bookIDRequired="true" bookIDOrdered="false"
	 * @generated
	 */
	void checkInBooking(int bookID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" firstNameRequired="true" firstNameOrdered="false" lastNameRequired="true" lastNameOrdered="false" startDateRequired="true" startDateOrdered="false" endDateRequired="true" endDateOrdered="false"
	 * @generated
	 */
	int initiateBooking(String firstName, String lastName, String startDate, String endDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" numBedsRequired="true" numBedsOrdered="false" startDateRequired="true" startDateOrdered="false" endDateRequired="true" endDateOrdered="false"
	 * @generated
	 */
	EList<FreeRoomTypesDTO> getFreeRooms(int numBeds, String startDate, String endDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookIDRequired="true" bookIDOrdered="false"
	 * @generated
	 */
	Boolean confirmBooking(int bookID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeDescRequired="true" roomTypeDescOrdered="false" bookIDRequired="true" bookIDOrdered="false"
	 * @generated
	 */
	Boolean addRoomToBooking(String roomTypeDesc, int bookID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookIDRequired="true" bookIDOrdered="false"
	 * @generated
	 */
	Double initiateCheckout(int bookID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" ccRequired="true" ccOrdered="false" ccvRequired="true" ccvOrdered="false" expMonthRequired="true" expMonthOrdered="false" expYearRequired="true" expYearOrdered="false" firstNameRequired="true" firstNameOrdered="false" lastnameRequired="true" lastnameOrdered="false" amountRequired="true" amountOrdered="false"
	 * @generated
	 */
	Boolean payDuringCheckout(String cc, String ccv, int expMonth, int expYear, String firstName, String lastname, double amount);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookIDRequired="true" bookIDOrdered="false" roomNumberRequired="true" roomNumberOrdered="false"
	 * @generated
	 */
	double initiateRoomCheckout(Integer bookID, Integer roomNumber);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void clearData();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNumberRequired="true" roomNumberOrdered="false" ccRequired="true" ccOrdered="false" ccvRequired="true" ccvOrdered="false" expMonthRequired="true" expMonthOrdered="false" expYearRequired="true" expYearOrdered="false" firstNameRequired="true" firstNameOrdered="false" lastNameRequired="true" lastNameOrdered="false"
	 * @generated
	 */
	boolean payRoomDuringCheckout(Integer roomNumber, String cc, String ccv, Integer expMonth, Integer expYear, String firstName, String lastName);
} // IBookHandler
