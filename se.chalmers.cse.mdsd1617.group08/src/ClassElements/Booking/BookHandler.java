/**
 */
package ClassElements.Booking;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Book Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ClassElements.Booking.BookHandler#getSingletonInstance <em>Singleton Instance</em>}</li>
 *   <li>{@link ClassElements.Booking.BookHandler#getIpaymenthandler <em>Ipaymenthandler</em>}</li>
 *   <li>{@link ClassElements.Booking.BookHandler#getIbookinfo <em>Ibookinfo</em>}</li>
 * </ul>
 *
 * @see ClassElements.Booking.BookingPackage#getBookHandler()
 * @model
 * @generated
 */
public interface BookHandler extends IBookHandler {
	/**
	 * Returns the value of the '<em><b>Singleton Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Singleton Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Singleton Instance</em>' reference.
	 * @see #setSingletonInstance(BookHandler)
	 * @see ClassElements.Booking.BookingPackage#getBookHandler_SingletonInstance()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	BookHandler getSingletonInstance();

	/**
	 * Sets the value of the '{@link ClassElements.Booking.BookHandler#getSingletonInstance <em>Singleton Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Singleton Instance</em>' reference.
	 * @see #getSingletonInstance()
	 * @generated
	 */
	void setSingletonInstance(BookHandler value);

	/**
	 * Returns the value of the '<em><b>Ipaymenthandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ipaymenthandler</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ipaymenthandler</em>' reference.
	 * @see #setIpaymenthandler(IPaymentHandler)
	 * @see ClassElements.Booking.BookingPackage#getBookHandler_Ipaymenthandler()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IPaymentHandler getIpaymenthandler();

	/**
	 * Sets the value of the '{@link ClassElements.Booking.BookHandler#getIpaymenthandler <em>Ipaymenthandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ipaymenthandler</em>' reference.
	 * @see #getIpaymenthandler()
	 * @generated
	 */
	void setIpaymenthandler(IPaymentHandler value);

	/**
	 * Returns the value of the '<em><b>Ibookinfo</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ibookinfo</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ibookinfo</em>' reference.
	 * @see #setIbookinfo(IBookInfo)
	 * @see ClassElements.Booking.BookingPackage#getBookHandler_Ibookinfo()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IBookInfo getIbookinfo();

	/**
	 * Sets the value of the '{@link ClassElements.Booking.BookHandler#getIbookinfo <em>Ibookinfo</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ibookinfo</em>' reference.
	 * @see #getIbookinfo()
	 * @generated
	 */
	void setIbookinfo(IBookInfo value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void BookHandler();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true" ordered="false"
	 * @generated
	 */
	BookHandler getInstance();

} // BookHandler
